/*****************************************************
This program was produced by the
CodeWizardAVR V1.24.8e Evaluation
Automatic Program Generator
� Copyright 1998-2006 Pavel Haiduc, HP InfoTech s.r.l.
http://www.hpinfotech.com

Project : Matrix Keypad(4x3)
Version : 1.0
Date    : 25/25/2007
Author  : SRINIVASAN.M
Company : Scorpion Controls
Comments: 
This programe is made to support matrix kepad with the specification of 4 row and 3 columns.
If you have any doubts in this programe, then visit ATMEL community in www.Orkut.com.


Chip type           : ATmega8535
Program type        : Application
Clock frequency     : 1.000000 MHz
Memory model        : Small
External SRAM size  : 0
Data Stack size     : 128
*****************************************************/


// Standard Input/Output functions
#ifndef ATMEGA32
    #define MCU         ATMEGA32
    #include <mega32.h>
#endif
#include <MatrixKeypad4x3.h>
#include <delay.h> // ��͹˹�ǧ���� ��ͧ��駤�� xtal � config �ͧ compiler ����

// ��˹�..
// - ��� port ��� 2 �ѧ���蹴�ҹ�� ��ҹ�� (㹹���� portA)
// - Px.0 - x.3 ��͡Ѻ Row
// - Px.4 - x.6 ��͡Ѻ Col 

// �Ըա����
// 0. ��С�ȡ�� include �������͹�Ѻ MCU �����
// 1. ��С�� matrix_key_init(); (�� portA ��ҹ͡�˹�ͨҡ����ͧ��)
// 2. ��Ǩ�� matrix_key_chk(); (�Ф׹������� ������衴)

// ������ҧ debounce
// ��Ǩ����ա�á���� key ������� ����� debounce
/*unsigned char get_keypad()
{
    unsigned char ch;
    if ((ch = matrix_key_chk()) != 'x')
    {
        delay_ms(25); // debounce
        if ((ch = matrix_key_chk()) != 'x')
        {
            while(matrix_key_chk() != 'x'); // wait release key
            delay_ms(25); // debounce
        }
    }
    return ch;
}*/

// �� portA
#define KEYPAD_PORT PORTA
#define KEYPAD_DDR  DDRA
#define KEYPAD_PIN	PINA

/*

  ���ҧ᡹

  PinX.6  PinX.5  PinX.4
    |       |       |
	|       |       |
----+-------+-------+------   PinX.3
    |       |       |
----+-------+-------+------   PinX.2
    |       |       |
----+-------+-------+------   PinX.1
    |       |       |
----+-------+-------+------   PinX.0

*/

void matrix_key_init(void)
{   
    KEYPAD_DDR = 0xf0;	// bit 7,6,5,4 = output
						// bit 3,2,1,0 = input	
    KEYPAD_PORT = 0x0f; // 0b  0000 1111 //for internal pullup
}

unsigned char matrix_key_chk(void)
{
    unsigned char x;
    unsigned char keypad = 'x';
    
    // column 1
    KEYPAD_PORT = 0xbf; // 1011 1111
    for (x=0;x<255;x++);    // ��ͧ���� delay (����繤�����ص������) ���蹹���ѹ����ҹ��� port ���ѹ
    switch(KEYPAD_PIN & 0x0f)
    {
        case 0x07: keypad='1'; break; // 0000 0111
        case 0x0b: keypad='4'; break; // 0000 1011
        case 0x0d: keypad='7'; break; // 0000 1101
        case 0x0e: keypad='*'; break; // 0000 1110
    }
    if (keypad != 'x') return keypad;
    
    // column 2
    
    KEYPAD_PORT = 0xdf; // 1101 1111 
    for (x=0;x<255;x++);  // ��ͧ���� delay (����繤�����ص������) ���蹹���ѹ����ҹ��� port ���ѹ
    switch(KEYPAD_PIN & 0x0f)
    {
        case 0x07: keypad='2'; break;
        case 0x0b: keypad='5'; break;
        case 0x0d: keypad='8'; break;
        case 0x0e: keypad='0'; break;
    }        
    if (keypad != 'x') return keypad;
    
    // column 3
    KEYPAD_PORT = 0xef; // 1110 1111
    for (x=0;x<255;x++);    // ��ͧ���� delay (����繤�����ص������) ���蹹���ѹ����ҹ��� port ���ѹ
    switch(KEYPAD_PIN & 0x0f)
    {
        case 0x07: keypad='3'; break;
        case 0x0b: keypad='6'; break;
        case 0x0d: keypad='9'; break;
        case 0x0e: keypad='#'; break;
    }
    //if (keypad != 'x') return keypad; 
    return keypad;
}
                
                
/**************************************************************************
// ��Ǩ����ա�á���� key ������� ����� debounce
/**************************************************************************/
/*
unsigned char get_keypad()
{
    unsigned char ch = 'x';
    if ((ch = matrix_key_chk()) != 'x')
    {
        delay_ms(20); // debounce
        if ((ch = matrix_key_chk()) != 'x')
        {
            while(matrix_key_chk() != 'x'); // wait release key
            delay_ms(10); // debounce
        }
    }
    return ch;
}*/