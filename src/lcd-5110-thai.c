/****************************************************/
/* Example Program For ATMEGA32                     */
/* MCU      : ATMEGA32(XTAL=16 MHz)                 */
/*          : Frequency Bus = 16 MHz                */
/* Compiler : CodeVisionAVR 1.25.8a Professional    */
/* Write By : BenNueng@gmail.com                    */
/* Function : Demo Interface LCD Nokia-5110         */ 
/*          : Use Pin I/O Generate SPI Signal       */
/*          : Thai language                         */
/****************************************************/
/* Interface LCD ETT-NOKIA-5110 By AVR Function     */
/*          : use default wire with config          */
/*            by lcd-control.h                      */
/****************************************************/  


/* include section */
#include <lcd-5110-thai.h>

flash unsigned char numeric_tbl[] = {'0','1','2','3','4','5','6','7','8','9'};

// ��˹�������������
/* ��С�ȵ���� GLOBAL ����ͧ����к� VIDEO RAM */
unsigned char VIDEO_RAM[LCD_NOKIA_5110];
// MODE ; 0 = normal (english 14x6 line), 1 = graphics (Thai 14x3 line)
unsigned char LCD_MODE = LCD_GRAPHICS;
// ���˹� x,y ������Ҵ (���˹�Ҩ�, ����к�) (axis)
unsigned char ax = 0;
unsigned char ay = 0;
// ���� buffer �纡�û����ż������ʴ�˹�Ҩ�
unsigned char tmp_display_string[42];
unsigned char tmp_display_string_len;


/*****************************************/
/* �Ҵ�Ҿ�ҡ VideoRam 价�� LCD            */
/*****************************************/
void lcd_update_video_ram(void)
{  
    unsigned int i = 0;        // Memory Display(Byte) Counter
    if (LCD_MODE == LCD_GRAPHICS)
    {  
        lcd_write_command(128+0);   // Set X Position = 0(0..83)
        lcd_write_command(64+0);    // Set Y Position = 0(0..5)
  
        // 84 * 6 = LCD_NOKIA_5110
        for(i=0; i<LCD_NOKIA_5110; i++)          // All Display RAM = LCD_NOKIA_5110 Byte  
            lcd_write_data(VIDEO_RAM[i]); // Fill Picture Display
    }
}   

// ź�Ҿ� VIDEO RAM
void lcd_clear_video_ram(void)
{
    unsigned int i = 0;
    for(i=0; i< LCD_NOKIA_5110; i++)
        VIDEO_RAM[i] = 0;
        
    // reset all cursor
    ax = 0;
    ay = 0;
}

// ��˹����˹� ax,ay �Ѩ�غѹ    
void lcd_set_axay(unsigned char x, unsigned char y)
{
    ax = x; ay = y;
}

// plot �ش价����˹觹��
void lcd_set_dot(unsigned char x, unsigned char y)
{
    unsigned char bitcount = 0;
    unsigned char mask = 1;
    x = x > 83 ? 83 : x;
    y = y > 47 ? 47 : y;
    
    bitcount = VRAM_BIT(y);
    mask <<= bitcount;
    VIDEO_RAM[VRAM_ARRAY_INDEX((unsigned int)x,(unsigned int)y)] |= mask;
}

// ����ҵ��˹� x,y ����繤������
unsigned char lcd_get_dot(unsigned char x, unsigned char y)
{
    unsigned char bitcount = 0;
    unsigned char mask = 1;
    x = x > 83 ? 83 : x;
    y = y > 47 ? 47 : y;
    
    bitcount = VRAM_BIT(y);
    mask <<= bitcount;
    mask = ~mask;
    mask = VIDEO_RAM[VRAM_ARRAY_INDEX((unsigned int)x,(unsigned int)y)] & mask;
    return  mask >>= bitcount ;
}                                                    
                                                     
// ź�ش � ���˹觹���
void lcd_clr_dot(unsigned char x, unsigned char y)
{
    unsigned char bitcount = 0;
    unsigned char mask = 1;
    x = x > 83 ? 83 : x;
    y = y > 47 ? 47 : y;
    
    bitcount = VRAM_BIT(y);
    mask <<= bitcount;
    mask = ~mask;
    VIDEO_RAM[VRAM_ARRAY_INDEX((unsigned int)x,(unsigned int)y)] &= mask;
}
 
// �������Ҵ�ش ����ź�ش � ���˹觹��
void lcd_plot_dot(unsigned char x, unsigned char y, unsigned char dotproperty)
{
    if (dotproperty)
        lcd_set_dot(x,y);
    else
        lcd_clr_dot(x,y);
}

// ��˹���Ҩ���ҹ���� normal ���� graphics
void lcd_set_mode(unsigned char mode)
{
    if (mode == LCD_NORMAL)
    {
        LCD_MODE = LCD_NORMAL;
    }
    else
    {
        LCD_MODE = LCD_GRAPHICS;
        lcd_clear_video_ram();
    }
}
 
/****************************/
/* Fill Picture Display LCD */
/****************************/
void lcd_fill_picture_special(flash unsigned char *tab_picture, unsigned char write_mode)
{  
    unsigned int i = 0;        // Memory Display(Byte) Counter
    
    lcd_write_command(128+0);   // Set X Position = 0(0..83)
    lcd_write_command(64+0);    // Set Y Position = 0(0..5)
  
    // 84 * 6 = 504
    for(i=0;i<504;i++)          // All Display RAM = 504 Byte  
    {
        if (write_mode == WRITE_NORMAL)
        {
            VIDEO_RAM[i] = tab_picture[i]; // Fill Picture Display
        }
        else if (write_mode == WRITE_REVERSE)
        {
            VIDEO_RAM[i] = ~tab_picture[i]; // Fill Picture Display        
        }
    }            
} 

// �Ҵ�ѡ���ŧ VIDEO RAM
void lcd_put_charxy_2vram(unsigned char x,unsigned char y, unsigned char character,unsigned char write_mode)
{
    lcd_set_axay(x,y);
    lcd_put_char_2vram(character, write_mode);
}

void lcd_put_char_2vram(unsigned char character, unsigned char write_mode)
{                        
    // for plot Thai lang
    unsigned char bitcount = 0; 
    unsigned char tmp_data;
    
    // Font Size Counter
    unsigned char font_size_count = 0; 			
    // Font Data Pointer
    unsigned int  font_data_index;  	 			

    // Skip 0x00..0x1F Font Code
    font_data_index = character-32;    			
    // 5 Byte / Font       
    font_data_index = font_data_index*5;			
  
    // ��Ǩ�ͺ᡹ y ��͹�Ҵ
    ay = ay > MAX_Y ? MAX_Y : ay;
    // Get 5 Byte Font & Display on LCD
    while(font_size_count<5)                     			
    {  													
        // Get Data of Font From Table & Write 2 VIDEO RAM
        tmp_data = tab_font[font_data_index];
        for (bitcount = 0; bitcount < 8; bitcount++)
        {   
            // 0 = ��¹����
            if (write_mode == WRITE_NORMAL)
            {
                if ((tmp_data & 1) == 1)
                    lcd_set_dot(ax, ay + bitcount);
                else
                    lcd_clr_dot(ax, ay + bitcount);
            }
            // 1 = transparent
            else if (write_mode == WRITE_TRANSPARENT)
            {
                if ((tmp_data & 1) == 1)
                    lcd_set_dot(ax, ay + bitcount);
            }
            // 2 = reverse
            else if (write_mode == WRITE_REVERSE)
            {
                if ((tmp_data & 1) == 1)
                    lcd_clr_dot(ax, ay + bitcount);
                else
                    lcd_set_dot(ax, ay + bitcount);
            }
            // 3 = transparent reverse
            else if (write_mode == WRITE_REVERSE_TRANSPARENT)
            {
                if ((tmp_data & 1) == 1)
                    lcd_clr_dot(ax, ay + bitcount);
            }

            tmp_data >>= 1;
        }
        // ��Ǩ�ͺ
        if (ax < MAX_X)
            ax++;
        font_size_count++;  	// Next Byte Counter
        font_data_index++;  	// Next	Byte Pointer
    }  
    // space 1 px
    if (ax < MAX_X)
        ax++;
}   

// ����¹�Ţ��úԤ���Ţ�� �����Ҵ
void lcd_change_num_2thai(unsigned char *str_number)
{
    unsigned char length,x;
    length = strlen(str_number);
    for (x = 0;x < length;x++)
    {    
        switch(str_number[x])
        {
            case '1': lcd_put_char_2vram('�', WRITE_TRANSPARENT);break;
            case '2': lcd_put_char_2vram('�', WRITE_TRANSPARENT);break;
            case '3': lcd_put_char_2vram('�', WRITE_TRANSPARENT);break;
            case '4': lcd_put_char_2vram('�', WRITE_TRANSPARENT);break;
            case '5': lcd_put_char_2vram('�', WRITE_TRANSPARENT);break;
            case '6': lcd_put_char_2vram('�', WRITE_TRANSPARENT);break;
            case '7': lcd_put_char_2vram('�', WRITE_TRANSPARENT);break;
            case '8': lcd_put_char_2vram('�', WRITE_TRANSPARENT);break;
            case '9': lcd_put_char_2vram('�', WRITE_TRANSPARENT);break;
            case '0': lcd_put_char_2vram('�', WRITE_TRANSPARENT);break;
            default : lcd_put_char_2vram( str_number[x], WRITE_TRANSPARENT);
        }
    }
}
// ��������¤������ �ҡ ax,ay ����ش
void lcd_put_stringf_2vram(flash unsigned char *string,unsigned char write_mode)
{          
    // Dummy Character Count
    unsigned char i=0;  					
    unsigned char tmpx,tmpy;
    unsigned char CharCount = strlenf(string);

    while(i<CharCount)  
    {
        tmpx = ax;
        tmpy = ay;
        // ������ѡ��  �� �� �� �� �� �� �����͹��Ѻ���¹ �٧�дѺ 1
        // 209(�ѹ�ҡ��), 212 - 215(�� - ��), 231(�������), 237-238(�Ԥ�Ե,���ä���)
        if (    (string[i] == 209) || (string[i] >= 212 && string[i] <= 215) ||
                (string[i] == 231) || (string[i] >= 237 && string[i] <= 238)    )
        {
            ax = ax - 5 > 0 ? ax - 6: ax;   // ź 5 ���Ǩ, ź 6 ���ԧ
            ay = ay - 3 > 0 ? ay - 3: ay;
            lcd_put_char_2vram(string[i], write_mode);	// Print 1-Char to LCD
            ax = tmpx;
            ay = tmpy;
        }
        // ������ѡ�� �������� �����͹��Ѻ���¹ �٧�дѺ 2
        // 232 - 236
        else if (   string[i] >= 232 && string[i] <= 236    )
        {
            ax = ax - 5 > 0 ? ax - 5: ax;   // ��ź 5 ���Ш��������ͧ�Դ˹���
            ay = ay - 5 > 0 ? ay - 5: ay;
            lcd_put_char_2vram(string[i], write_mode);	// Print 1-Char to LCD
            ax = tmpx;
            ay = tmpy;
        }
        // ����� �� �� ��(�Թ��) �����͹��Ѻ���¹ ����дѺ 1
        // 216 - 218
        else if (   string[i] >= 216 && string[i] <= 218    )
        {
            ax = ax - 5 > 0 ? ax - 6: ax;
            ay = ay + 2 > 0 ? ay + 2: ay;
            lcd_put_char_2vram(string[i], write_mode);	// Print 1-Char to LCD
            ax = tmpx;
            ay = tmpy;
        }
        // �͡�˹�ͨҡ��� �Ҵ����
        else
        {   
            // ��Ǩ�ͺ���˹觡�͹����Ҵ
            if (ax + 5 > MAX_X)
            { 
                ax = 0;
                if (ay + 16 > MAX_Y)
                    ay = THAI_LINE_0; 
                else
                    ay += 16;
            }
            lcd_put_char_2vram(string[i], write_mode);	// Print 1-Char to LCD
        }
        
        i++;                        // Next Character Print
    }  
}


// ��������¤������ �ҡ ax,ay ����ش 
// ����͹ lcd_put_stringf_2vram() �����Ҩҡ ram
void lcd_put_string_2vram(unsigned char *string,unsigned char write_mode)
{          
    // Dummy Character Count
    unsigned char i=0;  					
    unsigned char tmpx,tmpy;
    unsigned char CharCount = strlen(string);

    while(i<CharCount)  
    {
        tmpx = ax;
        tmpy = ay;
        // ������ѡ��  �� �� �� �� �� �� �����͹��Ѻ���¹ �٧�дѺ 1
        // 209(�ѹ�ҡ��), 212 - 215(�� - ��), 231(�������), 237-238(�Ԥ�Ե,���ä���)
        if (    (string[i] == 209) || (string[i] >= 212 && string[i] <= 215) ||
                (string[i] == 231) || (string[i] >= 237 && string[i] <= 238)    )
        {
            ax = ax - 5 > 0 ? ax - 6: ax;   // ź 5 ���Ǩ, ź 6 ���ԧ
            ay = ay - 3 > 0 ? ay - 3: ay;
            lcd_put_char_2vram(string[i], write_mode);	// Print 1-Char to LCD
            ax = tmpx;
            ay = tmpy;
        }
        // ������ѡ�� �������� �����͹��Ѻ���¹ �٧�дѺ 2
        // 232 - 236
        else if (   string[i] >= 232 && string[i] <= 236    )
        {
            ax = ax - 5 > 0 ? ax - 5: ax;   // ��ź 5 ���Ш��������ͧ�Դ˹���
            ay = ay - 5 > 0 ? ay - 5: ay;
            lcd_put_char_2vram(string[i], write_mode);	// Print 1-Char to LCD
            ax = tmpx;
            ay = tmpy;
        }
        // ����� �� �� ��(�Թ��) �����͹��Ѻ���¹ ����дѺ 1
        // 216 - 218
        else if (   string[i] >= 216 && string[i] <= 218    )
        {
            ax = ax - 5 > 0 ? ax - 6: ax;
            ay = ay + 2 > 0 ? ay + 2: ay;
            lcd_put_char_2vram(string[i], write_mode);	// Print 1-Char to LCD
            ax = tmpx;
            ay = tmpy;
        }
        // �͡�˹�ͨҡ��� �Ҵ����
        else
        {   
            // ��Ǩ�ͺ���˹觡�͹����Ҵ
            if (ax + 5 > MAX_X)
            { 
                ax = 0;
                if (ay + 16 > MAX_Y)
                    ay = THAI_LINE_0; 
                else
                    ay += 16;
            }
            lcd_put_char_2vram(string[i], write_mode);	// Print 1-Char to LCD
        }
        
        i++;                        // Next Character Print
    }  
}

// �Ҵ�ٻ�١�ê��ŧ
void draw_down_arrow(void)
{
    unsigned char x;
    for (x=40;x<49;x++)
        lcd_set_dot(x,THAI_LINE_2);
    for (x=41;x<48;x++)
        lcd_set_dot(x,THAI_LINE_2+1);
    for (x=42;x<47;x++)
        lcd_set_dot(x,THAI_LINE_2+2);
    for (x=43;x<46;x++)
        lcd_set_dot(x,THAI_LINE_2+3);
    lcd_set_dot(44,THAI_LINE_2+4);
    lcd_update_video_ram();
}