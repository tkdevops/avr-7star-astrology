

/* ��ǹ����ʴ��� component ��ҧ� */
#include <lcd-5110-thai.h>
#include <gui.h>
#include <matrixkeypad4x3.h>


#define true 1
#define false 0
 

// ��Ǩ�ͺ column ����ҡ���ҷ���к��������
unsigned char chk_column_between(unsigned char x,unsigned char y,unsigned char check)
{
    if (check > x && check < y)
        return true;
    return false;
}

// �Ҵ��ШѴ��� component ���������
// parameter x,y ���˹觷����Ҵ
// hh,mm,ss �� ��. �ҷ� �Թҷ�
// init_column ���˹觤���������
// return �繤�� ��ѡ 0 = ����, 99 = ���͡ , �͡��� = ���
unsigned char gui_time_component(unsigned char ax,unsigned char ay,
    unsigned char *hh, unsigned char *mm, unsigned char *ss,
    unsigned char init_column)
{           
    unsigned char tmp_value[7]; // [0] = free
    unsigned char x,y,xstart;
    unsigned char keypress;    
    unsigned char cmdexit = false;
    unsigned char current_column = init_column; // start column 1
    
    // �ŧ ��.
    tmp_value[1] = *hh / 10;
    tmp_value[2] = *hh % 10;
    // �ŧ �ҷ�
    tmp_value[3] = *mm / 10;
    tmp_value[4] = *mm % 10;
    // �ŧ �Թҷ�
    tmp_value[5] = *ss / 10;
    tmp_value[6] = *ss % 10;
    while ( chk_column_between(0,7,current_column) && !cmdexit)
    {
        keypress = get_keypad();
        // ��ҡ� ���� '1' ������ ���͡
        if ( keypress == '1' )
        {
            current_column = 99;
            cmdexit = true;
        }
        // ��ҡ����� ����
        else if ( keypress == '4')
        {
            //if ( chk_column_between(0,7,current_column - 1) )
                current_column -= 1;
        }
        // ��ҡ����� ���
        else if (keypress == '6')
        {
            //if ( chk_column_between(0,7,current_column + 1) )
                current_column += 1;
        }
        // ��ҡ����
        else if (keypress == '2')
        {
            switch(current_column)
            {
                //��ѡ�á������ 0-2
                case 1:
                    tmp_value[current_column] += 1;

                    if (tmp_value[current_column] == 2)
                    {   
                        if ( tmp_value[(unsigned char)(current_column + 1)] > 4 )
                            tmp_value[(unsigned char)(current_column + 1)] = 4;   
                    }
                    else if (tmp_value[current_column] > 2)
                    {
                        tmp_value[current_column] = 0;
                    }
                break;
                
                // ��Ѻ�Թ 3 ���������ѡ�á�� 2
                case 2:
                    if (tmp_value[current_column - 1] == 2)
                    {
                        if ( tmp_value[current_column] < 3)
                            tmp_value[current_column] += 1;
                        else
                            tmp_value[current_column] = 0;
                    }
                    // �����ѡ�á�� 0,1 ��Ѻ�黡��
                    else if (tmp_value[current_column] + 1 < 10)
                    {
                        tmp_value[current_column] += 1;
                    }
                    else
                    {
                        tmp_value[current_column] = 0;
                    }                        
                break;
                
                // ��ѡ 3,5 ��ѡ�á��������Թ 5
                case 3:
                case 5:
                    tmp_value[current_column] += 1;                    
                    if (tmp_value[current_column] > 5)
                        tmp_value[current_column] = 0;
                break;
                
                // ��ѡ 4,6 0-9 ����
                case 4:
                case 6:
                    // �����ѡ�á �� 0..5 ��ͻ�Ѻ����
                    if (tmp_value[current_column - 1] < 6)
                    {
                        if (tmp_value[current_column] + 1 < 10)
                            tmp_value[current_column] += 1;
                        else
                            tmp_value[current_column] = 0;
                    }
                break;
            }
        }
        // ��ҡ�ŧ
        else if (keypress == '5')
        {
            tmp_value[current_column] -= 1;
            switch(current_column)
            {
                //��ѡ�á������ 0-2
                case 1:
                    if (tmp_value[current_column] == 255)
                        tmp_value[current_column] = 2;
                    
                    if (tmp_value[current_column] == 2 && 
                        tmp_value[(unsigned char)(current_column + 1)] > 4 )
                        tmp_value[(unsigned char)(current_column + 1)] = 4;   
                break;
                
                // ��Ѻ�Թ 3 ���������ѡ�á�� 2
                case 2:
                    if (tmp_value[current_column] == 255)
                    {
                        if (tmp_value[current_column - 1] == 2)
                            tmp_value[current_column] = 3;
                        else
                            tmp_value[current_column] = 9;                            
                    }
                break;
                
                // ��ѡ 3,5 ��ѡ�á��������Թ 5
                case 3:
                case 5:
                    if (tmp_value[current_column] == 255)
                        tmp_value[current_column] = 5;
                break;
                
                // ��ѡ 4,6 0-9 ����
                case 4:
                case 6:
                    // �����ѡ�á �� 0..5 ��ͻ�Ѻ����
                    if (tmp_value[current_column] == 255)
                        tmp_value[current_column] = 9;
                break;
            }
        }
        
        // display // ���������ʴ����ҡ�͹���
        if (keypress != 'x' || init_column != 99)
        {
            init_column = 99; // �������ʴ��������
            
            // ź˹�Ҩ� ����ǳ����ͧ�ʴ���
            // ����� ax ���֧ 8(�ѡ�� hh:mm:ss) * 6(�������ҧ��� space px) + ax(�ش�����)
            for (x = ax; x < 8*6+ax; x++)
                for (y = ay; y < 9+ay; y++)
                    lcd_clr_dot(x,y);
            // draw new
            lcd_set_axay(ax,ay);
            sprintf(tmp_display_string,"%d%d:%d%d:%d%d", tmp_value[1], tmp_value[2],
                            tmp_value[3], tmp_value[4], tmp_value[5], tmp_value[6]);
            lcd_put_string_2vram(tmp_display_string, WRITE_TRANSPARENT);
            
            // draw cursor
            switch(current_column)
            {
                case 1:
                case 2:
                    xstart = (6*current_column)+ax-6;
                break;
                case 3:
                case 4:
                    xstart = (6*(current_column+1))+ax-6;
                break;
                case 5:
                case 6:
                    xstart = (6*(current_column+2))+ax-6;
                break;
            }
            for (x = xstart; x < xstart+6; x++)
                lcd_set_dot(x,ay+8);
                
            // update screen
            lcd_update_video_ram();
        }
    }
    
    *hh = tmp_value[1] * 10 + tmp_value[2];
    *mm = tmp_value[3] * 10 + tmp_value[4];
    *ss = tmp_value[5] * 10 + tmp_value[6];
    // clear cursor
    for (x = xstart; x < xstart+6; x++)
        lcd_clr_dot(x,ay+8);
    return current_column;
}

// �Ҵ��ШѴ��� component ������ѹ��� ���
// parameter x,y ���˹觷����Ҵ
// dd,mm,yy �� �ѹ ��͹ ��      xx/mmm/yyyy (�.�.)
// init_column ����������
// return �繤�� ��ѡ 0 = ����, 99 = ���͡, �͡���(8) = ���
unsigned char gui_thai_date_component(unsigned char ax,unsigned char ay,
    unsigned char *dd, unsigned char *mm, unsigned int *yy,
    unsigned char init_column)
{
    unsigned int tmp_calc;
    // xx/x/xxxx (7 column �Ţ��ѡ����ѡ�ѹ����� ���Ф���� 2 ��ʹ)
    // �ٻẺ 12/3/'2'567    
    unsigned char tmp_value[8]; // [0] = free  
    unsigned char x,y,xstart,xstop;
    unsigned char keypress;
    unsigned char cmdexit = false;    
    unsigned char current_column = init_column; // start column 1
    unsigned char next_column; 
    
    // �ŧ �ѹ
    tmp_value[1] = *dd / 10;
    tmp_value[2] = *dd % 10;
    // ����͹�����㹵����
    tmp_value[3] = (unsigned char)*mm;
    // �ŧ �������㹵����
    tmp_calc = *yy - 2000;
    tmp_value[4] = 2;
    tmp_value[5] = (unsigned char)(tmp_calc / 100);
    tmp_calc = tmp_calc - ((int)tmp_value[5]*100);
    tmp_value[6] = tmp_calc / 10;
    tmp_value[7] = tmp_calc % 10;
    while ( chk_column_between(0,8,current_column) && !cmdexit)
    {
        keypress = get_keypad();            
        // ��ҡ� ���� '1' ������ ���͡
        if ( keypress == '1' )
        {
            current_column = 99;
            cmdexit = true;
        }
        // ��ҡ����� ����
        else if ( keypress == '4')
        {
            current_column -= 1;
            next_column = current_column +1;
            if (current_column == 4)
            {
                current_column -= 1;
                next_column = current_column -1;
            }
        }
        // ��ҡ����� ���
        else if (keypress == '6')
        {
            current_column += 1;
            next_column = current_column +1;
            if (current_column == 4)
            {
                current_column += 1;
                next_column = current_column +1;
            }
        }
        // ��ҡ����
        else if (keypress == '2')
        {
            switch(current_column)
            {
                //��ѡ�á������ 0-3
                case 1:
                    tmp_value[current_column] += 1;
                    // ��Ǩ����� 3 �������
                    if (tmp_value[current_column] == 3)
                    {   
                        if ( tmp_value[next_column] > 1 )
                            tmp_value[next_column] = 1;   
                    }
                    else if (tmp_value[current_column] > 3)
                    {
                        tmp_value[current_column] = 0;
                    }
                break;
                
                // ��Ѻ�Թ 1 ���������ѡ�á�� 3
                case 2:                    
                    if (tmp_value[current_column - 1] == 3)
                    {
                        if ( tmp_value[current_column] < 1)
                            tmp_value[current_column] += 1;
                        else
                            tmp_value[current_column] = 0;
                    }
                    // �����ѡ�á�� 0,1,2 ��Ѻ�黡��
                    else if (tmp_value[current_column] + 1 < 10)
                    {
                        tmp_value[current_column] += 1;
                    }
                    else
                    {
                        tmp_value[current_column] = 0;
                    }
                break;
                
                // ��ѡ 3 ����ѡ��͹ ������ 1 - 12
                case 3:
                    tmp_value[current_column] += 1;                    
                    if (tmp_value[current_column] > 12)
                        tmp_value[current_column] = 1;                    
                break;
                // ��ѡ 4 ��ѡ�ѹ �����
                case 4:
                break;
                
                // ��ѡ 5 (��ѡ���¢ͧ��) ������ 4-5 
                case 5:
                    tmp_value[current_column] += 1;
                    // �����ѡ������ 5 ��ѡ�Ժ�Թ 7 ����Ѻ
                    if (tmp_value[current_column] == 5 && tmp_value[next_column] >= 7)
                    {
                        tmp_value[next_column] = 7;
                        // �����ѡ˹����ҡ���� 5 ���¡��ͧ��Ѻ�� 5
                        if (tmp_value[(int)next_column + 1] > 5)
                            tmp_value[(int)next_column + 1] = 5;
                    }
                    // ����Թ 5 ������������
                    if (tmp_value[current_column] > 5)
                        tmp_value[current_column] = 4;
                break;
                // ��ѡ 6 (��ѡ�Ժ), �����ѡ������ 5 ��ѡ�Ժ�������� 0-7 
                case 6:
                     tmp_value[current_column] += 1;                                          
                     // �����ѡ���� �� 5 ��ѡ�Ժ���� 7 ��ѡ˹�������Թ 5
                     if (tmp_value[current_column-1] == 5 && tmp_value[current_column] == 7)
                     {
                         // ��ѡ˹��¨������� 0 -5 ���л����٧�ش��� 2575
                         if (tmp_value[next_column] > 5)
                             tmp_value[next_column] = 5;                         
                     }
                     // �����ѡ���� �� 5 ��ѡ�Ժ�������� 0-7
                     else if (tmp_value[current_column-1] == 5 && tmp_value[current_column] > 7)
                     {
                         // ��ѡ�Ժ������ 7
                         tmp_value[current_column] = 0;
                     }
                     else
                     {
                         // ��ѡ�Ժ ������ 0-9 �������
                         if (tmp_value[current_column] > 9)
                             tmp_value[current_column] = 0;
                     }                         
                break;
                
                // ��ѡ 7 (˹���)
                case 7:
                    // �����ѡ 100 = 5 ��ѡ�Ժ = 7 ��ѡ˹������� 0-5
                    if (tmp_value[(int)current_column-2] == 5 &&
                        tmp_value[(int)current_column-1] == 7)
                    {
                        // ������Թ 5
                        if (tmp_value[current_column] + 1 < 6)
                            tmp_value[current_column] += 1;
                        else
                            tmp_value[current_column] = 0;
                    }
                    //  �͡��� 0-9 ����
                    else
                    {     
                        if (tmp_value[current_column] + 1 < 10)
                            tmp_value[current_column] += 1;
                        else
                            tmp_value[current_column] = 0;
                    }
                break;
            }
        }
        // ��ҡ�ŧ
        else if (keypress == '5')
        {
            // copy �ҡ��� '2' �� �����������ź -
            tmp_value[current_column] -= 1;
            switch(current_column)
            {
                //��ѡ�á������ 0-3
                case 1:
                    if (tmp_value[current_column] == 255)
                        tmp_value[current_column] = 3;
                    
                    // ��Ǩ����� 3 �������
                    if (tmp_value[current_column] == 3)
                    {   
                        if ( tmp_value[next_column] > 1 )
                            tmp_value[next_column] = 1;   
                    }
                    else if (tmp_value[current_column] > 3)
                    {
                        tmp_value[current_column] = 0;
                    }
                break;
                
                // ��Ѻ�Թ 1 ���������ѡ�á�� 3
                case 2:                    
                    if (tmp_value[current_column - 1] == 3)
                    {
                        if (tmp_value[current_column] == 255)
                            tmp_value[current_column] = 1;
                    }
                    // �����ѡ�á�� 0,1,2 ��Ѻ�黡��
                    else if (tmp_value[current_column] == 255)
                    {
                        tmp_value[current_column] = 9;
                    }
                break;
                
                // ��ѡ 3 ����ѡ��͹ ������ 1 - 12
                case 3:
                    if (tmp_value[current_column] == 0)
                        tmp_value[current_column] = 12;                    
                break;
                // ��ѡ 4 ��ѡ�ѹ �����
                case 4:
                break;
                
                // ��ѡ 5 (��ѡ���¢ͧ��) ������ 4-5 
                case 5:
                    // ��ҵ�ӡ��� 4 ������������
                    if (tmp_value[current_column] < 4)
                        tmp_value[current_column] = 5;
                        
                    // �����ѡ������ 5 ��ѡ�Ժ�Թ 7 ����Ѻ
                    if (tmp_value[current_column] == 5 && tmp_value[next_column] >= 7)
                    {
                        tmp_value[next_column] = 7;
                        // �����ѡ˹����ҡ���� 5 ���¡��ͧ��Ѻ�� 5
                        if (tmp_value[(int)next_column + 1] > 5)
                            tmp_value[(int)next_column + 1] = 5;
                    }
                break;
                // ��ѡ 6 (��ѡ�Ժ), �����ѡ������ 5 ��ѡ�Ժ�������� 0-7 
                case 6:
                    // ��Ѻ
                    if (tmp_value[current_column-1] == 5 && tmp_value[current_column] == 255)
                    {
                            tmp_value[current_column] = 7;
                    }

                    if (tmp_value[current_column] == 255)
                        tmp_value[current_column] = 9;
                     
                     // ��Ǩ
                     // �����ѡ���� �� 5, ��ѡ�Ժ�� 7 ��ѡ˹��¨�����Թ 5
                     if (tmp_value[current_column-1] == 5 && tmp_value[current_column] == 7)
                     {
                         // ��ѡ˹��¨������� 0 -5 ���л����٧�ش��� 2575
                         if (tmp_value[next_column] > 5)
                             tmp_value[next_column] = 5;                         
                     }
                break;
                
                // ��ѡ 7 (˹���)
                case 7:
                    // �����ѡ 100 = 5 ��ѡ�Ժ = 7 ��ѡ˹������� 0-5
                    if (tmp_value[(int)current_column-2] == 5 &&
                        tmp_value[(int)current_column-1] == 7)
                    {
                        // ������Թ 5
                        if (tmp_value[current_column] == 255)
                            tmp_value[current_column] = 5;
                    }
                    //  �͡��� 0-9 ����
                    else
                    {     
                        if (tmp_value[current_column] == 255)
                            tmp_value[current_column] = 9;
                    }
                break;
            }                
        }
        
        // display // ���������ʴ����ҡ�͹���
        if (keypress != 'x' || init_column != 99)
        {
            init_column = 99; // �������ʴ��������
            
            // ź˹�Ҩ� ����ǳ����ͧ�ʴ���
            // ����� ax ���֧ 11(�ѡ�� mm/ddd/yyyy) * 6(�������ҧ��� space px) + ax(�ش�����)
            for (x = ax; x < 11*6+ax; x++)
                for (y = ay-6; y < 9+ay; y++)
                    lcd_clr_dot(x,y);
                    
            // draw new
            lcd_set_axay(ax,ay);
            // �ʴ��ѹ
            sprintf(tmp_display_string,"%d%d/", tmp_value[1], tmp_value[2]);
            lcd_put_string_2vram(tmp_display_string, WRITE_TRANSPARENT);
            // �ʴ���͹            
            lcd_put_stringf_2vram(THAI_MONTH_TBL[ tmp_value[3] ],WRITE_TRANSPARENT);
            // �ʴ���
            sprintf(tmp_display_string,"/2%d%d%d", tmp_value[5], tmp_value[6], tmp_value[7]);
            lcd_put_string_2vram(tmp_display_string, WRITE_TRANSPARENT);
            
            // draw cursor
            switch(current_column)
            {
                // �ѹ
                case 1:
                case 2:
                    xstart = (6*current_column)+ax-6;
                    xstop = xstart+6;
                break;
                
                // ��͹
                case 3:
                    xstart = (6*(current_column+1))+ax-6;
                    xstop = xstart+12;
                break;
                
                // xx/ddd/x456
                // ��ѡ ���� �Ժ ˹��� �ͧ�� 
                case 4:
                case 5:
                case 6:
                case 7:
                    xstart = (6*(current_column+4))+ax-6;
                    xstop = xstart+6;
                break;
            }
            for (x = xstart; x < xstop; x++)
                lcd_set_dot(x,ay+8);
                
            // update screen
            lcd_update_video_ram();
        }
    }
    *dd = tmp_value[1] * 10 + tmp_value[2];
    *mm = tmp_value[3];
    *yy = 2000U + ((int)tmp_value[5] * 100) + ((int)tmp_value[6] * 10) + tmp_value[7];
    // clear cursor
    for (x = xstart; x < xstop; x++)
        lcd_clr_dot(x,ay+8);        
    return current_column;    
}


