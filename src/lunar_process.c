/*
 * ��кҷ���稾�����ҸԺ������Թ�����Ǫ����ظ� ������خ�������������� �繾����ҡ�ѵ�����Ѫ��ŷ�� � ��觾�к���Ҫ�ѡ��ǧ�� 
 * �ʴ稾���Ҫ��������� �ѹ����� ��͹��� ��� � ��� �����ç �ç�Ѻ�ѹ��� � ���Ҥ� �.�. ���� 
 * �繾���Ҫ���ʾ��ͧ���� �� 㹾�кҷ���稾�Ш�Ũ���������������� 
 * �����Ҫ���ѵ�������ѹ������� �� ���Ҥ� �ը� �ط��ѡ�Ҫ ���� 
 * ����ʴ����ä�������ѹ��� �� ��Ȩԡ�¹ �.�. ���� �����Ъ������� �� ����� �ʴ稴�ç�Ҫ���ѵ���� �� ��
 * 
 */

//##########################################################################
//
// Programname : LunarDateCalc
// Objective :
//   - ������ӹǹ��ԷԹ�ѹ�ä�� 176 �� �����  1/4/2400 - 31/3/2575
//   - ��������¹���� code ������������Ҩе�ͧ port 价�� Microcontroller
// Programmer : BenNueng
// Email : BenNueng@Gmail.com
// StartDate : 24/06/2551 15:32
// StopDate : 25/06/2551 22:32
//
// links :
// - �������¹���Ҿط��ѡ�Ҫ : http://th.wikipedia.org/wiki/�ط��ѡ�Ҫ
// - �ʴ�������ҧ��õѴ��ԷԹ : http://www.horauranian.com/index.php?lay=boardshow&ac=webboard_show&Category=horauraniancom&thispage=7&No=254466
//
//##########################################################################

#include <lunar_process.h>

// ��С�Ȥ�Ҥ����� flash

// �ѹ㴪�������ѹ���� , ��ͧ�á�� free ���͡�˹��������
// 8 = �ӹǹ��, 3 = ������ǵ���ѡ�÷����Ƿ���ش(2) + '\0'
flash unsigned char ENG_DAY_TBL[8][3] = {"fr","Su","Mo","Tu","We","Th","Fr","Sa"};

// ���ҧ�纪����ѹ��� Ẻ���� , ��ͧ�á�� free
flash unsigned char THAI_DAY_TBL[8][3] = {"fr","��"," �"," �"," �","��"," �"," �"};

// ���ҧ�纪�����͹�� Ẻ���� , ��ͧ�á�� free
flash unsigned char THAI_MONTH_TBL[13][5] = {"free","��.","��.","�դ.","���","��.","���.","��.","ʤ.","��.","��.","��.","��."};
                         
// ���ҧ�纪�����͹�ҡ� Ẻ���� , ��ͧ�á�� free
flash unsigned char ENG_MONTH_TBL[13][4] = {"fre","Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"};

// ���ҧ�纪��ͻչѡ�ѵ� , ��ͧ�á�� free
flash unsigned char ANIMAL_YEAR_TBL[13][7] = {"free","�Ǵ","���","���","���","���ç","�����","������","����","�͡","�С�","��","�ع"};

// �������͹ (�ҡ�) ��ա���ѹ , ��ͧ�á�� 0 ���͡�˹��������
flash unsigned char TOTAL_DAY_TBL[] = {0,31,28,31,30,31,30,31,31,30,31,30,31};

// �������͹ (��) ���͹� �ա���ѹ , ��ͧ�á�� 0 �����������
flash unsigned char LUNAR_DAY_TBL[] = {0,14,15,14,15,14,15,14,15,14,15,14,15};

        /* original
        int[] LUNAR_YEAR_TBL = new int[]{
        0,2,0,1,2,0,2,0,1,2,0,0,2,0,1,2,0,2,0,1,
        2,0,0,2,1,2,0,0,2,0,1,2,0,2,0,1,2,0,0,2,
        0,1,2,0,2,0,1,2,0,0,2,1,2,0,0,2,0,1,2,0,
        1,2,0,2,0,0,2,0,1,2,0,2,1,0,2,0,1,2,0,1,
        2,0,2,0,0,2,0,2,1,0,2,0,1,2,0,1,2,0,0,2,
        1,2,0,0,2,0,1,2,0,2,0,0,2,1,0,2,1,0,2,0,
        2,0,1,2,0,0,2,0,2,0,1,2,0,1,2,0,2,0,0,2,
        1,0,2,1,0,2,0,2,0,1,2,0,1,2,0,2,0,1,2,0,
        0,2,0,0,2,0,2,0,1,2,0,0,2,1,2,0};
        */

        /*
         * ����ó� ������ѹ��� 1/1/2484 - 31/3/2575
         * ���ҧ��˹���һչ��� �繻ջ������
         * 0 = ������� �������
         * 1 = ������� ͸ԡ���
         * 2 = ͸ԡ��� �������
         int []LUNAR_YEAR_TBL = new int[]{
            0, // ����� 1/1/2484
            0,2,0,2,1,0,2,0,1,2,0,1,2,0,0,2,1,2,  
            0,0,2,0,1,2,0,2,0,0,2,1,0,2,1,0,2,0,
            2,0,1,2,0,0,2,0,2,0,1,2,0,1,2,0,2,0,0,2,
            1,0,2,1,0,2,0,2,0,1,2,0,1,2,0,2,0,1,2,0,
            0,2,0,0,2,0,2,0,1,2,0,0,2,1,2,0};
         */

// 1/1/2423 - 31/3/2575
flash unsigned char LUNAR_YEAR_TBL[] = {
            2,1,2,0,0,2,0,1,2,0,2,0,1,2,0,
            0,2,0,1,2,0,2,0,1,2,0,0,2,1,2,
            0,0,2,0,1,2,0,1,2,0,2,0,0,2,0,
            1,2,0,2,1,0,2,0,1,2,0,1,2,0,2,
            0,0,2,0,2,1,0,2,0,1,2,0,1,2,0,
            0,2,1,2,0,0,2,0,1,2,0,2,0,0,2,
            1,0,2,1,0,2,0,2,0,1,2,0,0,2,0,
            2,0,1,2,0,1,2,0,2,0,0,2,1,0,2,
            1,0,2,0,2,0,1,2,0,1,2,0,2,0,1,
            2,0,0,2,0,0,2,0,2,0,1,2,0,0,2,
            1,2,0,99};

// ��˹���Ңͧ �շ���ҡ����ش�������ö���� (��ͤ�� year_table ����ͧ)            
// �����¡��ǹ�ٻ�ͺ ���ǵ�Ǩ�շ��ŧ���´��� 99 (��蹤�ͨش����ش)
unsigned int MAX_YEAR = 0;


    /* ======================== ����� Global �������к� ============================ */
    // ��С�ȵç���������� compile ��ҹ��� ���з�� process ���ա�á�˹��������ա����
    
		    /* //����ó������ ��Ѻ�� 1/1/2484
		    //# ��˹�����������
		    unsigned char m_day_name = 4;  //# �����ѹ �������Ф��ʵ������͹�ѹ
		    unsigned char m_day_no = 1; //# �ѹ��� �������Ф��ʵ������͹�ѹ
		    unsigned char m_month_no = 1; //# ��͹�ҡ�

		    unsigned int m_christ_year = 1941; //# ���ҡ�
		    unsigned int m_thai_year = 2484; //# �վ.�.
		    
		    bool m_phase_up = true; //# ��ҧ���(true) ���͢�ҧ���(false)
		    unsigned char m_lunar_phase = 4; //# �Զ�
		    unsigned char m_thai_month = 2; //# ��͹�� ����ѹ�ä��
		    unsigned char m_animal_year = 5; //# �չѡ�ѵ� ��������� ���ç
		    
		    bool month_twiced = false; //# ��˹���ùѺ��͹ 8 2 ����

		    //# ���Ի�ԷԹ�¨�����¹�� �.�. ����� ��͹ 4, ���Ҽ�ҹ�� 2483 仨�����¹����͹ 1 ����͹���ʵ�
		    unsigned char m_change_year_month = 1; //# 4
		    */

    //# ��˹�����������  ������� 1/1/2423
    // ����õ�ҧ� ��С���Ҩҡ header
    unsigned char m_day_name = 7;  //# �����ѹ �������Ф��ʵ������͹�ѹ
    unsigned char m_day_no = 1; //# �ѹ��� �������Ф��ʵ������͹�ѹ
    unsigned char m_month_no = 1; //# ��͹�ҡ�

    unsigned int m_thai_year = 2423; //# �վ.�.
    unsigned int m_christ_year = 1881; //# ���ҡ�           

    bool m_phase_up = true; //# ��ҧ���(true) ���͢�ҧ���(false)
    unsigned char m_lunar_phase = 2; //# �Զ�
    unsigned char m_thai_month = 2; //# ��͹�� ����ѹ�ä��
    unsigned char m_animal_year = 5; //# �չѡ�ѵ� ��������� ���ç
    
    bool month_twiced = false; //# ��˹���ùѺ��͹ 8 2 ����
    unsigned int m_year_count = 0; // ��ǹѺ�ͺ ��ùѺ��
    /* ========================== end =================================== */


// ���Ҥ��㹵��ҧ ���ʹ���Ҥ���٧�ش�ͧ�դ������
void setup_max_year(void)
{
    unsigned int count = 0;
    if (MAX_YEAR == 0)
    {
        for(count = 0; LUNAR_YEAR_TBL[count] != 99; count++);
        MAX_YEAR = count;
    }
}

//# ��Ǩ�ͺ��һչ��� ��͹ ��.�� 29 �ѹ������� (�� �.�.)
unsigned char is_leap_year(unsigned int year)
{
    //# �����ô��� 4 ŧ��ǵ�ͧ�ҾԨ�ó��ա����..
    if ((year % 4) == 0)
    {
    	//# �����ô��� 100 ŧ����ա ��ͧ�Ԩ�ó��ա
	    if ((year % 100) == 0)
	        //# �����ô��� 400 ŧ����ա �ʴ������ ͸ԡ��÷Թ
	        return (year % 400 == 0);
	    else
	        //# �����ô��� 100 ���ŧ��� �ʴ�����繻�͸ԡ��÷Թ
	        return true;
    }
    //# �����ô��� 4 ���ŧ��� �繻ջ��� ��͹
    return false;
}

//# ����¹ �� �.�. �� �.�.
unsigned int change_b2c(unsigned char month, unsigned int thai_year)
{
    unsigned int year = thai_year;
    if (year >= 2483)
    {
        year = year - 543;
    }
    else
    {
    	if ((month >= 1) && (month <= 3))
	        year = year - 542;
	    else
    	    year = year - 543;
    }
    return year;
}
                          
//# ��Ǩ�ͺ Ǵ� ��Ҷ١��ͧ�������
unsigned char chk_valid_date(unsigned char day, unsigned char month, unsigned int year)
{
    //# ��͹�ŧ�繻� �.�.
    unsigned int cyear = change_b2c(month,year);
    
    //# 1. ���ѹ�������ը�ԧ������� (1 �.�. - 31 �դ. 2483)
    if ((year == 2483) && (month >= 1 && month <= 3))
        return 0; //# "error : date not found"
    
    //# 2. �ѹ����Թ��˹�������� 2400 <= x <= 2575
    if ((year < 2400) || (year > 2575))
        return 10; //# "error : date overflow"
    
    //# 3. ������ѹ�������ը�ԧ㹻� 2400 (�չ������ ��͹ 1,2,3 �͢����͹ 4 ���繻� 2401)
    if ((year == 2400) && (month >= 4 && month <= 12))
        return 20;
    
    //# ��Ǩ�ͺ�����١��ͧ
    //# 3. ���ѹ��� 29 �.�. ���͹���������������
    if (!is_leap_year(cyear) && (day == 29 && month == 2))
	    return 30; // # "error : 29 mar in leap year"

    return true;
}

//# ��Ǩ����ѹ���Ѩ�غѹ���� �ç�Ѻ�ѹ����ͧ����������
unsigned char find_date(unsigned char fday,unsigned char fmonth,unsigned int fyear,unsigned char day,unsigned char month,unsigned int year)
{
    return ((fday == day) && (fmonth == month) && (fyear == year));
}

// bug ��ͧ��Ǩ��ҧ���/��� ����
//# ��Ǩ��� �ѹ�Ѩ�غѹ���ѹ����������                                       
// waxing_moon = true = ��ҧ���
// waxing_moon = false = ��ҧ���
unsigned char chk_monk_day(unsigned char waxing_moon, unsigned char lunar_phase,unsigned char thai_month,unsigned char adikawan_year)
{
    // ������Ѻ !!
    // ��� 14 �����͹ 7 ����繻�͸ԡ��� (���� 30�ѹ) ��������ѹ���
    if (!waxing_moon && lunar_phase == 14 && thai_month == 7 && adikawan_year)
    {
        return false;
    }
    // ������ѹ���������� 15 ��� ���� 8 ���
    if (lunar_phase == 15 || lunar_phase == 8)
        return true;    
    
    // �������� 14 �����͹�Ţ��� �����ѹ���
    if ( !waxing_moon &&  (lunar_phase == 14) && (thai_month % 2 == 1) )
        return true;
        
    return false;
}


//# �����ѹ����к� ������ѹ���� , arg year = �.�. Ẻ��, �ѹ��� ��͹Ẻ�ҡ�
//# �� function ���ŧ�.�. �� �.�. ���ǹ����令ӹǹ�ͧ���ѵ��ѵ�
unsigned char process(unsigned char day,unsigned char month,unsigned int year)
{
    //# ���Ի�ԷԹ�¨�����¹�� �.�. ����� ��͹ 4, ���Ҽ�ҹ�� 2483 仨�����¹����͹ 1 ����͹���ʵ�
    unsigned char m_change_year_month = 4;
    // �纤�ҡ�õ�Ǩ �ѹ������ �١��ͧ�������
    unsigned char result_chk_valid_date = 0;

    // ��ҷ���ͧ����颳�������ٻ
    bool change_month = false;  //# �纤�ҡ�õ�Ǩ�ͺ��Ҩе�ͧ������͹�������
    unsigned long int	total_day_count = 0; // count all day at start
    unsigned char chk_ram = 14;  // ��͹��Ǩ ��ҧ���
    
    /* ======================== ����� Global �������к� ============================ */
    
		    /* //����ó������ ��Ѻ�� 1/1/2484
		    //# ��˹�����������
		    unsigned char m_day_name = 4;  //# �����ѹ �������Ф��ʵ������͹�ѹ
		    unsigned char m_day_no = 1; //# �ѹ��� �������Ф��ʵ������͹�ѹ
		    unsigned char m_month_no = 1; //# ��͹�ҡ�

		    unsigned int m_christ_year = 1941; //# ���ҡ�
		    unsigned int m_thai_year = 2484; //# �վ.�.
		    
		    bool m_phase_up = true; //# ��ҧ���(true) ���͢�ҧ���(false)
		    unsigned char m_lunar_phase = 4; //# �Զ�
		    unsigned char m_thai_month = 2; //# ��͹�� ����ѹ�ä��
		    unsigned char m_animal_year = 5; //# �չѡ�ѵ� ��������� ���ç

		    //# ���Ի�ԷԹ�¨�����¹�� �.�. ����� ��͹ 4, ���Ҽ�ҹ�� 2483 仨�����¹����͹ 1 ����͹���ʵ�
		    unsigned char m_change_year_month = 1; //# 4
		    */

    //# ��˹�����������  ������� 1/1/2423
    // ����õ�ҧ� ��С���Ҩҡ header
    m_day_name = 7;  //# �����ѹ �������Ф��ʵ������͹�ѹ
    m_day_no = 1; //# �ѹ��� �������Ф��ʵ������͹�ѹ
    m_month_no = 1; //# ��͹�ҡ�

    m_thai_year = 2423; //# �վ.�.
    m_christ_year = 1881; //# ���ҡ�           

    m_phase_up = true; //# ��ҧ���(true) ���͢�ҧ���(false)
    m_lunar_phase = 2; //# �Զ�
    m_thai_month = 2; //# ��͹�� ����ѹ�ä��
    m_animal_year = 5; //# �չѡ�ѵ� ��������� ���ç
    
    month_twiced = false; //# ��˹���ùѺ��͹ 8 2 ����
    /* ========================== end =================================== */
    
    // ������������� ���٧�ش�������� �ա���
    setup_max_year();
    //# ��Ǩ�����١��ͧ
    result_chk_valid_date = chk_valid_date(day,month,year);
    if (result_chk_valid_date == true)
    {
        //# ��������ǹ�ͺ�ӹǹ �����Ҩ������ҷ�����ԧ�͡�� !!       
        bool found_date = find_date(m_day_no, m_month_no, m_thai_year, day, month, year);        
        m_year_count = 0; //# ��ǹѺ�ͺ��ҵ͹������� ������������ lunar_year_type ��ͧ�
        month_twiced = false; //# ��˹���ùѺ��͹ 8 2 ����    
        
        while ((m_year_count < MAX_YEAR) && (!found_date))
	    {
	        // ��Ǩ�Ѻ��������ҷ���������ѹ (�ѧ��������������ª������)
	        total_day_count++;

	        //# ===================== �ӹǹ Ǵ� �ҡ� ================================
	        //# �����ѹ �������Ф��ʵ������͹�ѹ
	        m_day_name += 1;
	        if (m_day_name > 7)
	        {
		        m_day_name = 1;
	        }
            change_month = false;  //# �纤�ҡ�õ�Ǩ�ͺ��Ҩе�ͧ������͹�������
	        //# +�ѹ��� (�������Ф��ʵ������͹�ѹ)
	        m_day_no += 1;
	        //# ����� ��͹ 2 (�.�.) ��лչ���� 29 �ѹ
	        if (m_month_no == 2 && is_leap_year(m_christ_year))
	        {
		        if (m_day_no > 29)  //# ��Ǩ��� 29
		            change_month = true;
	        }
	        else
	        {
		        //# �������͹���� ��Ǩ������, ����ҧ�������
		        if (m_day_no > TOTAL_DAY_TBL[m_month_no])
		            change_month = true;
            }

	        //# ����͵�Ǩ�ͺ������� ��ͧ������͹�ҡ�
	        if (change_month == true)
	        {
		        //# reset �ѹ ��Ѻ���ѹ��� 1 ����͹���
		        m_day_no = 1;
		        //# ����¹��͹����
		        m_month_no += 1;
		        if (m_month_no > 12)
		        {
		            m_month_no = 1;
		            //# ������ �ҡ�
		            m_christ_year += 1; //# ���ҡ�
		        }
	        }                   

	        //# ===================== �ӹǹ Ǵ� �.�. ===================================
	        //# ���Ի�ԷԹ�¨�����¹�� �.�. ����� ��͹ 4 (m_change_year_month = 4)
	        //# ���Ҽ�ҹ�� 2483 仨�����¹����͹ 1 ����͹���ʵ�
	        //#
	        if ((m_day_no == 1) && (m_month_no == m_change_year_month))
	        {
		        m_thai_year += 1; //# + �վ.�.
	        }

	        //# ����� 31 �.�. 2483 �ѹ����¹ �.�.����ѹ��� 1 �.�. �ء��
	        if ((m_day_no == 31) && (m_month_no == 12 && m_thai_year == 2483))
		        m_change_year_month = 1;

	        //# ===================== �ӹǹ Ǵ� �ѹ�ä�� ================================
	        //# +�Զ�
	        m_lunar_phase += 1;
	        //# ��� + ���Թ�� 16 ��Ӣ�ҧ��� �������¹�繢�ҧ���
	        if ((m_phase_up == true) && (m_lunar_phase > 15))
	        {
		        m_phase_up = false;
		        m_lunar_phase = 1;
	        }

	        //# ��˹������������ͧ��õ�Ǩ�ͺ
	        chk_ram = 14;
	        //# ����繢�ҧ��� ��ͧ�������͹�������������� ��������¹�繢�ҧ���
	        //# ͸ԡ��� ���� ��� 15 ���
	        if ((LUNAR_YEAR_TBL[m_year_count] == 1) && (m_thai_month == 7))
		        chk_ram = 15;
	        else
		        //# ������� ���� ͸ԡ����ѧ�����ٻẺ���
		        chk_ram = LUNAR_DAY_TBL[m_thai_month];

	        //# ����Ң�ҧ����ҡ���ҷ���������ѧ ����Թ�����������¹�� ��ҧ���
	        if ((m_phase_up == false) && (m_lunar_phase > chk_ram))
	        {
		        //# ����¹�繢�ҧ���
		        m_phase_up = true;
		        //# 1 ���
		        m_lunar_phase = 1;
		        //# �����͹����
		        m_thai_month += 1;
		        //# 㹡óշ�� ��Ẻ ͸ԡ��� �������͹ 8(+1=9) �������¹Ѻ�ҡ�͹ ����ͧ�����͹����
		        if ((LUNAR_YEAR_TBL[m_year_count] == 2) && (m_thai_month == 9) && (month_twiced == false))
		        {
		            month_twiced = true;
		            m_thai_month -= 1;
		            //# �� clear ����͢�� m_year_count ����
		        }
		
		        //# �������͹ 5 �������¹�չѡ�ѵ�
		        if (m_thai_month == 5)
		        {
		            //# �ǡ���ҧ�ѹ��� ��Шӻ������ա 1
		            m_year_count += 1;

		            //# �ǡ�չѡ�ѵ�
		            m_animal_year += 1;
		            //# ��һչѡ�ѵ��ҡ���� 12 �����͹���� 1
		            if (m_animal_year > 12)
			            m_animal_year = 1;
		    
		            //# clear ��ùѺ�����͹Ỵ 2 ����
		            month_twiced = false;
		        }
		        //# �����͹���ҡ���� 12 �����͹���� 1
		        if (m_thai_month > 12)
		        {
		            m_thai_month = 1;
		        }
	        }
	    
	        //# ��º����ѹ�Ѩ�غѹ�� �ѹ���ç������� (�.�.)
	        found_date = find_date(m_day_no, m_month_no, m_thai_year, day, month, year);
	    } // while
	    //# ��Ҥú����٧�ش ��������� �ʴ���ҼԴ��Ҵ
	    if (m_year_count >= MAX_YEAR)
	        return 40; //# "error 10: date overflow"
	
	    //# �ç����ʴ������ !!                                   	    
	        //sprintf(display_line[0],"%d/%d/%d(%d)\0    ",m_day_no,m_month_no,m_thai_year,m_christ_year);	    
	        /* work ! */
	        //tmp_string_len = strlenf(ENG_DAY_TBL[m_day_name]);
	        //memcpyf(tmp_string, ENG_DAY_TBL[m_day_name], tmp_string_len );
	        //sprintf(display_line[1],"%s(%d)            ",tmp_string, m_day_name);
	        //sprintf(display_line[2],"ph = %d(%c%d)%d        ",m_day_name, m_phase_up ? '+' :'-',m_lunar_phase,m_thai_month);
	        //sprintf(display_line[3],"ch zodiac = %d       ",m_animal_year);
        return 1; //# error 1 = found !
    } // main if
    
    return result_chk_valid_date ; //# error from date valid: 
}

