/*
 * ��кҷ���稾�����ҸԺ������Թ�����Ǫ����ظ� ������خ�������������� �繾����ҡ�ѵ�����Ѫ��ŷ�� � ��觾�к���Ҫ�ѡ��ǧ��
 * �ʴ稾���Ҫ��������� �ѹ����� ��͹��� ��� � ��� �����ç �ç�Ѻ�ѹ��� � ���Ҥ� �.�. ����
 * �繾���Ҫ���ʾ��ͧ���� �� 㹾�кҷ���稾�Ш�Ũ����������������
 * �����Ҫ���ѵ�������ѹ������� �� ���Ҥ� �ը� �ط��ѡ�Ҫ ����
 * ����ʴ����ä�������ѹ��� �� ��Ȩԡ�¹ �.�. ���� �����Ъ������� �� ����� �ʴ稴�ç�Ҫ���ѵ���� �� ��
 *
 */

//##########################################################################
//
// Programname : LunarDateCalc
// Objective :
//   - ������ӹǹ��ԷԹ�ѹ�ä�� 176 �� �����  1/4/2400 - 31/3/2575
//   - ��������¹���� code ������������Ҩе�ͧ port 价�� Microcontroller
// Programmer : BenNueng
// Email : BenNueng@Gmail.com
// StartDate : 24/06/2551 15:32
// StopDate : 25/06/2551 22:32
//
// links :
// - �������¹���Ҿط��ѡ�Ҫ : http://th.wikipedia.org/wiki/�ط��ѡ�Ҫ
// - �ʴ�������ҧ��õѴ��ԷԹ : http://www.horauranian.com/index.php?lay=boardshow&ac=webboard_show&Category=horauraniancom&thispage=7&No=254466
//
//##########################################################################


// include part
#include <mega32.h>
	#ifndef __SLEEP_DEFINED__
	#define __SLEEP_DEFINED__
	.EQU __se_bit=0x80
	.EQU __sm_mask=0x70
	.EQU __sm_powerdown=0x20
	.EQU __sm_powersave=0x30
	.EQU __sm_standby=0x60
	.EQU __sm_ext_standby=0x70
	.EQU __sm_adc_noise_red=0x10
	.SET power_ctrl_reg=mcucr
	#endif
#include <lcd-5110.h>
#include <string.h>
#include <stdio.h>

/* the I2C bus is connected to PORTC */
/* the SDA signal is bit 1 */
/* the SCL signal is bit 0 */
#asm
    .equ __i2c_port=0x15
    .equ __sda_bit=1
    .equ __scl_bit=0
#endasm
#include <i2c.h>
#include <pcf8583.h>

// define part
#define RTC_CHIP     0
#define bool         unsigned char
#define true         1
#define false        0


// ��С�Ȥ�Ҥ����� flash
// �ѹ㴪�������ѹ���� , ��ͧ�á�� free ���͡�˹��������
// 8 = �ӹǹ��, 10 = ������ǵ���ѡ�÷����Ƿ���ش(9) + '\0'
flash char DAY_TBL[8][10] = {"free","sunday","monday","tuesday","wednesday","thursday","friday","saturday"};

// �������͹ (�ҡ�) ��ա���ѹ , ��ͧ�á�� 0 ���͡�˹��������
const unsigned char TOTAL_DAY_TBL[] = {0,31,28,31,30,31,30,31,31,30,31,30,31};

// �������͹ (��) ���͹� �ա���ѹ , ��ͧ�á�� 0 �����������
const unsigned char LUNAR_DAY_TBL[] = {0,14,15,14,15,14,15,14,15,14,15,14,15};

        /* original
        int[] LUNAR_YEAR_TBL = new int[]{
        0,2,0,1,2,0,2,0,1,2,0,0,2,0,1,2,0,2,0,1,
        2,0,0,2,1,2,0,0,2,0,1,2,0,2,0,1,2,0,0,2,
        0,1,2,0,2,0,1,2,0,0,2,1,2,0,0,2,0,1,2,0,
        1,2,0,2,0,0,2,0,1,2,0,2,1,0,2,0,1,2,0,1,
        2,0,2,0,0,2,0,2,1,0,2,0,1,2,0,1,2,0,0,2,
        1,2,0,0,2,0,1,2,0,2,0,0,2,1,0,2,1,0,2,0,
        2,0,1,2,0,0,2,0,2,0,1,2,0,1,2,0,2,0,0,2,
        1,0,2,1,0,2,0,2,0,1,2,0,1,2,0,2,0,1,2,0,
        0,2,0,0,2,0,2,0,1,2,0,0,2,1,2,0};
        */

        /*
         * ����ó� ������ѹ��� 1/1/2484 - 31/3/2575
         * ���ҧ��˹���һչ��� �繻ջ������
         * 0 = ������� �������
         * 1 = ������� ͸ԡ���
         * 2 = ͸ԡ��� �������
         int []LUNAR_YEAR_TBL = new int[]{
            0, // ����� 1/1/2484
            0,2,0,2,1,0,2,0,1,2,0,1,2,0,0,2,1,2,
            0,0,2,0,1,2,0,2,0,0,2,1,0,2,1,0,2,0,
            2,0,1,2,0,0,2,0,2,0,1,2,0,1,2,0,2,0,0,2,
            1,0,2,1,0,2,0,2,0,1,2,0,1,2,0,2,0,1,2,0,
            0,2,0,0,2,0,2,0,1,2,0,0,2,1,2,0};
         */

// 1/1/2423 - 31/3/2575
const unsigned char LUNAR_YEAR_TBL[] = {
            2,1,2,0,0,2,0,1,2,0,2,0,1,2,0,
            0,2,0,1,2,0,2,0,1,2,0,0,2,1,2,
            0,0,2,0,1,2,0,1,2,0,2,0,0,2,0,
            1,2,0,2,1,0,2,0,1,2,0,1,2,0,2,
            0,0,2,0,2,1,0,2,0,1,2,0,1,2,0,
            0,2,1,2,0,0,2,0,1,2,0,2,0,0,2,
            1,0,2,1,0,2,0,2,0,1,2,0,0,2,0,
            2,0,1,2,0,1,2,0,2,0,0,2,1,0,2,
            1,0,2,0,2,0,1,2,0,1,2,0,2,0,1,
            2,0,0,2,0,0,2,0,2,0,1,2,0,0,2,1,2,0};

// ��˹���Ңͧ �շ���ҡ����ش�������ö���� (��ͤ�� year_table ����ͧ)
const unsigned int MAX_YEAR = sizeof(LUNAR_YEAR_TBL);

//# ���ʴ���
unsigned char display_line[4][15];
unsigned char tmp_string[16];
unsigned char tmp_string_len;

//# ���Ѻ���� ����ѹ���
unsigned char hh,mm,ss,hs;
unsigned char now_day,now_month;
unsigned int now_year;

/* user defined function */
unsigned char is_leap_year(unsigned int year);
unsigned int change_b2c(unsigned char month, unsigned int thai_year);
unsigned char chk_valid_date(unsigned char day, unsigned char month, unsigned int year);
unsigned char find_date(unsigned char fday,unsigned char fmonth,unsigned int fyear,unsigned char day,unsigned char month,unsigned int year);
unsigned char process(unsigned char day,unsigned char month,unsigned int year);


//# ��Ǩ�ͺ��һչ��� ��͹ ��.�� 29 �ѹ������� (�� �.�.)
unsigned char is_leap_year(unsigned int year)
{
    //# �����ô��� 4 ŧ��ǵ�ͧ�ҾԨ�ó��ա����..
    if ((year % 4) == 0)
    {
    	//# �����ô��� 100 ŧ����ա ��ͧ�Ԩ�ó��ա
	    if ((year % 100) == 0)
	        //# �����ô��� 400 ŧ����ա �ʴ������ ͸ԡ��÷Թ
	        return (year % 400 == 0);
	    else
	        //# �����ô��� 100 ���ŧ��� �ʴ�����繻�͸ԡ��÷Թ
	        return true;
    }
    //# �����ô��� 4 ���ŧ��� �繻ջ��� ��͹
    return false;
}

//# ����¹ �� �.�. �� �.�.
unsigned int change_b2c(unsigned char month, unsigned int thai_year)
{
    unsigned int year = thai_year;
    if (year >= 2483)
    {
        year = year - 543;
    }
    else
    {
    	if ((month >= 1) && (month <= 3))
	        year = year - 542;
	    else
    	    year = year - 543;
    }
    return year;
}

//# ��Ǩ�ͺ Ǵ� ��Ҷ١��ͧ�������
unsigned char chk_valid_date(unsigned char day, unsigned char month, unsigned int year)
{
    //# ��͹�ŧ�繻� �.�.
    unsigned int cyear = change_b2c(month,year);

    //# 1. ���ѹ�������ը�ԧ������� (1 �.�. - 31 �դ. 2483)
    if ((year == 2483) && (month >= 1 && month <= 3))
        return false; //# "error : date not found"

    //# 2. �ѹ����Թ��˹�������� 2400 <= x <= 2575
    if ((year < 2400) || (year > 2575))
        return false; //# "error : date overflow"

    //# 3. ������ѹ�������ը�ԧ㹻� 2400 (�չ������ ��͹ 1,2,3 �͢����͹ 4 ���繻� 2401)
    if ((year == 2400) && (month >= 4 && month <= 12))
        return false;

    //# ��Ǩ�ͺ�����١��ͧ
    //# 3. ���ѹ��� 29 �.�. ���͹���������������
    if (!is_leap_year(cyear) && (day == 29 && month == 2))
	    return false; // # "error : 29 mar in leap year"

    return true;
}

//# ��Ǩ����ѹ���Ѩ�غѹ���� �ç�Ѻ�ѹ����ͧ����������
unsigned char find_date(unsigned char fday,unsigned char fmonth,unsigned int fyear,unsigned char day,unsigned char month,unsigned int year)
{
    return ((fday == day) && (fmonth == month) && (fyear == year));
}


//# �����ѹ����к� ������ѹ���� , arg year = �.�. Ẻ��, �ѹ��� ��͹Ẻ�ҡ�
//# �� function ���ŧ�.�. �� �.�. ���ǹ����令ӹǹ�ͧ���ѵ��ѵ�
unsigned char process(unsigned char day,unsigned char month,unsigned int year)
{
		    /* //����ó������ ��Ѻ�� 1/1/2484
		    //# ��˹�����������
		    unsigned char m_day_name = 4;  //# �����ѹ �������Ф��ʵ������͹�ѹ
		    unsigned char m_day_no = 1; //# �ѹ��� �������Ф��ʵ������͹�ѹ
		    unsigned char m_month_no = 1; //# ��͹�ҡ�

		    unsigned int m_christ_year = 1941; //# ���ҡ�
		    unsigned int m_thai_year = 2484; //# �վ.�.

		    bool m_phase_up = true; //# ��ҧ���(true) ���͢�ҧ���(false)
		    unsigned char m_lunar_phase = 4; //# �Զ�
		    unsigned char m_thai_month = 2; //# ��͹�� ����ѹ�ä��
		    unsigned char m_animal_year = 5; //# �չѡ�ѵ� ��������� ���ç

		    //# ���Ի�ԷԹ�¨�����¹�� �.�. ����� ��͹ 4, ���Ҽ�ҹ�� 2483 仨�����¹����͹ 1 ����͹���ʵ�
		    unsigned char m_change_year_month = 1; //# 4
		    */

    //# ��˹�����������  ������� 1/1/2423
    unsigned char m_day_name = 7;  //# �����ѹ �������Ф��ʵ������͹�ѹ
    unsigned char m_day_no = 1; //# �ѹ��� �������Ф��ʵ������͹�ѹ
    unsigned char m_month_no = 1; //# ��͹�ҡ�

    unsigned int m_thai_year = 2423; //# �վ.�.
    unsigned int m_christ_year = 1881; //# ���ҡ�

    bool m_phase_up = true; //# ��ҧ���(true) ���͢�ҧ���(false)
    unsigned char m_lunar_phase = 2; //# �Զ�
    unsigned char m_thai_month = 2; //# ��͹�� ����ѹ�ä��
    unsigned char m_animal_year = 5; //# �չѡ�ѵ� ��������� ���ç

    //# ���Ի�ԷԹ�¨�����¹�� �.�. ����� ��͹ 4, ���Ҽ�ҹ�� 2483 仨�����¹����͹ 1 ����͹���ʵ�
    unsigned char m_change_year_month = 4;

    // ��ҷ���ͧ����颳�������ٻ
    bool change_month = false;  //# �纤�ҡ�õ�Ǩ�ͺ��Ҩе�ͧ������͹�������
    unsigned long int	total_day_count = 0; // count all day at start
    unsigned char chk_ram = 14;  // ��͹��Ǩ ��ҧ���

    //# ��Ǩ�����١��ͧ
    if (chk_valid_date(day,month,year))
    {
        //# ��������ǹ�ͺ�ӹǹ �����Ҩ������ҷ�����ԧ�͡�� !!
        bool month_twiced = false; //# ��˹���ùѺ��͹ 8 2 ����
        unsigned int year_count = 0; //# ��ǹѺ�ͺ��ҵ͹������� ������������ lunar_year_type ��ͧ�
        bool found_date = find_date(m_day_no, m_month_no, m_thai_year, day, month, year);

        while ((year_count < MAX_YEAR) && (!found_date))
	    {
	        // ��Ǩ�Ѻ��������ҷ���������ѹ (�ѧ��������������ª������)
	        total_day_count++;

	        //# ===================== �ӹǹ Ǵ� �ҡ� ================================
	        //# �����ѹ �������Ф��ʵ������͹�ѹ
	        m_day_name += 1;
	        if (m_day_name > 7)
	        {
		        m_day_name = 1;
	        }
            change_month = false;  //# �纤�ҡ�õ�Ǩ�ͺ��Ҩе�ͧ������͹�������
	        //# +�ѹ��� (�������Ф��ʵ������͹�ѹ)
	        m_day_no += 1;
	        //# ����� ��͹ 2 (�.�.) ��лչ���� 29 �ѹ
	        if (m_month_no == 2 && is_leap_year(m_christ_year))
	        {
		        if (m_day_no > 29)  //# ��Ǩ��� 29
		            change_month = true;
	        }
	        else
	        {
		        //# �������͹���� ��Ǩ������, ����ҧ�������
		        if (m_day_no > TOTAL_DAY_TBL[m_month_no])
		            change_month = true;
            }

	        //# ����͵�Ǩ�ͺ������� ��ͧ������͹�ҡ�
	        if (change_month == true)
	        {
		        //# reset �ѹ ��Ѻ���ѹ��� 1 ����͹���
		        m_day_no = 1;
		        //# ����¹��͹����
		        m_month_no += 1;
		        if (m_month_no > 12)
		        {
		            m_month_no = 1;
		            //# ������ �ҡ�
		            m_christ_year += 1; //# ���ҡ�
		        }
	        }

	        //# ===================== �ӹǹ Ǵ� �.�. ===================================
	        //# ���Ի�ԷԹ�¨�����¹�� �.�. ����� ��͹ 4 (m_change_year_month = 4)
	        //# ���Ҽ�ҹ�� 2483 仨�����¹����͹ 1 ����͹���ʵ�
	        //#
	        if ((m_day_no == 1) && (m_month_no == m_change_year_month))
	        {
		        m_thai_year += 1; //# + �վ.�.
	        }

	        //# ����� 31 �.�. 2483 �ѹ����¹ �.�.����ѹ��� 1 �.�. �ء��
	        if ((m_day_no == 31) && (m_month_no == 12 && m_thai_year == 2483))
		        m_change_year_month = 1;

	        //# ===================== �ӹǹ Ǵ� �ѹ�ä�� ================================
	        //# +�Զ�
	        m_lunar_phase += 1;
	        //# ��� + ���Թ�� 16 ��Ӣ�ҧ��� �������¹�繢�ҧ���
	        if ((m_phase_up == true) && (m_lunar_phase > 15))
	        {
		        m_phase_up = false;
		        m_lunar_phase = 1;
	        }

	        //# ��˹������������ͧ��õ�Ǩ�ͺ
	        chk_ram = 14;
	        //# ����繢�ҧ��� ��ͧ�������͹�������������� ��������¹�繢�ҧ���
	        //# ͸ԡ��� ���� ��� 15 ���
	        if ((LUNAR_YEAR_TBL[year_count] == 1) && (m_thai_month == 7))
		        chk_ram = 15;
	        else
		        //# ������� ���� ͸ԡ����ѧ�����ٻẺ���
		        chk_ram = LUNAR_DAY_TBL[m_thai_month];

	        //# ����Ң�ҧ����ҡ���ҷ���������ѧ ����Թ�����������¹�� ��ҧ���
	        if ((m_phase_up == false) && (m_lunar_phase > chk_ram))
	        {
		        //# ����¹�繢�ҧ���
		        m_phase_up = true;
		        //# 1 ���
		        m_lunar_phase = 1;
		        //# �����͹����
		        m_thai_month += 1;
		        //# 㹡óշ�� ��Ẻ ͸ԡ��� �������͹ 8(+1=9) �������¹Ѻ�ҡ�͹ ����ͧ�����͹����
		        if ((LUNAR_YEAR_TBL[year_count] == 2) && (m_thai_month == 9) && (month_twiced == false))
		        {
		            month_twiced = true;
		            m_thai_month -= 1;
		            //# �� clear ����͢�� year_count ����
		        }

		        //# �������͹ 5 �������¹�չѡ�ѵ�
		        if (m_thai_month == 5)
		        {
		            //# �ǡ���ҧ�ѹ��� ��Шӻ������ա 1
		            year_count += 1;

		            //# �ǡ�չѡ�ѵ�
		            m_animal_year += 1;
		            //# ��һչѡ�ѵ��ҡ���� 12 �����͹���� 1
		            if (m_animal_year > 12)
			            m_animal_year = 1;

		            //# clear ��ùѺ�����͹Ỵ 2 ����
		            month_twiced = false;
		        }
		        //# �����͹���ҡ���� 12 �����͹���� 1
		        if (m_thai_month > 12)
		        {
		            m_thai_month = 1;
		        }
	        }

	        //# ��º����ѹ�Ѩ�غѹ�� �ѹ���ç������� (�.�.)
	        found_date = find_date(m_day_no, m_month_no, m_thai_year, day, month, year);
	    } // while
	    //# ��Ҥú����٧�ش ��������� �ʴ���ҼԴ��Ҵ
	    if (year_count >= MAX_YEAR)
	        return false; //# "error : date overflow"

	    //# �ç����ʴ������ !!
	    sprintf(display_line[0],"%d/%d/%d(%d)\0    ",m_day_no,m_month_no,m_thai_year,m_christ_year);

	    // work !
	    tmp_string_len = strlenf(DAY_TBL[m_day_name]);
	    memcpyf(tmp_string, DAY_TBL[m_day_name], tmp_string_len );
	    sprintf(display_line[1],"%s(%d)            ",tmp_string, m_day_name);
	    sprintf(display_line[2],"ph = %d(%c%d)%d        ",m_day_name, m_phase_up ? '+' :'-',m_lunar_phase,m_thai_month);
	    sprintf(display_line[3],"ch zodiac = %d       ",m_animal_year);
        return true;
    } // main if

    return false; //# error :
}

void main(void)
{
    // ���ͺ real time clock
    i2c_init();
    rtc_init(RTC_CHIP, 1);
    //rtc_set_time(RTC_CHIP, 13, 40, 0, 0);
    //rtc_set_date(RTC_CHIP, 27, 6, 2008);

  DDRB = 0b00111111;                                                    // PB[7,6] = Input,PB[5..0] = Output

  /* Initial GPIO Signal Interface LCD Nokia-5110 */
  LCD5110_RES_LOW();					// Active Reset
  LCD5110_RES_HIGH();					// Normal Operation
  LCD5110_DC_HIGH(); 					// D/C = High("1"=Data)
  LCD5110_LED_HIGH();					// LED = High(ON LED)
  LCD5110_SDIN_LOW();                                                   // Standby SPI Data
  LCD5110_SCLK_LOW();                                                   // Standby SPI Clock
  LCD5110_SCE_LOW();					// SCE = Low(Enable)

  /* Start Initial & Display Character to LCD */
  lcd_initial();                				// Initial LCD
  lcd_clear_screen();              				// Clear Screen Display

  lcd_gotoxy(0,0);					// Set Cursor = Line-1

  /* if (process(29,12,2525) == true)
  {
      lcd_print_string(display_line[0],14);			// Display LCD Line-1
      lcd_print_string(display_line[1],14);			// Display LCD Line-1
      lcd_print_string(display_line[2],14);			// Display LCD Line-1
      lcd_print_string(display_line[3],14);			// Display LCD Line-1
  }
  else
  {
        lcd_print_flash_string("err",3);
  }  */

  while(1)
  {
     delay(250000);
     //lcd_clear_screen();              				// Clear Screen Display
     lcd_gotoxy(0,0);					// Set Cursor = Line-1
     lcd_print_flash_string("              ",14);
     lcd_print_flash_string("              ",14);

     rtc_get_date(RTC_CHIP, &now_day, &now_month, &now_year);
     sprintf(tmp_string,"%d/%d/%d     ", now_day, now_month, now_year);
     lcd_print_string(tmp_string,14);

     rtc_get_time(RTC_CHIP,&hh,&mm,&ss,&hs);
     sprintf(tmp_string,"%d:%d:%d       ", hh,mm,ss);
     lcd_print_string(tmp_string,14);
  }
}
/*********************************************/
/* Example Program For ET-AVR STAMP ATMEGA32 */
/* MCU      : ATMEGA64(XTAL=16 MHz)          */
/*          : Frequency Bus = 16 MHz         */
/* Compiler : CodeVisionAVR 1.24.8d          */
/* Write By : Adisak Choochan(ETT CO.,LTD.)  */
/* Function : Demo Interface LCD Nokia-5110  */
/*          : Use Pin I/O Generate SPI Signal*/
/*********************************************/
/* Interface LCD NOKIA-5110 By AVR Function  */
/* -> ATMEGA64   --> LCD Nokia-5110          */
/* -> PB0(I/O)   --> SCE(Active "0")         */
/* -> PB1(I/O)   --> RES(Active "0")         */
/* -> PB2(I/O)   --> D/C(1=Data,0=Command)   */
/* -> PB3(I/O)   --> SDIN                    */
/* -> PB4(I/O)   --> SCLK                    */
/* -> PB5(I/O)   --> LED(Active "1")         */
/*********************************************/

/* Include  Section */
#include <mega32.h>                                                     // ATmega64 MCU
	#ifndef __SLEEP_DEFINED__
	#define __SLEEP_DEFINED__
	.EQU __se_bit=0x80
	.EQU __sm_mask=0x70
	.EQU __sm_powerdown=0x20
	.EQU __sm_powersave=0x30
	.EQU __sm_standby=0x60
	.EQU __sm_ext_standby=0x70
	.EQU __sm_adc_noise_red=0x10
	.SET power_ctrl_reg=mcucr
	#endif
#include <lcd-5110.h>


/************************************/
/* Font Code Size 5:Byte/Font Table */
/************************************/
flash unsigned char tab_font[ ] =
{
 0x00, 0x00, 0x00, 0x00, 0x00,   // sp
 0x00, 0x00, 0x2f, 0x00, 0x00,   // !
 0x00, 0x07, 0x00, 0x07, 0x00,   // "
 0x14, 0x7f, 0x14, 0x7f, 0x14,   // #
 0x24, 0x2a, 0x7f, 0x2a, 0x12,   // $
 0x62, 0x64, 0x08, 0x13, 0x23,   // %
 0x36, 0x49, 0x55, 0x22, 0x50,   // &
 0x00, 0x05, 0x03, 0x00, 0x00,   // �
 0x00, 0x1c, 0x22, 0x41, 0x00,   // (
 0x00, 0x41, 0x22, 0x1c, 0x00,   // )
 0x14, 0x08, 0x3E, 0x08, 0x14,   // *
 0x08, 0x08, 0x3E, 0x08, 0x08,   // +
 0x00, 0x00, 0xA0, 0x60, 0x00,   // ,
 0x08, 0x08, 0x08, 0x08, 0x08,   // -
 0x00, 0x60, 0x60, 0x00, 0x00,   // .
 0x20, 0x10, 0x08, 0x04, 0x02,   // /
 0x3E, 0x51, 0x49, 0x45, 0x3E,   // 0
 0x00, 0x42, 0x7F, 0x40, 0x00,   // 1
 0x42, 0x61, 0x51, 0x49, 0x46,   // 2
 0x21, 0x41, 0x45, 0x4B, 0x31,   // 3
 0x18, 0x14, 0x12, 0x7F, 0x10,   // 4
 0x27, 0x45, 0x45, 0x45, 0x39,   // 5
 0x3C, 0x4A, 0x49, 0x49, 0x30,   // 6
 0x01, 0x71, 0x09, 0x05, 0x03,   // 7
 0x36, 0x49, 0x49, 0x49, 0x36,   // 8
 0x06, 0x49, 0x49, 0x29, 0x1E,   // 9
 0x00, 0x36, 0x36, 0x00, 0x00,   // :
 0x00, 0x56, 0x36, 0x00, 0x00,   // ;
 0x08, 0x14, 0x22, 0x41, 0x00,   // <
 0x14, 0x14, 0x14, 0x14, 0x14,   // =
 0x00, 0x41, 0x22, 0x14, 0x08,   // >
 0x02, 0x01, 0x51, 0x09, 0x06,   // ?
 0x32, 0x49, 0x59, 0x51, 0x3E,   // @
 0x7C, 0x12, 0x11, 0x12, 0x7C,   // A
 0x7F, 0x49, 0x49, 0x49, 0x36,   // B
 0x3E, 0x41, 0x41, 0x41, 0x22,   // C
 0x7F, 0x41, 0x41, 0x22, 0x1C,   // D
 0x7F, 0x49, 0x49, 0x49, 0x41,   // E
 0x7F, 0x09, 0x09, 0x09, 0x01,   // F
 0x3E, 0x41, 0x49, 0x49, 0x7A,   // G
 0x7F, 0x08, 0x08, 0x08, 0x7F,   // H
 0x00, 0x41, 0x7F, 0x41, 0x00,   // I
 0x20, 0x40, 0x41, 0x3F, 0x01,   // J
 0x7F, 0x08, 0x14, 0x22, 0x41,   // K
 0x7F, 0x40, 0x40, 0x40, 0x40,   // L
 0x7F, 0x02, 0x0C, 0x02, 0x7F,   // M
 0x7F, 0x04, 0x08, 0x10, 0x7F,   // N
 0x3E, 0x41, 0x41, 0x41, 0x3E,   // O
 0x7F, 0x09, 0x09, 0x09, 0x06,   // P
 0x3E, 0x41, 0x51, 0x21, 0x5E,   // Q
 0x7F, 0x09, 0x19, 0x29, 0x46,   // R
 0x46, 0x49, 0x49, 0x49, 0x31,   // S
 0x01, 0x01, 0x7F, 0x01, 0x01,   // T
 0x3F, 0x40, 0x40, 0x40, 0x3F,   // U
 0x1F, 0x20, 0x40, 0x20, 0x1F,   // V
 0x3F, 0x40, 0x38, 0x40, 0x3F,   // W
 0x63, 0x14, 0x08, 0x14, 0x63,   // X
 0x07, 0x08, 0x70, 0x08, 0x07,   // Y
 0x61, 0x51, 0x49, 0x45, 0x43,   // Z
 0x00, 0x7F, 0x41, 0x41, 0x00,   // [
 0x55, 0x2A, 0x55, 0x2A, 0x55,   // 55
 0x00, 0x41, 0x41, 0x7F, 0x00,   // ]
 0x04, 0x02, 0x01, 0x02, 0x04,   // ^
 0x40, 0x40, 0x40, 0x40, 0x40,   // _
 0x00, 0x01, 0x02, 0x04, 0x00,   // �
 0x20, 0x54, 0x54, 0x54, 0x78,   // a
 0x7F, 0x48, 0x44, 0x44, 0x38,   // b
 0x38, 0x44, 0x44, 0x44, 0x20,   // c
 0x38, 0x44, 0x44, 0x48, 0x7F,   // d
 0x38, 0x54, 0x54, 0x54, 0x18,   // e
 0x08, 0x7E, 0x09, 0x01, 0x02,   // f
 0x18, 0xA4, 0xA4, 0xA4, 0x7C,   // g
 0x7F, 0x08, 0x04, 0x04, 0x78,   // h
 0x00, 0x44, 0x7D, 0x40, 0x00,   // i
 0x40, 0x80, 0x84, 0x7D, 0x00,   // j
 0x7F, 0x10, 0x28, 0x44, 0x00,   // k
 0x00, 0x41, 0x7F, 0x40, 0x00,   // l
 0x7C, 0x04, 0x18, 0x04, 0x78,   // m
 0x7C, 0x08, 0x04, 0x04, 0x78,   // n
 0x38, 0x44, 0x44, 0x44, 0x38,   // o
 0xFC, 0x24, 0x24, 0x24, 0x18,   // p
 0x18, 0x24, 0x24, 0x18, 0xFC,   // q
 0x7C, 0x08, 0x04, 0x04, 0x08,   // r
 0x48, 0x54, 0x54, 0x54, 0x20,   // s
 0x04, 0x3F, 0x44, 0x40, 0x20,   // t
 0x3C, 0x40, 0x40, 0x20, 0x7C,   // u
 0x1C, 0x20, 0x40, 0x20, 0x1C,   // v
 0x3C, 0x40, 0x30, 0x40, 0x3C,   // w
 0x44, 0x28, 0x10, 0x28, 0x44,   // x
 0x1C, 0xA0, 0xA0, 0xA0, 0x7C,   // y
 0x44, 0x64, 0x54, 0x4C, 0x44,   // z
 0x00, 0x08, 0x36, 0x41, 0x00,   // {
 0x00, 0x00, 0x7F, 0x00, 0x00,   // |
 0x00, 0x41, 0x36, 0x08, 0x00,   // }
 0x08, 0x10, 0x08, 0x04, 0x08    // ~
};

/*****************************/
/* Graphic 84x48 Pixed Table */
/*****************************/

flash unsigned char tab_picture[ ] =
{
 0x00,0x00,0x00,0x00,0x00,
 0x00,0x00,0x00,0x00,0x00,
 0x00,0x00,0x00,0x00,0x00,
 0x00,0x00,0x00,0xC0,0xE0,
 0xE0,0x70,0x30,0xB0,0xB0,
 0xB0,0xB0,0xB0,0xB0,0xB0,
 0xB0,0xB0,0xA0,0x00,0x00,
 0x00,0xA0,0xB0,0xB0,0xB0,
 0xB0,0xB0,0xB0,0x30,0xB0,
 0xB0,0xB0,0xB0,0xB0,0xB0,
 0xB0,0xA0,0x00,0x00,0xA0,
 0xB0,0xB0,0xB0,0xB0,0xB0,
 0xB0,0x30,0xB0,0xB0,0xB0,
 0xB0,0xB0,0xB0,0xB0,0xA0,
 0x00,0x00,0x00,0x00,0x00,
 0x00,0x00,0x00,0x00,0x00,
 0x00,0x00,0x00,0x00,0x00,
 0x00,0x00,0x00,0x00,0x00,
 0x00,0x00,0x00,0x00,0x00,
 0x00,0x00,0x00,0x00,0x00,
 0x00,0xFF,0xFF,0x01,0xFC,
 0xFE,0x03,0x01,0x01,0x01,
 0x01,0x01,0x01,0x01,0x01,
 0x01,0x00,0x00,0x00,0x00,
 0x00,0x01,0x01,0x01,0x01,
 0xFF,0xFF,0x00,0xFF,0xFF,
 0x01,0x01,0x01,0x01,0x01,
 0x00,0x00,0x00,0x00,0x01,
 0x01,0x01,0x01,0xFF,0xFF,
 0x00,0xFF,0xFF,0x01,0x01,
 0x01,0x01,0x01,0x00,0x00,
 0x00,0x00,0x00,0x00,0x00,
 0x00,0x00,0x00,0x00,0x00,
 0x00,0x00,0x00,0x00,0x00,
 0x00,0x00,0x00,0x00,0x00,
 0x00,0x00,0x00,0x00,0x00,
 0x00,0x00,0x00,0x00,0x00,
 0xFF,0xFF,0x00,0xFD,0xFD,
 0x0D,0x0D,0x0D,0x0D,0x0D,
 0x0D,0x0D,0x0D,0x04,0x00,
 0x00,0x00,0x00,0x00,0x00,
 0x00,0x00,0x00,0x00,0xFF,
 0xFF,0x00,0xFF,0xFF,0x00,
 0x00,0x00,0x00,0x00,0x00,
 0x00,0x00,0x00,0x00,0x00,
 0x00,0x00,0xFF,0xFF,0x00,
 0xFF,0xFF,0x00,0x00,0x00,
 0x00,0x00,0x00,0x00,0x00,
 0x00,0x00,0x00,0x00,0x00,
 0x00,0x00,0x00,0x00,0x00,
 0x00,0x00,0x00,0x00,0x00,
 0x00,0x00,0x00,0x00,0x00,
 0x00,0x00,0x00,0x00,0x00,
 0x00,0x00,0x00,0x00,0x07,
 0x0F,0x1C,0x39,0x73,0x67,
 0x6E,0x6C,0x6C,0x6C,0x6C,
 0x6C,0x6C,0x6C,0x6C,0x28,
 0x00,0x00,0x00,0x00,0x00,
 0x00,0x00,0x00,0x3F,0x7F,
 0x00,0x7F,0x3F,0x00,0x00,
 0x00,0x00,0x00,0x00,0x00,
 0x00,0x00,0x00,0x00,0x00,
 0x00,0x3F,0x7F,0x00,0x7F,
 0x3F,0x00,0x00,0x00,0x00,
 0x00,0x00,0x00,0x00,0x00,
 0x00,0x00,0x00,0x00,0x00,
 0x00,0x00,0x00,0x00,0x00,
 0x00,0x00,0x00,0x00,0x00,
 0x80,0x00,0x00,0x00,0x80,
 0x00,0x80,0x00,0x00,0x00,
 0x80,0x00,0x80,0x00,0x00,
 0x00,0x80,0x00,0x00,0x00,
 0x00,0x80,0x80,0x80,0x80,
 0x80,0x00,0x80,0x80,0x80,
 0x80,0x80,0x00,0x80,0x80,
 0x80,0x80,0x80,0x00,0x80,
 0x80,0x80,0x80,0x80,0x00,
 0x00,0x80,0x80,0x80,0x00,
 0x00,0x80,0x00,0x00,0x00,
 0x80,0x00,0x00,0x00,0x00,
 0x00,0x80,0x80,0x80,0x00,
 0x00,0x00,0x80,0x80,0x80,
 0x00,0x00,0x80,0x00,0x00,
 0x00,0x80,0x00,0x00,0x00,
 0x00,0x00,0x00,0x00,0x3F,
 0x10,0x08,0x10,0x3F,0x00,
 0x3F,0x10,0x08,0x10,0x3F,
 0x00,0x3F,0x10,0x08,0x10,
 0x3F,0x00,0x30,0x30,0x00,
 0x3F,0x24,0x24,0x24,0x20,
 0x00,0x00,0x00,0x3F,0x00,
 0x00,0x00,0x00,0x00,0x3F,
 0x00,0x00,0x00,0x3F,0x24,
 0x24,0x24,0x20,0x00,0x3F,
 0x08,0x08,0x08,0x3F,0x00,
 0x3F,0x01,0x02,0x01,0x3F,
 0x00,0x30,0x30,0x00,0x1F,
 0x20,0x20,0x20,0x11,0x00,
 0x1F,0x20,0x20,0x20,0x1F,
 0x00,0x3F,0x01,0x02,0x01,
 0x3F,0x00,0x00,0x00
};


/*
void main(void)
{
  DDRB = 0b00111111;                                                    // PB[7,6] = Input,PB[5..0] = Output

  // Initial GPIO Signal Interface LCD Nokia-5110
  LCD5110_RES_LOW();					// Active Reset
  LCD5110_RES_HIGH();					// Normal Operation
  LCD5110_DC_HIGH(); 					// D/C = High("1"=Data)
  LCD5110_LED_HIGH();					// LED = High(ON LED)
  LCD5110_SDIN_LOW();                                                   // Standby SPI Data
  LCD5110_SCLK_LOW();                                                   // Standby SPI Clock
  LCD5110_SCE_LOW();					// SCE = Low(Enable)

  // Start Initial & Display Character to LCD
  lcd_initial();                				// Initial LCD
  lcd_clear_screen();              				// Clear Screen Display

  while(1)
  {
    lcd_gotoxy(0,0);					// Set Cursor = Line-1
    lcd_print_string("BASE AVRMEGA64",14);			// Display LCD Line-1
    lcd_print_string("BY ETT CO.,LTD",14);			// Display LCD Line-2
    lcd_print_string("Demo SPI & LCD",14);			// Display LCD Line-3
    lcd_print_string("LCD Nokia-5110",14);			// Display LCD Line-4
    lcd_print_string("Demo Character",14);			// Display LCD Line-5
    lcd_print_string("14Char x 6Line",14);			// Display LCD Line-6 								// Display LCD Line-6
    delay(1000000);

    lcd_fill_picture();	 				// Display Graphic
    delay(1000000);
  }
}
*/

/********************************/
/* Write Data or Command to LCD */
/* D/C = "0" = Write Command    */
/* D/C = "1" = Write Display    */
/********************************/
void lcd_write_data(unsigned char DataByte)
{
  unsigned char Bit = 0;				// Bit Counter

  LCD5110_DC_HIGH(); 					// Active DC = High("1"=Data)

  for (Bit = 0; Bit < 8; Bit++)		                        // 8 Bit Write
  {
    if ((DataByte & 0x80) == 0x80)                                      // MSB First of Data Bit(7..0)
    {
      LCD5110_SDIN_HIGH();                                              // SPI Data = "1"
    }
    else
    {
      LCD5110_SDIN_LOW();                                               // SPI Data = "0"
    }

    LCD5110_SCLK_HIGH();		   		// Strobe Bit Data

    SPI_Delay();				            // Delay Clock

    LCD5110_SCLK_LOW();  				// Next Clock
    DataByte <<= 1;	 			            // Next Bit Data
  }
}

/********************************/
/* Write Data or Command to LCD */
/* D/C = "0" = Write Command    */
/* D/C = "1" = Write Display    */
/********************************/
void lcd_write_command(unsigned char CommandByte)
{
  unsigned char Bit = 0;   				// Bit Counter

  LCD5110_DC_LOW(); 	   				// Active DC = Low("0"=Command)

  for (Bit = 0; Bit < 8; Bit++)		                        // 8 Bit Write
  {
    if ((CommandByte & 0x80) == 0x80)                                   // MSB First of Data Bit(7..0)
    {
      LCD5110_SDIN_HIGH();                                              // SPI Data = "1"
    }
    else
    {
      LCD5110_SDIN_LOW();                                               // SPI Data = "0"
    }

    LCD5110_SCLK_HIGH();		   		// Strobe Bit Data

    SPI_Delay();				            // Delay Clock

    LCD5110_SCLK_LOW();  				// Next Clock
    CommandByte <<= 1;	 			            // Next Bit Data
  }
}

/**************************/
/* Delay SPI Clock Signal */
/**************************/
void SPI_Delay(void)
{
  int x;  						// Short Delay Counter
  x++;
  x++;
}

/**************************/
/* Initial LCD Nokia-5110 */
/**************************/
void lcd_initial(void)
{
  LCD5110_RES_LOW();					// Active Reset
  LCD5110_RES_HIGH();					// Normal Operation

  lcd_write_command(32+1); 				// Function Set = Extend Instruction(00100+PD,V,H=00100+0,0,1)
  lcd_write_command(128+38);				// Set VOP(1+VOP[6..0] = 1+0100110)
  lcd_write_command(4+3);   				// Temp Control(000001+TC1,TC0=000001+1,1)
  lcd_write_command(16+3);  				// Bias System(00010,BS2,BS1,BS0=00010,0,1,1)

  lcd_write_command(32+0);  				// Function Set = Basic Instruction(00100+PD,V,H = 00100+0,0,0)
  lcd_write_command(12);    				// Display Control = Normal Mode(00001D0E=00001100)
}

/****************************/
/* Clear Screen Display LCD */
/****************************/
void lcd_clear_screen(void)
{
  unsigned int  i=0; 					// Memory Display(Byte) Counter

  lcd_write_command(128+0);  				// Set X Position = 0(0..83)
  lcd_write_command(64+0);   				// Set Y Position = 0(0..5)

  for(i=0;i<504;i++)   	     				// All Display RAM = 504 Byte
  lcd_write_data(0);  	     				// Clear Screen Display
}

/****************************/
/* Fill Picture Display LCD */
/****************************/
void lcd_fill_picture(void)
{
  unsigned int  i=0;   													// Memory Display(Byte) Counter

  lcd_write_command(128+0);       										// Set X Position = 0(0..83)
  lcd_write_command(64+0);         										// Set Y Position = 0(0..5)

  for(i=0;i<504;i++)   													// All Display RAM = 504 Byte
  lcd_write_data(tab_picture[i]); 										// Fill Picture Display
}

/***************************/
/* Set Cursor X,Y Position */
/* X[0-83]: 84 Column Data */
/* Y[0-5] : 6 Row(48 Dot)  */
/***************************/
void lcd_gotoxy(unsigned char x,unsigned char y)
{
  lcd_write_command(128+x);  				// Set X Position(1+x6,x5,x4,x3,x2,x1,x0)
  lcd_write_command(64+y);   				// Set Y Position(01000+y2,y1,y0)
}

/***************************/
/* Put Char to LCD Display */
/***************************/
void lcd_put_char(unsigned char character)
{
  unsigned char font_size_count = 0; 			// Font Size Counter
  unsigned int  font_data_index;  	 			// Font Data Pointer

  font_data_index = character-32;    			// Skip 0x00..0x1F Font Code
  font_data_index = font_data_index*5;			// 5 Byte / Font

  while(font_size_count<5)                     			// Get 5 Byte Font & Display on LCD
  {
    lcd_write_data(tab_font[font_data_index]);  		// Get Data of Font From Table & Write LCD
    font_size_count++;  				// Next Byte Counter
    font_data_index++;  				// Next	Byte Pointer
  }
  lcd_write_data(0);					// 1 Pixel Dot Space
}

/*******************************/
/* Print String from FLASH to LCD Display */
/*******************************/
void lcd_print_flash_string(flash unsigned char *string , unsigned char CharCount)
{
  unsigned char i=0;  					// Dummy Character Count

  while(i<CharCount)
  {
    lcd_put_char(string[i]);				// Print 1-Char to LCD
    i++;                           				// Next Character Print
  }
}

/*******************************/
/* Print String to LCD Display */
/*******************************/
void lcd_print_string(unsigned char *string , unsigned char CharCount)
{
  unsigned char i=0;  					// Dummy Character Count

  while(i<CharCount)
  {
    lcd_put_char(string[i]);				// Print 1-Char to LCD
    i++;                           				// Next Character Print
  }
}

/*******************************************/
/* Long Delay Time Function(1..4294967295) */
/*******************************************/
void delay(unsigned long i)
{
  while(i > 0) {i--;}													// Loop Decrease Counter
  return;
}
