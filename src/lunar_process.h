
#define bool         unsigned char
#define true         1
#define false        0

/* user defined function */
unsigned char is_leap_year(unsigned int year);
unsigned int change_b2c(unsigned char month, unsigned int thai_year);
unsigned char chk_monk_day(unsigned char waxing_moon, unsigned char lunar_phase,unsigned char thai_month,unsigned char adikawan_year);
unsigned char chk_valid_date(unsigned char day, unsigned char month, unsigned int year);
unsigned char find_date(unsigned char fday,unsigned char fmonth,unsigned int fyear,unsigned char day,unsigned char month,unsigned int year);
unsigned char process(unsigned char day,unsigned char month,unsigned int year);

// ��С�Ȥ�Ҥ����� flash
// �ѹ㴪�������ѹ���� , ��ͧ�á�� free ���͡�˹��������
// 8 = �ӹǹ��, 3 = ������ǵ���ѡ�÷����Ƿ���ش(2) + '\0'
extern flash unsigned char ENG_DAY_TBL[8][3];

// ���ҧ�纪�����͹�ҡ� Ẻ���� , ��ͧ�á�� free
extern flash unsigned char ENG_MONTH_TBL[13][4];

// ���ҧ�纪����ѹ��� Ẻ���� , ��ͧ�á�� free
extern flash unsigned char THAI_DAY_TBL[8][3];

// ���ҧ�纪�����͹�� Ẻ���� , ��ͧ�á�� free
extern flash unsigned char THAI_MONTH_TBL[13][5];

// ���ҧ�纪��ͻչѡ�ѵ� , ��ͧ�á�� free
extern flash unsigned char ANIMAL_YEAR_TBL[13][7];

// ���ҧ�������͹ (�ҡ�) ��ա���ѹ , ��ͧ�á�� 0 ���͡�˹��������
extern flash unsigned char TOTAL_DAY_TBL[];

// ���ҧ�������͹ (��) ���͹� �ա���ѹ , ��ͧ�á�� 0 �����������
extern flash unsigned char LUNAR_DAY_TBL[];

// ���ҧ�纤�� ͸ԡ������ͻ������ ����л� 1/1/2423 - 31/3/2575
extern flash unsigned char LUNAR_YEAR_TBL[];

// ��˹���Ңͧ �շ���ҡ����ش�������ö���� (��ͤ�� year_table ����ͧ)            
extern unsigned int MAX_YEAR;
//*******************************************************************
//# ����ú���ǳ��� ���纤�ҷ����ҡ��� process ��觶�� process �� true ��Ҿǡ����繤�Ҩ�ԧ
// ��������¹���͵���� ����������鹩�Ѻ�Ҩҡ c#
extern unsigned char m_day_name;  //# �����ѹ �������Ф��ʵ������͹�ѹ
extern unsigned char m_day_no; //# �ѹ��� �������Ф��ʵ������͹�ѹ
extern unsigned char m_month_no; //# ��͹�ҡ�

extern unsigned int m_thai_year; //# �վ.�.
extern unsigned int m_christ_year; //# ���ҡ�           

extern bool m_phase_up; //# ��ҧ���(true) ���͢�ҧ���(false)
extern unsigned char m_lunar_phase; //# �Զ�
extern unsigned char m_thai_month; //# ��͹�� ����ѹ�ä��
extern unsigned char m_animal_year; //# �չѡ�ѵ� ��������� ���ç
    
extern bool month_twiced; //# ��˹���ùѺ��͹ 8 2 ����
extern unsigned int m_year_count; // ��ǹѺ�ͺ��

//*******************************************************************