
/*************************************/
/* ATMEGA64 Interface LCD Nokia-5110 */
/* -> ATMEGA64   --> LCD Nokia-5110  */
/* -> PB0(I/O)   --> SCE(Active "0") */
/* -> PB1(I/O)   --> RES(Active "0") */
/* -> PB2(I/O)   --> D/C("1"=Data    */
/*                       "0"=Command)*/
/* -> PB3(I/O)   --> SDIN            */
/* -> PB4(I/O)   --> SCLK            */
/* -> PB5(I/O)   --> LED(Active "1") */
/*************************************/

// Define LCD Nokia-5110 PinI/O Interface Mask Bit 
#define  LCD5110_SCE_HIGH()  	PORTB |= 0b00000001	            // SCE(PB0) = '1'(Disable)  
#define  LCD5110_SCE_LOW()  	PORTB &= 0b11111110 		// SCE(PB0) = '0'(Enable)
#define  LCD5110_RES_HIGH()  	PORTB |= 0b00000010		// RES(PB1) = '1'(Normal) 
#define  LCD5110_RES_LOW()  	PORTB &= 0b11111101		// RES(PB1) = '0'(Reset)
#define  LCD5110_DC_HIGH() 	PORTB |= 0b00000100		// D/C(PB2) = '1'(Data) 
#define  LCD5110_DC_LOW() 	PORTB &= 0b11111011		// D/C(PB2) = '0'(Command)
#define  LCD5110_SDIN_HIGH() 	PORTB |= 0b00001000		// LED(PB3) = '1'(Logic "1") 
#define  LCD5110_SDIN_LOW() 	PORTB &= 0b11110111		// LED(PB3) = '0'(Logic "0")
#define  LCD5110_SCLK_HIGH() 	PORTB |= 0b00010000		// LED(PB4) = '1'(Shift Data) 
#define  LCD5110_SCLK_LOW() 	PORTB &= 0b11101111		// LED(PB4) = '0'(Stand By)
#define  LCD5110_LED_HIGH() 	PORTB |= 0b00100000		// LED(PB5) = '1'(LED ON) 
#define  LCD5110_LED_LOW() 	PORTB &= 0b11011111		// LED(PB5) = '0'(LED OFF)
// End of Define For LCD Nokia-5110

/* User Define Function */
void lcd_write_data(unsigned char DataByte);			// Write Data to LCD
void lcd_write_command(unsigned char CommandByte);		// Write Command to LCD
void SPI_Delay(void);				   	// SPI Delay Clock
void lcd_initial(void);					// Initial LCD Nokia-5110
void lcd_clear_screen(void);				// Clear Screen Display
void lcd_fill_picture(void);				// Fill Picture Display
void lcd_gotoxy(unsigned char x,unsigned char y); 		// Set Cursor X(0..83),Y(0..5) 
void lcd_put_char(unsigned char character); 			// Put String(1 Char)
void lcd_print_string(unsigned char *string ,
                      unsigned char CharCount); 	            // Print String to LCD
void lcd_print_flash_string(flash unsigned char *string ,
                      unsigned char CharCount); 	            // Print String to LCD
void delay(unsigned long int);	
