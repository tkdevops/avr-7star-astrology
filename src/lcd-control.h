/****************************************************/
/* Example Program For ATMEGA32                     */
/* MCU      : ATMEGA32(XTAL=16 MHz)                 */
/*          : Frequency Bus = 16 MHz                */
/* Compiler : CodeVisionAVR 1.25.8a Professional    */
/* Write By : BenNueng@gmail.com                    */
/* Function : Demo Interface LCD Nokia-5110         */ 
/*          : Use Pin I/O Generate SPI Signal       */
/*          : Thai language                         */
/****************************************************/
/* Interface LCD ETT-NOKIA-5110 By AVR Function     */
/* -> ATMEGA64   --> LCD Nokia-5110                 */
/* -> PB0(I/O)   --> SCE(Active "0")                */
/* -> PB1(I/O)   --> RES(Active "0")                */
/* -> PB2(I/O)   --> D/C(1=Data,0=Command)          */
/* -> PB3(I/O)   --> SDIN                           */
/* -> PB4(I/O)   --> SCLK                           */
/* -> PB5(I/O)   --> LED(Active "1")                */
/****************************************************/       

/* Include  Section */
#ifndef MCU
    #define MCU             ATMEGA32
    #include <mega32.h>     // ATmega32 MCU
#endif
                            
/*************************************/
/* ATMEGA32 Interface LCD Nokia-5110 */
/* -> ATMEGA64   --> LCD Nokia-5110  */
/* -> PB0(I/O)   --> SCE(Active "0") */
/* -> PB1(I/O)   --> RES(Active "0") */
/* -> PB2(I/O)   --> D/C("1"=Data    */
/*                       "0"=Command)*/
/* -> PB3(I/O)   --> SDIN            */
/* -> PB4(I/O)   --> SCLK            */
/* -> PB5(I/O)   --> LED(Active "1") */
/*************************************/


//#ifdef LCD_BUG_FIXED                
        // �͡Ẻǧ�üԴ ������§�ҵ�ͧ�� PB0 �繵�ǤǺ�������Դ�Դ LED
        // Define LCD Nokia-5110 PinI/O Interface Mask Bit 
        #define  LCD5110_SCE_HIGH()  	PORTB |= 0b00000010	    // SCE(PB1) = '1'(Disable)  
        #define  LCD5110_SCE_LOW()  	PORTB &= 0b11111101 	      // SCE(PB1) = '0'(Enable)
        #define  LCD5110_RES_HIGH()  	PORTB |= 0b00000100		// RES(PB2) = '1'(Normal) 
        #define  LCD5110_RES_LOW()  	PORTB &= 0b11111011		// RES(PB2) = '0'(Reset)
        #define  LCD5110_DC_HIGH() 	    PORTB |= 0b00001000		// D/C(PB3) = '1'(Data) 
        #define  LCD5110_DC_LOW() 	    PORTB &= 0b11110111		// D/C(PB3) = '0'(Command)
        #define  LCD5110_SDIN_HIGH() 	PORTB |= 0b00010000		// LED(PB4) = '1'(Logic "1") 
        #define  LCD5110_SDIN_LOW() 	PORTB &= 0b11101111		// LED(PB4) = '0'(Logic "0")
        #define  LCD5110_SCLK_HIGH() 	PORTB |= 0b00100000		// LED(PB5) = '1'(Shift Data) 
        #define  LCD5110_SCLK_LOW() 	PORTB &= 0b11011111		// LED(PB5) = '0'(Stand By)        
        #define  LCD5110_LED_HIGH() 	PORTB |= 0b01000000		// LED(PB6) = '1'(LED ON) 
        #define  LCD5110_LED_LOW() 	    PORTB &= 0b10111111		// LED(PB6) = '0'(LED OFF)
        // ��������觻Դ�Դ GND �ͧ LCD
        #define  LCD5110_ON()           PORTB &= 0b11111110		// LED(PB0) = '0'(GND = LCD ON)
        #define  LCD5110_OFF()          PORTB |= 0b00000001		// LED(PB0) = '1'(LCD OFF)

/*#else
        // Define LCD Nokia-5110 PinI/O Interface Mask Bit 
        #define  LCD5110_SCE_HIGH()  	PORTB |= 0b00000001	    // SCE(PB0) = '1'(Disable)  
        #define  LCD5110_SCE_LOW()  	PORTB &= 0b11111110 	// SCE(PB0) = '0'(Enable)
        #define  LCD5110_RES_HIGH()  	PORTB |= 0b00000010		// RES(PB1) = '1'(Normal) 
        #define  LCD5110_RES_LOW()  	PORTB &= 0b11111101		// RES(PB1) = '0'(Reset)
        #define  LCD5110_DC_HIGH() 	    PORTB |= 0b00000100		// D/C(PB2) = '1'(Data) 
        #define  LCD5110_DC_LOW() 	    PORTB &= 0b11111011		// D/C(PB2) = '0'(Command)
        #define  LCD5110_SDIN_HIGH() 	PORTB |= 0b00001000		// LED(PB3) = '1'(Logic "1") 
        #define  LCD5110_SDIN_LOW() 	PORTB &= 0b11110111		// LED(PB3) = '0'(Logic "0")
        #define  LCD5110_SCLK_HIGH() 	PORTB |= 0b00010000		// LED(PB4) = '1'(Shift Data) 
        #define  LCD5110_SCLK_LOW() 	PORTB &= 0b11101111		// LED(PB4) = '0'(Stand By)
        #define  LCD5110_LED_HIGH() 	PORTB |= 0b00100000		// LED(PB5) = '1'(LED ON) 
        #define  LCD5110_LED_LOW() 	    PORTB &= 0b11011111		// LED(PB5) = '0'(LED OFF)
        // ��������觻Դ�Դ GND �ͧ LCD
        #define  LCD5110_ON()           PORTB &= 0b10111111		// LED(PB6) = '0'(GND = LCD ON)
        #define  LCD5110_OFF()          PORTB |= 0b01000000		// LED(PB6) = '1'(LCD OFF)

        //new test
        //#define  LCD5110_OFF()           PORTB &= 0b10111111		// LED(PB6) = '0'(GND = LCD ON)
        //#define  LCD5110_ON()          PORTB |= 0b01000000		// LED(PB6) = '1'(LCD OFF)
#endif*/

// End of Define For LCD Nokia-5110

/* User Define Function */
// Write Data to LCD
void lcd_write_data(unsigned char DataByte);
// Write Command to LCD
void lcd_write_command(unsigned char CommandByte);		
// SPI Delay Clock
void SPI_Delay(void);				   	    
// Initial LCD Nokia-5110
void lcd_initial(void);					
// Clear Screen Display
void lcd_clear_screen(void);				
// Fill Picture Display
//void lcd_fill_picture(void); // old
void lcd_fill_picture(flash unsigned char *tab_picture);
// Set Cursor X(0..83),Y(0..5) 
void lcd_gotoxy(unsigned char x,unsigned char y); 	
// Put String(1 Char)
void lcd_put_char(unsigned char character); 			
// Print "FLASH" String to LCD
void lcd_print_flash_string(flash unsigned char *string , unsigned char CharCount);
// Print "SRAM" String to LCD 
void lcd_print_string(unsigned char *string , unsigned char CharCount);
// ˹�ǧ�������ҧ����
void delay(unsigned long int); 

                                          
/*  ����� font */
extern flash unsigned char tab_font[ ]; 
extern flash unsigned char tab_picture[ ];


