/****************************************************/
/* Example Program For ATMEGA32                     */
/* MCU      : ATMEGA32(XTAL=16 MHz)                 */
/*          : Frequency Bus = 16 MHz                */
/* Compiler : CodeVisionAVR 1.25.8a Professional    */
/* Write By : BenNueng@gmail.com                    */
/* Function : Demo Interface LCD Nokia-5110         */ 
/*          : Use Pin I/O Generate SPI Signal       */
/*          : Thai language                         */
/****************************************************/
/* Interface LCD ETT-NOKIA-5110 By AVR Function     */
/*          : use default wire with config          */
/*            by lcd-control.h                      */
/****************************************************/  

/* Include  Section */
#include <lcd-control.h>
#include <string.h>


/* ��͡�˹� ����Ѻ������ */
// ��Ҩӹǹ��鹷�� ����ͧ�ͧ�������Ѻ����Ҵ�ٻ
#define LCD_NOKIA_5110      504     // 84 * 6 = 504
// LCD 5110 �ըش������� 0,0 ��� ��-���� ��������٧�ش x = 83, ���ҧ�٧�ش y = 47
#define MAX_X               83
#define MAX_Y               47
#define THAI_LINE_0         6       // 6 ��ͨش��������Ҵ�ѡ�� 0-5 �Ҵ����٧� ��ҧ�
#define THAI_LINE_1         THAI_LINE_0 + 16
#define THAI_LINE_2         THAI_LINE_1 + 16
// ������ҧ�
#define LCD_NORMAL          0
#define LCD_GRAPHICS        1
// ��������Ҵ����ѡ��
#define WRITE_NORMAL                        0
#define WRITE_TRANSPARENT                   1
#define WRITE_REVERSE                       2
#define WRITE_REVERSE_TRANSPARENT           3
// �ٵ��ŧ���˹� x,y �繵��˹� video ram
#define VRAM_ARRAY_INDEX(x,y)   ((y/8) *84 +x)
// ��ҺԵ����к�᡹ y ��ѧ�ҡ�����˹� index vram ; bit = y % 8
#define VRAM_BIT(y)             (y % 8)


/* ����觵�ҧ� �ͧ�к������� */
// update(refresh) video ram	
void lcd_update_video_ram(void);
// ź���� VIDEO RAM
void lcd_clear_video_ram(void);
// ��˹����˹� ax,ay
void lcd_set_axay(unsigned char x, unsigned char y);
// ��Ǩ����ҵ��˹觹��� �� 1 ���� 0
unsigned char lcd_get_dot(unsigned char x, unsigned char y);
// ���ŧ�ش
void lcd_set_dot(unsigned char x, unsigned char y);
// ���ź�ش
void lcd_clr_dot(unsigned char x, unsigned char y);
// ��˹��ش
void lcd_plot_dot(unsigned char x, unsigned char y, unsigned char dotproperty);
// �Ҵ�ٻ�١�ê��ŧ
void draw_down_arrow(void);
// ��˹���������ʴ��� LCD_NORMAL, LCD_GRAPHICS
void lcd_set_mode(unsigned char mode);
// �Ҵ����ѡ��价�� VIDEO RAM ���������ҧ� (Normal,Transparent,Reverse)
void lcd_put_char_2vram(unsigned char character, unsigned char write_mode);
void lcd_put_charxy_2vram(unsigned char x,unsigned char y, unsigned char character, unsigned char write_mode);
// ����¹�Ţ��úԤ���Ţ�� �����Ҵ
void lcd_change_num_2thai(unsigned char *str_number);
// ��������¤������ �ҡ ax,ay ����ش
void lcd_put_stringf_2vram(flash unsigned char *string,unsigned char write_mode);
void lcd_put_string_2vram(unsigned char *string,unsigned char write_mode);
void lcd_fill_picture_special(flash unsigned char *tab_picture,unsigned char write_mode);

// ���ҧ��˹�����Ţ 0-9
extern flash unsigned char numeric_tbl[];

/* ��С�ȵ���� GLOBAL ����ͧ����к� VIDEO RAM */
extern unsigned char VIDEO_RAM[LCD_NOKIA_5110];
// MODE ; 0 = normal (english 14x6 line), 1 = graphics (Thai 14x3 line)
extern unsigned char LCD_MODE;
// ���˹� x,y ������Ҵ (���˹�Ҩ�, ����к�) (axis)
extern unsigned char ax,ay;
// ���� buffer �纡�û����ż������ʴ�˹�Ҩ�
extern unsigned char tmp_display_string[42];
extern unsigned char tmp_display_string_len;

