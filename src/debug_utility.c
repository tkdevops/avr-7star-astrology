
/**************************************************************************
//  utility ��� debug
// �����ҹ 
// 1. include ��� header
// 2. ���¡ debug_init() ���͡�˹� baudrate
/**************************************************************************/ 

#include <debug_utility.h>
#include <string.h>
#include <stdio.h>

unsigned char * debug_tmp_ptr;
unsigned char debug_tmp_string[50] = "";
unsigned char debug_tmp_string_len = 0;

// ������觤����ʴ��ŷ�� printer (��ҹ txt �ҡ flash)
void debugf(flash char * txt)
{
    putsf("\r\n");
    putsf(txt);
}

// ������觤����ʴ��ŷ�� printer (��ҹ txt �ҡ SRAM)
void debug(char * txt)
{
    putsf("\r\n");
    puts(txt);
}

// ������ʴ��� �Ţ 7 �����੾��(��ҹ txt �ҡ SRAM)
void debug_7number(char * txt)
{   
    unsigned char count = 1;
    putsf("\r\n");
    while (count < 8)
    {
        itoa((int)txt[count++],debug_tmp_string);
        puts(debug_tmp_string);
        putsf(",");
    }
}

// ��˹��������� baudrate 19200
void debug_init(void)
{
    // USART initialization
    // Communication Parameters: 8 Data, 1 Stop, No Parity
    // USART Receiver: On
    // USART Transmitter: On
    // USART Mode: Asynchronous
    // USART Baud Rate: 19200
    UCSRA=0x00;
    UCSRB=0x18;
    UCSRC=0x86;
    UBRRH=0x00;
    UBRRL=0x23;
}