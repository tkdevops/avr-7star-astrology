/********************************************/
/* Examples Program For "AVR8(ATMEGA128)    */
/* Target MCU  : ATMEL AVR8 : ATMEGA128     */
/*		 : X-TAL : 16.00 MHz	            */
/* Text Editor : AVR Studio V4.14(Build589) */
/* Compiler    : WinAVR 2008-06-10	        */
/* Function    : Example Use UART0,UART1    */
/********************************************/
// ���� ATMEGA 128 ���� 2 UART ��ҵ�ͧ����� MCU ����� UART ����
// � comment define DUAL_UART � header file
					   					  
#include "debug_serial_env.h"

/* ---------------- MCU ����� UART ���Ǩл�С��Ẻ��� ---------------- */
#ifndef DUAL_UART
/*****************/
/* Initial UART0 */
/*****************/
void init_serial0 (unsigned int ubrr)  
{
  /* Set baud rate */
  UBRRH = (unsigned char)(ubrr>>8);
  UBRRL = (unsigned char)ubrr;

  /* Enable receiver and transmitter */ // enable RX interrupt
  UCSRB = (1<<RXEN)|(1<<TXEN); // | (1 << RXCIE);

  /* Set frame format: 8data, 1stop bit */
  UCSRC = (1<<URSEL)|(3<<UCSZ0);  
}
  
/******************************/
/* Write Character To UART[0] */
/******************************/
void putchar0 (unsigned char data)  
{
  if (data == '\n')  
  {
    while (!( UCSRA & (1<<UDRE)));
    UDR = 0x0D;								  
  }

  while (!( UCSRA & (1<<UDRE)));
  UDR = data;
}


/******************************/
/* Get character From UART[0] */
/******************************/
unsigned char getchar0()  
{
  while(!(UCSRA & (1<<RXC)));
  return (UDR);
}

/***************************/
/* Print String to UART[0] */
/***************************/
void print_uart0(void)
{
  char *p;
  p = uart_buf;

  do
  {
    putchar0(*p);
    p++;
  }
  while(*p != '\0');

  return;
}
#endif

/* ---------------- �� MCU ����� UART���� ---------------- */

/* ---------------- � MCU ����� 2 UART �л�С��Ẻ��� ---------------- */

#ifdef DUAL_UART

/*****************/
/* Initial UART0 */
/*****************/
void init_serial0 (unsigned int ubrr)  
{
  /* Set baud rate */
  UBRR0H = (unsigned char)(ubrr>>8);
  UBRR0L = (unsigned char)ubrr;

/* Enable receiver and transmitter */ // enable RX interrupt
  UCSRB = (1<<RXEN)|(1<<TXEN); // | (1 << RXCIE);

  /* Set frame format: 8data, 2stop bit */
  UCSR0C = (1<<USBS0)|(3<<UCSZ00);  
}
  
/******************************/
/* Write Character To UART[0] */
/******************************/
void putchar0 (unsigned char data)  
{
  if (data == '\n')  
  {
    while (!( UCSR0A & (1<<UDRE0)));
    UDR0 = 0x0D;								  
  }

  while (!( UCSR0A & (1<<UDRE0)));
  UDR0 = data;
}
/******************************/
/* Get character From UART[0] */
/******************************/
unsigned char getchar0()  
{
  while(!(UCSR0A & (1<<RXC0)));
  return (UDR0);
}

/***************************/
/* Print String to UART[0] */
/***************************/
void print_uart0(void)
{
  char *p;
  p = uart_buf;

  do
  {
    putchar0(*p);
    p++;
  }
  while(*p != '\0');

  return;
}
/*****************/
/* Initial UART1 */
/*****************/
void init_serial1 (unsigned int ubrr)  
{
  /* Set baud rate */
  UBRR1H = (unsigned char)(ubrr>>8);
  UBRR1L = (unsigned char)ubrr;

  /* Enable receiver and transmitter */
  UCSR1B = (1<<RXEN1)|(1<<TXEN1);

  /* Set frame format: 8data, 2stop bit */
  UCSR1C = (1<<USBS1)|(3<<UCSZ10);
}
/******************************/
/* Write Character To UART[1] */
/******************************/
void putchar1 (unsigned char data)  
{
  if (data == '\n')  
  {
    while(!( UCSR1A & (1<<UDRE1)));
    UDR1 = 0x0D;									  
  }

  while ( !( UCSR1A & (1<<UDRE1)));
  UDR1 = data;
}

/******************************/
/* Get character From UART[1] */
/******************************/
unsigned char getchar1()  
{
  while(!(UCSR1A & (1<<RXC1)));
  return (UDR1);
}
/***************************/
/* Print String to UART[1] */
/***************************/
void print_uart1(void)
{
  char *p;
  p = uart_buf;

  do
  {
    putchar1(*p);
    p++;
  }
  while(*p != '\0');

  return;
}
#endif
/* ------------------------------------- ����ǹ Dual Uart ------------------------------- */


/***************************
    ������ҧ�����
****************************/
// Main Start Here
/*
int main(void)
{  
  unsigned char uart_data;

  init_serial0(MYUBRR);	  
  init_serial1(MYUBRR);
    
  // UART[0] Print String //
  sprintf(uart_buf,"WinARM:AVR8 : UART[0] Demo\n\r"); // ���ѡ�� ���� uart_buf ������ print_uart0();
  print_uart0();

  // �ա������ҧ���
  // sprintf(uart_buf,"%.2f",f_var);


  // UART[1] Print String //
  sprintf(uart_buf,"WinARM:AVR8 : UART[1] Demo\n\r");
  print_uart1();

  // Loop Receive & Echo Test //
  while(1)
  {    

    // Verify Receive RS232 : UART[0]
    if(UCSR0A & (1<<RXC0))
    {
	uart_data = getchar0();
	if(uart_data==0x0D)
	{
	  sprintf(uart_buf,"WinAVR:AVR8 : UART[0]\n\r");
	  print_uart0();
	}
	else
	{
	  putchar0(uart_data);
	}
    }

    // Verify Receive RS232 : UART[1]
    if(UCSR1A & (1<<RXC1))
    {
	uart_data = getchar1();
	if(uart_data==0x0D)
	{
	  sprintf(uart_buf,"WinAVR:AVR8 : UART[1]\n\r");
	  print_uart1();
	}
	else
	{
	 putchar1(uart_data);
	}
    }

  }														  

}
*/
