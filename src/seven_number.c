

#include <seven_number.h>

// ���ҧ��� ��ͧ�á �����  [�ѹ][������]
flash char YAM_DAY_TBL[8][9] = { {'0','0','0','0','0','0','0','0','0'},
                            {'0','1','6','4','2','7','5','3','1'},
                            {'0','2','7','5','3','1','6','4','2'},
                            {'0','3','1','6','4','2','7','5','3'},
                            {'0','4','2','7','5','3','1','6','4'},
                            {'0','5','3','1','6','4','2','7','5'},
                            {'0','6','4','2','7','5','3','1','6'},
                            {'0','7','5','3','1','6','4','2','7'}
                            };
                            
// ���ҧ��� ��ͧ�á �����
flash char YAM_NIGHT_TBL[8][9] = {{'0','0','0','0','0','0','0','0','0'},
                                    {'0','1','5','2','6','3','7','4','1'},
                                    {'0','2','6','3','7','4','1','5','2'},
                                    {'0','3','7','4','1','5','2','6','3'},
                                    {'0','4','1','5','2','6','3','7','4'},
                                    {'0','5','2','6','3','7','4','1','5'},
                                    {'0','6','3','7','4','1','5','2','6'},
                                    {'0','7','4','1','5','2','6','3','7'}
                                    };

// ���ҧ�����ҧ�ѹ��¹ �����ѧ
flash char YAM_DAY_BACK_TBL[8][8] = { {'0','0','0','0','0','0','0','0'},
                            {'0','3','5','7','2','4','6','1'},
                            {'0','4','6','1','3','5','7','2'},
                            {'0','5','7','2','4','6','1','3'},
                            {'0','6','1','3','5','7','2','4'},
                            {'0','7','2','4','6','1','3','5'},
                            {'0','1','3','5','7','2','4','6'},
                            {'0','2','4','6','1','3','5','7'}
                            };
                            
// �纼Ţͧ��äӹǹ�����觡�Ѻ
unsigned char m_7base[8];

// ��Ǩ����� ��� �ͧ�ѹ���� ���Ţ����
// part day = 'D', night = 'N'
unsigned char get_yam_number(char part,char day,char yam)
{
    if (part == 'D')
        return YAM_DAY_TBL[day][yam];
    return YAM_NIGHT_TBL[day][yam];
}

// �ŧ�������Թҷ�
unsigned long int time_2sec(unsigned char hh,unsigned char mm,unsigned char ss)
{   
    if (hh >= 18 && hh <= 24) 
        hh -= 12;
    else if (hh >= 0 && hh <= 5)
        hh += 12;                   
        
    return (((hh*60UL)+mm)*60UL)+ss;    
}
// ��Ǩ��������ҹ��� �������ǧ����������
unsigned char get_yam_period(unsigned char hh,unsigned char mm,unsigned char ss)
{    
    return ((time_2sec(hh,mm,ss) - 21600) / 5400)+1;
}

// ��Ǩ��������ҹ��� ������ѧ������� 1-3
unsigned char get_yam_rithm(unsigned char hh,unsigned char mm,unsigned char ss)
{
    unsigned int second;
    //formula
    second = (unsigned int)( 5400UL-((get_yam_period(hh,mm,ss)*90UL*60UL+21600) - time_2sec(hh,mm,ss)) );
    
    if (second < 1800)
        return 1;
    if (second < 3600)
        return 2;
    return 3;
}

/* 
**
** ����� function �ӹǹ �Ţ 7 ��� 9 �ҹ 
** �������ö���¡��ҹⴴ� ���ͧ���¡��ҹ
** get_base_from_bd()
**
*/
// clear ��ҷ���纡�͹˹�� �����ͼš�äӹǹ
static void clear_base(void)
{
    unsigned char count;
    for (count = 1; count <= 7; count++)
        m_7base[count] = 0;
}

// �ӹǹ��� �ҹ 123
static unsigned char * get_base_123(unsigned char dd, unsigned char mm, unsigned char yy,
    unsigned char base)
{
    unsigned char index;
    unsigned char tmp_value;
    
    if (base >= 1 && base <= 3)
    {
        if (base == 1) 
            tmp_value = dd;
        else if (base == 2)
            tmp_value = mm;
        else if (base == 3)
            tmp_value = yy;
        for (index = 1; index <= 7; index++)
        {
            m_7base[index] = tmp_value;
            tmp_value += 1;
            if (tmp_value > 7)
                tmp_value -= 7;
        } 
    }
    return (unsigned char *)m_7base;
}

// �ӹǹ��Ұҹ 4
static unsigned char * get_base_4(unsigned char dd, unsigned char mm, unsigned char yy)
{
    unsigned char index;
    unsigned char tmp_value;
    
    for (index = 1; index <= 7; index++)
    {
        // �ѹ
        tmp_value = dd + index - 1;
        if (tmp_value > 7)
            tmp_value -= 7;
        m_7base[index] = tmp_value;
        // ��͹
        tmp_value = mm + index - 1;
        if (tmp_value > 7)
            tmp_value -= 7;
        m_7base[index] += tmp_value;
        // ��
        tmp_value = yy + index - 1;
        if (tmp_value > 7)
            tmp_value -= 7;
        m_7base[index] += tmp_value;
    }
    return (unsigned char*)m_7base;
}

// �ӹǹ Ǵ� ��Ф׹��Ұҹ��� 5
static unsigned char * get_base_5(unsigned char dd, unsigned char mm, unsigned char yy)
{                        
    unsigned char count;
    // initial base 4;
    get_base_4(dd,mm,yy);
    for (count = 1; count <= 7; count++) 
    {
        m_7base[count] = m_7base[count] - ((m_7base[count] /7) * 7);
        if (m_7base[count] == 0)
            m_7base[count] = 7;
    }
    return (unsigned char*)m_7base;
}

// �ӹǹ Ǵ� ��Ф׹��Ұҹ��� 6,7
static unsigned char * get_base_67(unsigned char dd, unsigned char mm, unsigned char yy
    , unsigned char base)
{                        
    unsigned char twice = 1;
    unsigned char count;
    // initial_base 5;
    get_base_5(dd,mm,yy);
    
    if (base == 7)
        twice = 2;
    while (twice > 0)
    {
        for (count = 1; count <= 7; count++)
        {
            m_7base[count] = m_7base[count] * 2;
            m_7base[count] = m_7base[count] - ((m_7base[count] /7) * 7);
            if (m_7base[count] == 0)
                m_7base[count] = 7;
        }  
        twice -= 1;
    }    
    return (unsigned char*)m_7base;
}

// �ӹǹ Ǵ� ��Ф׹��Ұҹ��� 8
static unsigned char * get_base_8(unsigned char dd, unsigned char mm, unsigned char yy)
{
    unsigned char count;
    unsigned char first_num;
    // initial base 5
    get_base_5(dd,mm,yy);
    // �纤���Ţ����á
    first_num = m_7base[1];
    // �Թ�����ҧ�ѹ ��¹�Թ˹��
    for(count = 1; count <=7; count++)
    {
        m_7base[count] = YAM_DAY_TBL[first_num][count];
        // change char to number
        m_7base[count] -= 48;
    }
    return (unsigned char*)m_7base;
}

// �ӹǹ Ǵ� ��Ф׹��Ұҹ��� 9
static unsigned char * get_base_9(unsigned char dd, unsigned char mm, unsigned char yy)
{
    unsigned char count;
    unsigned char last_num;

    // initial base 5
    get_base_5(dd,mm,yy);
    // �纤���Ţ�ش���� ����á
    last_num = m_7base[7];
    
    // initial base 8
    get_base_8(dd,mm,yy);
    // �纤���Ţ�ش���� ��� 2
    last_num += m_7base[7];
    
    // ��Ѻ�Ţ�� 7
    last_num = last_num - ((last_num /7) * 7);
    if (last_num == 0)
        last_num = 7;
    
    // �Թ�����ҧ�ѹ ��¹�����ѧ
    for(count = 1; count <=7; count++)
    {
        m_7base[count] = YAM_DAY_BACK_TBL[last_num][count];
        // change char to number
        m_7base[count] -= 48;
    }
    return (unsigned char*)m_7base;
}

// �ӹǹ Ǵ� ��Ф׹��Ұҹ����ͧ���
unsigned char * get_base_from_bd(unsigned char dd, unsigned char mm, unsigned char yy,
    unsigned char base)
{    
    clear_base();
    if (dd > 7)
        dd -= 7;
    if (mm > 7)
        mm -= 7;
    if (yy > 7)
        yy -= 7;
    
    if (base >= 1 && base <= 3)
    {
        return get_base_123(dd,mm,yy,base);
    }
    // �ҹ����Ţ
    else if (base == 4)
    {
        return get_base_4(dd,mm,yy);
    }
    else if (base == 5)
    {
        return get_base_5(dd,mm,yy);
    }
    else if (base >= 6 && base <= 7)
    {
        return get_base_67(dd,mm,yy,base);
    }
    else if (base == 8)
    {
        return get_base_8(dd,mm,yy);
    }
    return get_base_9(dd,mm,yy);
}