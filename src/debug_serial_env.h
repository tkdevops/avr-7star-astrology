
/* ------------------------------- ��Ҿ�Ǵ���� debug output � UART ---------------------------- */

/*********************************************/
/* Examples Program For "AVR8(ATMEGA128)     */
/* Target MCU  : ATMEL AVR8 : ATMEGA128      */
/*		 : X-TAL : 16.00 MHz	             */
/* Text Editor : AVR Studio V4.14(Build589)  */
/* Compiler    : WinAVR 2008-06-10	         */
/* Function    : Example Use UART0,UART1     */
/*********************************************/

// �����˵� 
// ATMEGA128 �� 2 UART ��Ҩ���Ѻ MEGA8 ��ͧ���  DUAL_UART = 0

/********************************************************************
// UART config : �����Ѻ MCU ����� 2 UART , ��� MCU �� UART ������� comment
*********************************************************************
//#define DUAL_UART 1
********************************************************************/
					   					  
// ��Ҿ�Ǵ���� debug ������¡����ѧ�ش
#include <avr/io.h>
#include <stdio.h>

#ifndef F_CPU
    #define F_CPU 12000000UL
#endif

#include <util/delay.h> // ���¡����ѧ�ҡ ��С�� F_CPU

// ��˹� baudrate
#define BAUD 19200
#define MYUBRR F_CPU/16/BAUD-1

// UART Buffer
//#define MAX_BUFF    20
// �纵���ѡ�����㹹�� �͡����
char uart_buf[];

/* pototype  section */
void init_serial0 (unsigned int ubrr);                    /* Initial UART0 */
void putchar0(unsigned char data);						  /* Write Character To UART[0] */
unsigned char  getchar0(void);							  /* Get character From UART[0] */
void print_uart0(void);                                   /* Print String to UART[0] */

#ifdef DUAL_UART
void init_serial1 (unsigned int ubrr);                    /* Initial UART1 */
void putchar1(unsigned char data);						  /* Write Character To UART[1] */
unsigned char  getchar1(void);							  /* Get character From UART[1] */
void print_uart1(void);                                   /* Print String to UART[1] */
#endif


/* ------------------------------- ����Ҿ�Ǵ���� debug output � UART ---------------------------- */
