/*
 * ��кҷ���稾�����ҸԺ������Թ�����Ǫ����ظ� ������خ�������������� �繾����ҡ�ѵ�����Ѫ��ŷ�� � ��觾�к���Ҫ�ѡ��ǧ��
 * �ʴ稾���Ҫ��������� �ѹ����� ��͹��� ��� � ��� �����ç �ç�Ѻ�ѹ��� � ���Ҥ� �.�. ����
 * �繾���Ҫ���ʾ��ͧ���� �� 㹾�кҷ���稾�Ш�Ũ����������������
 * �����Ҫ���ѵ�������ѹ������� �� ���Ҥ� �ը� �ط��ѡ�Ҫ ����
 * ����ʴ����ä�������ѹ��� �� ��Ȩԡ�¹ �.�. ���� �����Ъ������� �� ����� �ʴ稴�ç�Ҫ���ѵ���� �� ��
 *
 */

//##########################################################################
//
// Programname : LunarDateCalc
// Objective :
//   - ������ӹǹ��ԷԹ�ѹ�ä�� 176 �� �����  1/4/2400 - 31/3/2575
//   - ��������¹���� code ������������Ҩе�ͧ port 价�� Microcontroller
// Programmer : BenNueng
// Email : BenNueng@Gmail.com
// StartDate : 24/06/2551 15:32
// StopDate : 25/06/2551 22:32
//
// links :
// - �������¹���Ҿط��ѡ�Ҫ : http://th.wikipedia.org/wiki/�ط��ѡ�Ҫ
// - �ʴ�������ҧ��õѴ��ԷԹ : http://www.horauranian.com/index.php?lay=boardshow&ac=webboard_show&Category=horauraniancom&thispage=7&No=254466
//
//##########################################################################

#include <lunar_process.h>

// ��С�Ȥ�Ҥ����� flash

// �ѹ㴪�������ѹ���� , ��ͧ�á�� free ���͡�˹��������
// 8 = �ӹǹ��, 3 = ������ǵ���ѡ�÷����Ƿ���ش(2) + '\0'
flash unsigned char ENG_DAY_TBL[8][3] = {"fr","Su","Mo","Tu","We","Th","Fr","Sa"};

// ���ҧ�纪����ѹ��� Ẻ���� , ��ͧ�á�� free
flash unsigned char THAI_DAY_TBL[8][3] = {"fr","��"," �"," �"," �","��"," �"," �"};

// ���ҧ�纪�����͹�� Ẻ���� , ��ͧ�á�� free
flash unsigned char THAI_MONTH_TBL[13][5] = {"free","��.","��.","�դ.","���","��.","���.","��.","ʤ.","��.","��.","��.","��."};

// ���ҧ�纪�����͹�ҡ� Ẻ���� , ��ͧ�á�� free
flash unsigned char ENG_MONTH_TBL[13][4] = {"fre","Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"};

// ���ҧ�纪��ͻչѡ�ѵ� , ��ͧ�á�� free
flash unsigned char ANIMAL_YEAR_TBL[13][7] = {"free","�Ǵ","���","���","���","���ç","�����","������","����","�͡","�С�","��","�ع"};

// �������͹ (�ҡ�) ��ա���ѹ , ��ͧ�á�� 0 ���͡�˹��������
flash unsigned char TOTAL_DAY_TBL[] = {0,31,28,31,30,31,30,31,31,30,31,30,31};

// �������͹ (��) ���͹� �ա���ѹ , ��ͧ�á�� 0 �����������
flash unsigned char LUNAR_DAY_TBL[] = {0,14,15,14,15,14,15,14,15,14,15,14,15};

        /* original
        int[] LUNAR_YEAR_TBL = new int[]{
        0,2,0,1,2,0,2,0,1,2,0,0,2,0,1,2,0,2,0,1,
        2,0,0,2,1,2,0,0,2,0,1,2,0,2,0,1,2,0,0,2,
        0,1,2,0,2,0,1,2,0,0,2,1,2,0,0,2,0,1,2,0,
        1,2,0,2,0,0,2,0,1,2,0,2,1,0,2,0,1,2,0,1,
        2,0,2,0,0,2,0,2,1,0,2,0,1,2,0,1,2,0,0,2,
        1,2,0,0,2,0,1,2,0,2,0,0,2,1,0,2,1,0,2,0,
        2,0,1,2,0,0,2,0,2,0,1,2,0,1,2,0,2,0,0,2,
        1,0,2,1,0,2,0,2,0,1,2,0,1,2,0,2,0,1,2,0,
        0,2,0,0,2,0,2,0,1,2,0,0,2,1,2,0};
        */

        /*
         * ����ó� ������ѹ��� 1/1/2484 - 31/3/2575
         * ���ҧ��˹���һչ��� �繻ջ������
         * 0 = ������� �������
         * 1 = ������� ͸ԡ���
         * 2 = ͸ԡ��� �������
         int []LUNAR_YEAR_TBL = new int[]{
            0, // ����� 1/1/2484
            0,2,0,2,1,0,2,0,1,2,0,1,2,0,0,2,1,2,
            0,0,2,0,1,2,0,2,0,0,2,1,0,2,1,0,2,0,
            2,0,1,2,0,0,2,0,2,0,1,2,0,1,2,0,2,0,0,2,
            1,0,2,1,0,2,0,2,0,1,2,0,1,2,0,2,0,1,2,0,
            0,2,0,0,2,0,2,0,1,2,0,0,2,1,2,0};
         */

// 1/1/2423 - 31/3/2575
flash unsigned char LUNAR_YEAR_TBL[] = {
            2,1,2,0,0,2,0,1,2,0,2,0,1,2,0,
            0,2,0,1,2,0,2,0,1,2,0,0,2,1,2,
            0,0,2,0,1,2,0,1,2,0,2,0,0,2,0,
            1,2,0,2,1,0,2,0,1,2,0,1,2,0,2,
            0,0,2,0,2,1,0,2,0,1,2,0,1,2,0,
            0,2,1,2,0,0,2,0,1,2,0,2,0,0,2,
            1,0,2,1,0,2,0,2,0,1,2,0,0,2,0,
            2,0,1,2,0,1,2,0,2,0,0,2,1,0,2,
            1,0,2,0,2,0,1,2,0,1,2,0,2,0,1,
            2,0,0,2,0,0,2,0,2,0,1,2,0,0,2,
            1,2,0,99};

// ��˹���Ңͧ �շ���ҡ����ش�������ö���� (��ͤ�� year_table ����ͧ)
// �����¡��ǹ�ٻ�ͺ ���ǵ�Ǩ�շ��ŧ���´��� 99 (��蹤�ͨش����ش)
unsigned int MAX_YEAR = 0;


    /* ======================== ����� Global �������к� ============================ */
    // ��С�ȵç���������� compile ��ҹ��� ���з�� process ���ա�á�˹��������ա����

		    /* //����ó������ ��Ѻ�� 1/1/2484
		    //# ��˹�����������
		    unsigned char m_day_name = 4;  //# �����ѹ �������Ф��ʵ������͹�ѹ
		    unsigned char m_day_no = 1; //# �ѹ��� �������Ф��ʵ������͹�ѹ
		    unsigned char m_month_no = 1; //# ��͹�ҡ�

		    unsigned int m_christ_year = 1941; //# ���ҡ�
		    unsigned int m_thai_year = 2484; //# �վ.�.

		    bool m_phase_up = true; //# ��ҧ���(true) ���͢�ҧ���(false)
		    unsigned char m_lunar_phase = 4; //# �Զ�
		    unsigned char m_thai_month = 2; //# ��͹�� ����ѹ�ä��
		    unsigned char m_animal_year = 5; //# �չѡ�ѵ� ��������� ���ç

		    bool month_twiced = false; //# ��˹���ùѺ��͹ 8 2 ����

		    //# ���Ի�ԷԹ�¨�����¹�� �.�. ����� ��͹ 4, ���Ҽ�ҹ�� 2483 仨�����¹����͹ 1 ����͹���ʵ�
		    unsigned char m_change_year_month = 1; //# 4
		    */

    //# ��˹�����������  ������� 1/1/2423
    // ����õ�ҧ� ��С���Ҩҡ header
    unsigned char m_day_name = 7;  //# �����ѹ �������Ф��ʵ������͹�ѹ
    unsigned char m_day_no = 1; //# �ѹ��� �������Ф��ʵ������͹�ѹ
    unsigned char m_month_no = 1; //# ��͹�ҡ�

    unsigned int m_thai_year = 2423; //# �վ.�.
    unsigned int m_christ_year = 1881; //# ���ҡ�

    bool m_phase_up = true; //# ��ҧ���(true) ���͢�ҧ���(false)
    unsigned char m_lunar_phase = 2; //# �Զ�
    unsigned char m_thai_month = 2; //# ��͹�� ����ѹ�ä��
    unsigned char m_animal_year = 5; //# �չѡ�ѵ� ��������� ���ç

    bool month_twiced = false; //# ��˹���ùѺ��͹ 8 2 ����
    unsigned int m_year_count = 0; // ��ǹѺ�ͺ ��ùѺ��
    /* ========================== end =================================== */


// ���Ҥ��㹵��ҧ ���ʹ���Ҥ���٧�ش�ͧ�դ������
void setup_max_year(void)
{
    unsigned int count = 0;
    if (MAX_YEAR == 0)
    {
        for(count = 0; LUNAR_YEAR_TBL[count] != 99; count++);
        MAX_YEAR = count;
    }
}

//# ��Ǩ�ͺ��һչ��� ��͹ ��.�� 29 �ѹ������� (�� �.�.)
unsigned char is_leap_year(unsigned int year)
{
    //# �����ô��� 4 ŧ��ǵ�ͧ�ҾԨ�ó��ա����..
    if ((year % 4) == 0)
    {
    	//# �����ô��� 100 ŧ����ա ��ͧ�Ԩ�ó��ա
	    if ((year % 100) == 0)
	        //# �����ô��� 400 ŧ����ա �ʴ������ ͸ԡ��÷Թ
	        return (year % 400 == 0);
	    else
	        //# �����ô��� 100 ���ŧ��� �ʴ�����繻�͸ԡ��÷Թ
	        return true;
    }
    //# �����ô��� 4 ���ŧ��� �繻ջ��� ��͹
    return false;
}

//# ����¹ �� �.�. �� �.�.
unsigned int change_b2c(unsigned char month, unsigned int thai_year)
{
    unsigned int year = thai_year;
    if (year >= 2483)
    {
        year = year - 543;
    }
    else
    {
    	if ((month >= 1) && (month <= 3))
	        year = year - 542;
	    else
    	    year = year - 543;
    }
    return year;
}

//# ��Ǩ�ͺ Ǵ� ��Ҷ١��ͧ�������
unsigned char chk_valid_date(unsigned char day, unsigned char month, unsigned int year)
{
    //# ��͹�ŧ�繻� �.�.
    unsigned int cyear = change_b2c(month,year);

    //# 1. ���ѹ�������ը�ԧ������� (1 �.�. - 31 �դ. 2483)
    if ((year == 2483) && (month >= 1 && month <= 3))
        return 0; //# "error : date not found"

    //# 2. �ѹ����Թ��˹�������� 2400 <= x <= 2575
    if ((year < 2400) || (year > 2575))
        return 10; //# "error : date overflow"

    //# 3. ������ѹ�������ը�ԧ㹻� 2400 (�չ������ ��͹ 1,2,3 �͢����͹ 4 ���繻� 2401)
    if ((year == 2400) && (month >= 4 && month <= 12))
        return 20;

    //# ��Ǩ�ͺ�����١��ͧ
    //# 3. ���ѹ��� 29 �.�. ���͹���������������
    if (!is_leap_year(cyear) && (day == 29 && month == 2))
	    return 30; // # "error : 29 mar in leap year"

    return true;
}

//# ��Ǩ����ѹ���Ѩ�غѹ���� �ç�Ѻ�ѹ����ͧ����������
unsigned char find_date(unsigned char fday,unsigned char fmonth,unsigned int fyear,unsigned char day,unsigned char month,unsigned int year)
{
    return ((fday == day) && (fmonth == month) && (fyear == year));
}

// bug ��ͧ��Ǩ��ҧ���/��� ����
//# ��Ǩ��� �ѹ�Ѩ�غѹ���ѹ����������
// waxing_moon = true = ��ҧ���
// waxing_moon = false = ��ҧ���
unsigned char chk_monk_day(unsigned char waxing_moon, unsigned char lunar_phase,unsigned char thai_month,unsigned char adikawan_year)
{
    // ������Ѻ !!
    // ��� 14 �����͹ 7 ����繻�͸ԡ��� (���� 30�ѹ) ��������ѹ���
    if (!waxing_moon && lunar_phase == 14 && thai_month == 7 && adikawan_year)
    {
        return false;
    }
    // ������ѹ���������� 15 ��� ���� 8 ���
    if (lunar_phase == 15 || lunar_phase == 8)
        return true;

    // �������� 14 �����͹�Ţ��� �����ѹ���
    if ( !waxing_moon &&  (lunar_phase == 14) && (thai_month % 2 == 1) )
        return true;

    return false;
}


//# �����ѹ����к� ������ѹ���� , arg year = �.�. Ẻ��, �ѹ��� ��͹Ẻ�ҡ�
//# �� function ���ŧ�.�. �� �.�. ���ǹ����令ӹǹ�ͧ���ѵ��ѵ�
unsigned char process(unsigned char day,unsigned char month,unsigned int year)
{
    //# ���Ի�ԷԹ�¨�����¹�� �.�. ����� ��͹ 4, ���Ҽ�ҹ�� 2483 仨�����¹����͹ 1 ����͹���ʵ�
    unsigned char m_change_year_month = 4;
    // �纤�ҡ�õ�Ǩ �ѹ������ �١��ͧ�������
    unsigned char result_chk_valid_date = 0;

    // ��ҷ���ͧ����颳�������ٻ
    bool change_month = false;  //# �纤�ҡ�õ�Ǩ�ͺ��Ҩе�ͧ������͹�������
    unsigned long int	total_day_count = 0; // count all day at start
    unsigned char chk_ram = 14;  // ��͹��Ǩ ��ҧ���

    /* ======================== ����� Global �������к� ============================ */

		    /* //����ó������ ��Ѻ�� 1/1/2484
		    //# ��˹�����������
		    unsigned char m_day_name = 4;  //# �����ѹ �������Ф��ʵ������͹�ѹ
		    unsigned char m_day_no = 1; //# �ѹ��� �������Ф��ʵ������͹�ѹ
		    unsigned char m_month_no = 1; //# ��͹�ҡ�

		    unsigned int m_christ_year = 1941; //# ���ҡ�
		    unsigned int m_thai_year = 2484; //# �վ.�.

		    bool m_phase_up = true; //# ��ҧ���(true) ���͢�ҧ���(false)
		    unsigned char m_lunar_phase = 4; //# �Զ�
		    unsigned char m_thai_month = 2; //# ��͹�� ����ѹ�ä��
		    unsigned char m_animal_year = 5; //# �չѡ�ѵ� ��������� ���ç

		    //# ���Ի�ԷԹ�¨�����¹�� �.�. ����� ��͹ 4, ���Ҽ�ҹ�� 2483 仨�����¹����͹ 1 ����͹���ʵ�
		    unsigned char m_change_year_month = 1; //# 4
		    */

    //# ��˹�����������  ������� 1/1/2423
    // ����õ�ҧ� ��С���Ҩҡ header
    m_day_name = 7;  //# �����ѹ �������Ф��ʵ������͹�ѹ
    m_day_no = 1; //# �ѹ��� �������Ф��ʵ������͹�ѹ
    m_month_no = 1; //# ��͹�ҡ�

    m_thai_year = 2423; //# �վ.�.
    m_christ_year = 1881; //# ���ҡ�

    m_phase_up = true; //# ��ҧ���(true) ���͢�ҧ���(false)
    m_lunar_phase = 2; //# �Զ�
    m_thai_month = 2; //# ��͹�� ����ѹ�ä��
    m_animal_year = 5; //# �չѡ�ѵ� ��������� ���ç

    month_twiced = false; //# ��˹���ùѺ��͹ 8 2 ����
    /* ========================== end =================================== */

    // ������������� ���٧�ش�������� �ա���
    setup_max_year();
    //# ��Ǩ�����١��ͧ
    result_chk_valid_date = chk_valid_date(day,month,year);
    if (result_chk_valid_date == true)
    {
        //# ��������ǹ�ͺ�ӹǹ �����Ҩ������ҷ�����ԧ�͡�� !!
        bool found_date = find_date(m_day_no, m_month_no, m_thai_year, day, month, year);
        m_year_count = 0; //# ��ǹѺ�ͺ��ҵ͹������� ������������ lunar_year_type ��ͧ�
        month_twiced = false; //# ��˹���ùѺ��͹ 8 2 ����

        while ((m_year_count < MAX_YEAR) && (!found_date))
	    {
	        // ��Ǩ�Ѻ��������ҷ���������ѹ (�ѧ��������������ª������)
	        total_day_count++;

	        //# ===================== �ӹǹ Ǵ� �ҡ� ================================
	        //# �����ѹ �������Ф��ʵ������͹�ѹ
	        m_day_name += 1;
	        if (m_day_name > 7)
	        {
		        m_day_name = 1;
	        }
            change_month = false;  //# �纤�ҡ�õ�Ǩ�ͺ��Ҩе�ͧ������͹�������
	        //# +�ѹ��� (�������Ф��ʵ������͹�ѹ)
	        m_day_no += 1;
	        //# ����� ��͹ 2 (�.�.) ��лչ���� 29 �ѹ
	        if (m_month_no == 2 && is_leap_year(m_christ_year))
	        {
		        if (m_day_no > 29)  //# ��Ǩ��� 29
		            change_month = true;
	        }
	        else
	        {
		        //# �������͹���� ��Ǩ������, ����ҧ�������
		        if (m_day_no > TOTAL_DAY_TBL[m_month_no])
		            change_month = true;
            }

	        //# ����͵�Ǩ�ͺ������� ��ͧ������͹�ҡ�
	        if (change_month == true)
	        {
		        //# reset �ѹ ��Ѻ���ѹ��� 1 ����͹���
		        m_day_no = 1;
		        //# ����¹��͹����
		        m_month_no += 1;
		        if (m_month_no > 12)
		        {
		            m_month_no = 1;
		            //# ������ �ҡ�
		            m_christ_year += 1; //# ���ҡ�
		        }
	        }

	        //# ===================== �ӹǹ Ǵ� �.�. ===================================
	        //# ���Ի�ԷԹ�¨�����¹�� �.�. ����� ��͹ 4 (m_change_year_month = 4)
	        //# ���Ҽ�ҹ�� 2483 仨�����¹����͹ 1 ����͹���ʵ�
	        //#
	        if ((m_day_no == 1) && (m_month_no == m_change_year_month))
	        {
		        m_thai_year += 1; //# + �վ.�.
	        }

	        //# ����� 31 �.�. 2483 �ѹ����¹ �.�.����ѹ��� 1 �.�. �ء��
	        if ((m_day_no == 31) && (m_month_no == 12 && m_thai_year == 2483))
		        m_change_year_month = 1;

	        //# ===================== �ӹǹ Ǵ� �ѹ�ä�� ================================
	        //# +�Զ�
	        m_lunar_phase += 1;
	        //# ��� + ���Թ�� 16 ��Ӣ�ҧ��� �������¹�繢�ҧ���
	        if ((m_phase_up == true) && (m_lunar_phase > 15))
	        {
		        m_phase_up = false;
		        m_lunar_phase = 1;
	        }

	        //# ��˹������������ͧ��õ�Ǩ�ͺ
	        chk_ram = 14;
	        //# ����繢�ҧ��� ��ͧ�������͹�������������� ��������¹�繢�ҧ���
	        //# ͸ԡ��� ���� ��� 15 ���
	        if ((LUNAR_YEAR_TBL[m_year_count] == 1) && (m_thai_month == 7))
		        chk_ram = 15;
	        else
		        //# ������� ���� ͸ԡ����ѧ�����ٻẺ���
		        chk_ram = LUNAR_DAY_TBL[m_thai_month];

	        //# ����Ң�ҧ����ҡ���ҷ���������ѧ ����Թ�����������¹�� ��ҧ���
	        if ((m_phase_up == false) && (m_lunar_phase > chk_ram))
	        {
		        //# ����¹�繢�ҧ���
		        m_phase_up = true;
		        //# 1 ���
		        m_lunar_phase = 1;
		        //# �����͹����
		        m_thai_month += 1;
		        //# 㹡óշ�� ��Ẻ ͸ԡ��� �������͹ 8(+1=9) �������¹Ѻ�ҡ�͹ ����ͧ�����͹����
		        if ((LUNAR_YEAR_TBL[m_year_count] == 2) && (m_thai_month == 9) && (month_twiced == false))
		        {
		            month_twiced = true;
		            m_thai_month -= 1;
		            //# �� clear ����͢�� m_year_count ����
		        }

		        //# �������͹ 5 �������¹�չѡ�ѵ�
		        if (m_thai_month == 5)
		        {
		            //# �ǡ���ҧ�ѹ��� ��Шӻ������ա 1
		            m_year_count += 1;

		            //# �ǡ�չѡ�ѵ�
		            m_animal_year += 1;
		            //# ��һչѡ�ѵ��ҡ���� 12 �����͹���� 1
		            if (m_animal_year > 12)
			            m_animal_year = 1;

		            //# clear ��ùѺ�����͹Ỵ 2 ����
		            month_twiced = false;
		        }
		        //# �����͹���ҡ���� 12 �����͹���� 1
		        if (m_thai_month > 12)
		        {
		            m_thai_month = 1;
		        }
	        }

	        //# ��º����ѹ�Ѩ�غѹ�� �ѹ���ç������� (�.�.)
	        found_date = find_date(m_day_no, m_month_no, m_thai_year, day, month, year);
	    } // while
	    //# ��Ҥú����٧�ش ��������� �ʴ���ҼԴ��Ҵ
	    if (m_year_count >= MAX_YEAR)
	        return 40; //# "error 10: date overflow"

	    //# �ç����ʴ������ !!
	        //sprintf(display_line[0],"%d/%d/%d(%d)\0    ",m_day_no,m_month_no,m_thai_year,m_christ_year);
	        /* work ! */
	        //tmp_string_len = strlenf(ENG_DAY_TBL[m_day_name]);
	        //memcpyf(tmp_string, ENG_DAY_TBL[m_day_name], tmp_string_len );
	        //sprintf(display_line[1],"%s(%d)            ",tmp_string, m_day_name);
	        //sprintf(display_line[2],"ph = %d(%c%d)%d        ",m_day_name, m_phase_up ? '+' :'-',m_lunar_phase,m_thai_month);
	        //sprintf(display_line[3],"ch zodiac = %d       ",m_animal_year);
        return 1; //# error 1 = found !
    } // main if

    return result_chk_valid_date ; //# error from date valid:
}

/****************************************************/
/* Example Program For ATMEGA32                     */
/* MCU      : ATMEGA32(XTAL=16 MHz)                 */
/*          : Frequency Bus = 16 MHz                */
/* Compiler : CodeVisionAVR 1.25.8a Professional    */
/* Write By : BenNueng@gmail.com                    */
/* Function : Demo Interface LCD Nokia-5110         */
/*          : Use Pin I/O Generate SPI Signal       */
/*          : Thai language                         */
/****************************************************/
/* Interface LCD ETT-NOKIA-5110 By AVR Function     */
/*          : use default wire with config          */
/*            by lcd-control.h                      */
/****************************************************/


/* include section */
#include <lcd-5110-thai.h>
	#ifndef __SLEEP_DEFINED__
	#define __SLEEP_DEFINED__
	.EQU __se_bit=0x80
	.EQU __sm_mask=0x70
	.EQU __sm_powerdown=0x20
	.EQU __sm_powersave=0x30
	.EQU __sm_standby=0x60
	.EQU __sm_ext_standby=0x70
	.EQU __sm_adc_noise_red=0x10
	.SET power_ctrl_reg=mcucr
	#endif

flash unsigned char numeric_tbl[] = {'0','1','2','3','4','5','6','7','8','9'};

// ��˹�������������
/* ��С�ȵ���� GLOBAL ����ͧ����к� VIDEO RAM */
unsigned char VIDEO_RAM[LCD_NOKIA_5110];
// MODE ; 0 = normal (english 14x6 line), 1 = graphics (Thai 14x3 line)
unsigned char LCD_MODE = LCD_GRAPHICS;
// ���˹� x,y ������Ҵ (���˹�Ҩ�, ����к�) (axis)
unsigned char ax = 0;
unsigned char ay = 0;
// ���� buffer �纡�û����ż������ʴ�˹�Ҩ�
unsigned char tmp_display_string[42];
unsigned char tmp_display_string_len;


/*****************************************/
/* �Ҵ�Ҿ�ҡ VideoRam 价�� LCD            */
/*****************************************/
void lcd_update_video_ram(void)
{
    unsigned int i = 0;        // Memory Display(Byte) Counter
    if (LCD_MODE == LCD_GRAPHICS)
    {
        lcd_write_command(128+0);   // Set X Position = 0(0..83)
        lcd_write_command(64+0);    // Set Y Position = 0(0..5)

        // 84 * 6 = LCD_NOKIA_5110
        for(i=0; i<LCD_NOKIA_5110; i++)          // All Display RAM = LCD_NOKIA_5110 Byte
            lcd_write_data(VIDEO_RAM[i]); // Fill Picture Display
    }
}

// ź�Ҿ� VIDEO RAM
void lcd_clear_video_ram(void)
{
    unsigned int i = 0;
    for(i=0; i< LCD_NOKIA_5110; i++)
        VIDEO_RAM[i] = 0;

    // reset all cursor
    ax = 0;
    ay = 0;
}

// ��˹����˹� ax,ay �Ѩ�غѹ
void lcd_set_axay(unsigned char x, unsigned char y)
{
    ax = x; ay = y;
}

// plot �ش价����˹觹��
void lcd_set_dot(unsigned char x, unsigned char y)
{
    unsigned char bitcount = 0;
    unsigned char mask = 1;
    x = x > 83 ? 83 : x;
    y = y > 47 ? 47 : y;

    bitcount = VRAM_BIT(y);
    mask <<= bitcount;
    VIDEO_RAM[VRAM_ARRAY_INDEX((unsigned int)x,(unsigned int)y)] |= mask;
}

// ����ҵ��˹� x,y ����繤������
unsigned char lcd_get_dot(unsigned char x, unsigned char y)
{
    unsigned char bitcount = 0;
    unsigned char mask = 1;
    x = x > 83 ? 83 : x;
    y = y > 47 ? 47 : y;

    bitcount = VRAM_BIT(y);
    mask <<= bitcount;
    mask = ~mask;
    mask = VIDEO_RAM[VRAM_ARRAY_INDEX((unsigned int)x,(unsigned int)y)] & mask;
    return  mask >>= bitcount ;
}

// ź�ش � ���˹觹���
void lcd_clr_dot(unsigned char x, unsigned char y)
{
    unsigned char bitcount = 0;
    unsigned char mask = 1;
    x = x > 83 ? 83 : x;
    y = y > 47 ? 47 : y;

    bitcount = VRAM_BIT(y);
    mask <<= bitcount;
    mask = ~mask;
    VIDEO_RAM[VRAM_ARRAY_INDEX((unsigned int)x,(unsigned int)y)] &= mask;
}

// �������Ҵ�ش ����ź�ش � ���˹觹��
void lcd_plot_dot(unsigned char x, unsigned char y, unsigned char dotproperty)
{
    if (dotproperty)
        lcd_set_dot(x,y);
    else
        lcd_clr_dot(x,y);
}

// ��˹���Ҩ���ҹ���� normal ���� graphics
void lcd_set_mode(unsigned char mode)
{
    if (mode == LCD_NORMAL)
    {
        LCD_MODE = LCD_NORMAL;
    }
    else
    {
        LCD_MODE = LCD_GRAPHICS;
        lcd_clear_video_ram();
    }
}

/****************************/
/* Fill Picture Display LCD */
/****************************/
void lcd_fill_picture_special(flash unsigned char *tab_picture, unsigned char write_mode)
{
    unsigned int i = 0;        // Memory Display(Byte) Counter

    lcd_write_command(128+0);   // Set X Position = 0(0..83)
    lcd_write_command(64+0);    // Set Y Position = 0(0..5)

    // 84 * 6 = 504
    for(i=0;i<504;i++)          // All Display RAM = 504 Byte
    {
        if (write_mode == WRITE_NORMAL)
        {
            VIDEO_RAM[i] = tab_picture[i]; // Fill Picture Display
        }
        else if (write_mode == WRITE_REVERSE)
        {
            VIDEO_RAM[i] = ~tab_picture[i]; // Fill Picture Display
        }
    }
}

// �Ҵ�ѡ���ŧ VIDEO RAM
void lcd_put_charxy_2vram(unsigned char x,unsigned char y, unsigned char character,unsigned char write_mode)
{
    lcd_set_axay(x,y);
    lcd_put_char_2vram(character, write_mode);
}

void lcd_put_char_2vram(unsigned char character, unsigned char write_mode)
{
    // for plot Thai lang
    unsigned char bitcount = 0;
    unsigned char tmp_data;

    // Font Size Counter
    unsigned char font_size_count = 0;
    // Font Data Pointer
    unsigned int  font_data_index;

    // Skip 0x00..0x1F Font Code
    font_data_index = character-32;
    // 5 Byte / Font
    font_data_index = font_data_index*5;

    // ��Ǩ�ͺ᡹ y ��͹�Ҵ
    ay = ay > MAX_Y ? MAX_Y : ay;
    // Get 5 Byte Font & Display on LCD
    while(font_size_count<5)
    {
        // Get Data of Font From Table & Write 2 VIDEO RAM
        tmp_data = tab_font[font_data_index];
        for (bitcount = 0; bitcount < 8; bitcount++)
        {
            // 0 = ��¹����
            if (write_mode == WRITE_NORMAL)
            {
                if ((tmp_data & 1) == 1)
                    lcd_set_dot(ax, ay + bitcount);
                else
                    lcd_clr_dot(ax, ay + bitcount);
            }
            // 1 = transparent
            else if (write_mode == WRITE_TRANSPARENT)
            {
                if ((tmp_data & 1) == 1)
                    lcd_set_dot(ax, ay + bitcount);
            }
            // 2 = reverse
            else if (write_mode == WRITE_REVERSE)
            {
                if ((tmp_data & 1) == 1)
                    lcd_clr_dot(ax, ay + bitcount);
                else
                    lcd_set_dot(ax, ay + bitcount);
            }
            // 3 = transparent reverse
            else if (write_mode == WRITE_REVERSE_TRANSPARENT)
            {
                if ((tmp_data & 1) == 1)
                    lcd_clr_dot(ax, ay + bitcount);
            }

            tmp_data >>= 1;
        }
        // ��Ǩ�ͺ
        if (ax < MAX_X)
            ax++;
        font_size_count++;  	// Next Byte Counter
        font_data_index++;  	// Next	Byte Pointer
    }
    // space 1 px
    if (ax < MAX_X)
        ax++;
}

// ����¹�Ţ��úԤ���Ţ�� �����Ҵ
void lcd_change_num_2thai(unsigned char *str_number)
{
    unsigned char length,x;
    length = strlen(str_number);
    for (x = 0;x < length;x++)
    {
        switch(str_number[x])
        {
            case '1': lcd_put_char_2vram('�', WRITE_TRANSPARENT);break;
            case '2': lcd_put_char_2vram('�', WRITE_TRANSPARENT);break;
            case '3': lcd_put_char_2vram('�', WRITE_TRANSPARENT);break;
            case '4': lcd_put_char_2vram('�', WRITE_TRANSPARENT);break;
            case '5': lcd_put_char_2vram('�', WRITE_TRANSPARENT);break;
            case '6': lcd_put_char_2vram('�', WRITE_TRANSPARENT);break;
            case '7': lcd_put_char_2vram('�', WRITE_TRANSPARENT);break;
            case '8': lcd_put_char_2vram('�', WRITE_TRANSPARENT);break;
            case '9': lcd_put_char_2vram('�', WRITE_TRANSPARENT);break;
            case '0': lcd_put_char_2vram('�', WRITE_TRANSPARENT);break;
            default : lcd_put_char_2vram( str_number[x], WRITE_TRANSPARENT);
        }
    }
}
// ��������¤������ �ҡ ax,ay ����ش
void lcd_put_stringf_2vram(flash unsigned char *string,unsigned char write_mode)
{
    // Dummy Character Count
    unsigned char i=0;
    unsigned char tmpx,tmpy;
    unsigned char CharCount = strlenf(string);

    while(i<CharCount)
    {
        tmpx = ax;
        tmpy = ay;
        // ������ѡ��  �� �� �� �� �� �� �����͹��Ѻ���¹ �٧�дѺ 1
        // 209(�ѹ�ҡ��), 212 - 215(�� - ��), 231(�������), 237-238(�Ԥ�Ե,���ä���)
        if (    (string[i] == 209) || (string[i] >= 212 && string[i] <= 215) ||
                (string[i] == 231) || (string[i] >= 237 && string[i] <= 238)    )
        {
            ax = ax - 5 > 0 ? ax - 6: ax;   // ź 5 ���Ǩ, ź 6 ���ԧ
            ay = ay - 3 > 0 ? ay - 3: ay;
            lcd_put_char_2vram(string[i], write_mode);	// Print 1-Char to LCD
            ax = tmpx;
            ay = tmpy;
        }
        // ������ѡ�� �������� �����͹��Ѻ���¹ �٧�дѺ 2
        // 232 - 236
        else if (   string[i] >= 232 && string[i] <= 236    )
        {
            ax = ax - 5 > 0 ? ax - 5: ax;   // ��ź 5 ���Ш��������ͧ�Դ˹���
            ay = ay - 5 > 0 ? ay - 5: ay;
            lcd_put_char_2vram(string[i], write_mode);	// Print 1-Char to LCD
            ax = tmpx;
            ay = tmpy;
        }
        // ����� �� �� ��(�Թ��) �����͹��Ѻ���¹ ����дѺ 1
        // 216 - 218
        else if (   string[i] >= 216 && string[i] <= 218    )
        {
            ax = ax - 5 > 0 ? ax - 6: ax;
            ay = ay + 2 > 0 ? ay + 2: ay;
            lcd_put_char_2vram(string[i], write_mode);	// Print 1-Char to LCD
            ax = tmpx;
            ay = tmpy;
        }
        // �͡�˹�ͨҡ��� �Ҵ����
        else
        {
            // ��Ǩ�ͺ���˹觡�͹����Ҵ
            if (ax + 5 > MAX_X)
            {
                ax = 0;
                if (ay + 16 > MAX_Y)
                    ay = THAI_LINE_0;
                else
                    ay += 16;
            }
            lcd_put_char_2vram(string[i], write_mode);	// Print 1-Char to LCD
        }

        i++;                        // Next Character Print
    }
}


// ��������¤������ �ҡ ax,ay ����ش
// ����͹ lcd_put_stringf_2vram() �����Ҩҡ ram
void lcd_put_string_2vram(unsigned char *string,unsigned char write_mode)
{
    // Dummy Character Count
    unsigned char i=0;
    unsigned char tmpx,tmpy;
    unsigned char CharCount = strlen(string);

    while(i<CharCount)
    {
        tmpx = ax;
        tmpy = ay;
        // ������ѡ��  �� �� �� �� �� �� �����͹��Ѻ���¹ �٧�дѺ 1
        // 209(�ѹ�ҡ��), 212 - 215(�� - ��), 231(�������), 237-238(�Ԥ�Ե,���ä���)
        if (    (string[i] == 209) || (string[i] >= 212 && string[i] <= 215) ||
                (string[i] == 231) || (string[i] >= 237 && string[i] <= 238)    )
        {
            ax = ax - 5 > 0 ? ax - 6: ax;   // ź 5 ���Ǩ, ź 6 ���ԧ
            ay = ay - 3 > 0 ? ay - 3: ay;
            lcd_put_char_2vram(string[i], write_mode);	// Print 1-Char to LCD
            ax = tmpx;
            ay = tmpy;
        }
        // ������ѡ�� �������� �����͹��Ѻ���¹ �٧�дѺ 2
        // 232 - 236
        else if (   string[i] >= 232 && string[i] <= 236    )
        {
            ax = ax - 5 > 0 ? ax - 5: ax;   // ��ź 5 ���Ш��������ͧ�Դ˹���
            ay = ay - 5 > 0 ? ay - 5: ay;
            lcd_put_char_2vram(string[i], write_mode);	// Print 1-Char to LCD
            ax = tmpx;
            ay = tmpy;
        }
        // ����� �� �� ��(�Թ��) �����͹��Ѻ���¹ ����дѺ 1
        // 216 - 218
        else if (   string[i] >= 216 && string[i] <= 218    )
        {
            ax = ax - 5 > 0 ? ax - 6: ax;
            ay = ay + 2 > 0 ? ay + 2: ay;
            lcd_put_char_2vram(string[i], write_mode);	// Print 1-Char to LCD
            ax = tmpx;
            ay = tmpy;
        }
        // �͡�˹�ͨҡ��� �Ҵ����
        else
        {
            // ��Ǩ�ͺ���˹觡�͹����Ҵ
            if (ax + 5 > MAX_X)
            {
                ax = 0;
                if (ay + 16 > MAX_Y)
                    ay = THAI_LINE_0;
                else
                    ay += 16;
            }
            lcd_put_char_2vram(string[i], write_mode);	// Print 1-Char to LCD
        }

        i++;                        // Next Character Print
    }
}

// �Ҵ�ٻ�١�ê��ŧ
void draw_down_arrow(void)
{
    unsigned char x;
    for (x=40;x<49;x++)
        lcd_set_dot(x,THAI_LINE_2);
    for (x=41;x<48;x++)
        lcd_set_dot(x,THAI_LINE_2+1);
    for (x=42;x<47;x++)
        lcd_set_dot(x,THAI_LINE_2+2);
    for (x=43;x<46;x++)
        lcd_set_dot(x,THAI_LINE_2+3);
    lcd_set_dot(44,THAI_LINE_2+4);
    lcd_update_video_ram();
}
/****************************************************/
/* Example Program For ATMEGA32                     */
/* MCU      : ATMEGA32(XTAL=16 MHz)                 */
/*          : Frequency Bus = 16 MHz                */
/* Compiler : CodeVisionAVR 1.25.8a Professional    */
/* Write By : BenNueng@gmail.com                    */
/* Function : Demo Interface LCD Nokia-5110         */
/*          : Use Pin I/O Generate SPI Signal       */
/*          : Thai language                         */
/****************************************************/
/* Interface LCD ETT-NOKIA-5110 By AVR Function     */
/* -> ATMEGA64   --> LCD Nokia-5110                 */
/* -> PB0(I/O)   --> SCE(Active "0")                */
/* -> PB1(I/O)   --> RES(Active "0")                */
/* -> PB2(I/O)   --> D/C(1=Data,0=Command)          */
/* -> PB3(I/O)   --> SDIN                           */
/* -> PB4(I/O)   --> SCLK                           */
/* -> PB5(I/O)   --> LED(Active "1")                */
/****************************************************/

/* Include  Section */
#include <lcd-control.h>
	#ifndef __SLEEP_DEFINED__
	#define __SLEEP_DEFINED__
	.EQU __se_bit=0x80
	.EQU __sm_mask=0x70
	.EQU __sm_powerdown=0x20
	.EQU __sm_powersave=0x30
	.EQU __sm_standby=0x60
	.EQU __sm_ext_standby=0x70
	.EQU __sm_adc_noise_red=0x10
	.SET power_ctrl_reg=mcucr
	#endif


/************************************/
/* Font Code Size 5:Byte/Font Table */
/************************************/
flash unsigned char tab_font[ ] =
{
 0x00, 0x00, 0x00, 0x00, 0x00,   // sp
 0x00, 0x00, 0x2f, 0x00, 0x00,   // !
 0x00, 0x07, 0x00, 0x07, 0x00,   // "
 0x14, 0x7f, 0x14, 0x7f, 0x14,   // #
 0x24, 0x2a, 0x7f, 0x2a, 0x12,   // $
 0x62, 0x64, 0x08, 0x13, 0x23,   // %
 0x36, 0x49, 0x55, 0x22, 0x50,   // &
 0x00, 0x05, 0x03, 0x00, 0x00,   // �
 0x00, 0x1c, 0x22, 0x41, 0x00,   // (
 0x00, 0x41, 0x22, 0x1c, 0x00,   // )
 0x14, 0x08, 0x3E, 0x08, 0x14,   // *
 0x08, 0x08, 0x3E, 0x08, 0x08,   // +
 0x00, 0x00, 0xA0, 0x60, 0x00,   // ,
 0x08, 0x08, 0x08, 0x08, 0x08,   // -
 0x00, 0x60, 0x60, 0x00, 0x00,   // .
 0x20, 0x10, 0x08, 0x04, 0x02,   // /
 0x3E, 0x51, 0x49, 0x45, 0x3E,   // 0
 0x00, 0x42, 0x7F, 0x40, 0x00,   // 1
 0x42, 0x61, 0x51, 0x49, 0x46,   // 2
 0x21, 0x41, 0x45, 0x4B, 0x31,   // 3
 0x18, 0x14, 0x12, 0x7F, 0x10,   // 4
 0x27, 0x45, 0x45, 0x45, 0x39,   // 5
 0x3C, 0x4A, 0x49, 0x49, 0x30,   // 6
 0x01, 0x71, 0x09, 0x05, 0x03,   // 7
 0x36, 0x49, 0x49, 0x49, 0x36,   // 8
 0x06, 0x49, 0x49, 0x29, 0x1E,   // 9
 0x00, 0x36, 0x36, 0x00, 0x00,   // :
 0x00, 0x56, 0x36, 0x00, 0x00,   // ;
 0x08, 0x14, 0x22, 0x41, 0x00,   // <
 0x14, 0x14, 0x14, 0x14, 0x14,   // =
 0x00, 0x41, 0x22, 0x14, 0x08,   // >
 0x02, 0x01, 0x51, 0x09, 0x06,   // ?
 0x32, 0x49, 0x59, 0x51, 0x3E,   // @
 0x7C, 0x12, 0x11, 0x12, 0x7C,   // A
 0x7F, 0x49, 0x49, 0x49, 0x36,   // B
 0x3E, 0x41, 0x41, 0x41, 0x22,   // C
 0x7F, 0x41, 0x41, 0x22, 0x1C,   // D
 0x7F, 0x49, 0x49, 0x49, 0x41,   // E
 0x7F, 0x09, 0x09, 0x09, 0x01,   // F
 0x3E, 0x41, 0x49, 0x49, 0x7A,   // G
 0x7F, 0x08, 0x08, 0x08, 0x7F,   // H
 0x00, 0x41, 0x7F, 0x41, 0x00,   // I
 0x20, 0x40, 0x41, 0x3F, 0x01,   // J
 0x7F, 0x08, 0x14, 0x22, 0x41,   // K
 0x7F, 0x40, 0x40, 0x40, 0x40,   // L
 0x7F, 0x02, 0x0C, 0x02, 0x7F,   // M
 0x7F, 0x04, 0x08, 0x10, 0x7F,   // N
 0x3E, 0x41, 0x41, 0x41, 0x3E,   // O
 0x7F, 0x09, 0x09, 0x09, 0x06,   // P
 0x3E, 0x41, 0x51, 0x21, 0x5E,   // Q
 0x7F, 0x09, 0x19, 0x29, 0x46,   // R
 0x46, 0x49, 0x49, 0x49, 0x31,   // S
 0x01, 0x01, 0x7F, 0x01, 0x01,   // T
 0x3F, 0x40, 0x40, 0x40, 0x3F,   // U
 0x1F, 0x20, 0x40, 0x20, 0x1F,   // V
 0x3F, 0x40, 0x38, 0x40, 0x3F,   // W
 0x63, 0x14, 0x08, 0x14, 0x63,   // X
 0x07, 0x08, 0x70, 0x08, 0x07,   // Y
 0x61, 0x51, 0x49, 0x45, 0x43,   // Z
 0x00, 0x7F, 0x41, 0x41, 0x00,   // [
 0x55, 0x2A, 0x55, 0x2A, 0x55,   // 55
 0x00, 0x41, 0x41, 0x7F, 0x00,   // ]
 0x04, 0x02, 0x01, 0x02, 0x04,   // ^
 0x40, 0x40, 0x40, 0x40, 0x40,   // _
 0x00, 0x01, 0x02, 0x04, 0x00,   // �
 0x20, 0x54, 0x54, 0x54, 0x78,   // a
 0x7F, 0x48, 0x44, 0x44, 0x38,   // b
 0x38, 0x44, 0x44, 0x44, 0x20,   // c
 0x38, 0x44, 0x44, 0x48, 0x7F,   // d
 0x38, 0x54, 0x54, 0x54, 0x18,   // e
 0x08, 0x7E, 0x09, 0x01, 0x02,   // f
 0x18, 0xA4, 0xA4, 0xA4, 0x7C,   // g
 0x7F, 0x08, 0x04, 0x04, 0x78,   // h
 0x00, 0x44, 0x7D, 0x40, 0x00,   // i
 0x40, 0x80, 0x84, 0x7D, 0x00,   // j
 0x7F, 0x10, 0x28, 0x44, 0x00,   // k
 0x00, 0x41, 0x7F, 0x40, 0x00,   // l
 0x7C, 0x04, 0x18, 0x04, 0x78,   // m
 0x7C, 0x08, 0x04, 0x04, 0x78,   // n
 0x38, 0x44, 0x44, 0x44, 0x38,   // o
 0xFC, 0x24, 0x24, 0x24, 0x18,   // p
 0x18, 0x24, 0x24, 0x18, 0xFC,   // q
 0x7C, 0x08, 0x04, 0x04, 0x08,   // r
 0x48, 0x54, 0x54, 0x54, 0x20,   // s
 0x04, 0x3F, 0x44, 0x40, 0x20,   // t
 0x3C, 0x40, 0x40, 0x20, 0x7C,   // u
 0x1C, 0x20, 0x40, 0x20, 0x1C,   // v
 0x3C, 0x40, 0x30, 0x40, 0x3C,   // w
 0x44, 0x28, 0x10, 0x28, 0x44,   // x
 0x1C, 0xA0, 0xA0, 0xA0, 0x7C,   // y
 0x44, 0x64, 0x54, 0x4C, 0x44,   // z
 0x00, 0x08, 0x36, 0x41, 0x00,   // {
 0x00, 0x00, 0x7F, 0x00, 0x00,   // |
 0x00, 0x41, 0x36, 0x08, 0x00,   // }
 0x08, 0x10, 0x08, 0x04, 0x08,   // ~

 // Thai-Font by BenNueng
0x00,0x00,0x00,0x00,0x00, //127/ --> 
0x00,0x00,0x00,0x00,0x00, //128/ --> �
0x00,0x00,0x00,0x00,0x00, //129/ --> �
0x00,0x00,0x00,0x00,0x00, //130/ --> �
0x00,0x00,0x00,0x00,0x00, //131/ --> �
0x00,0x00,0x00,0x00,0x00, //132/ --> �
0x00,0x00,0x00,0x00,0x00, //133/ --> �
0x00,0x00,0x00,0x00,0x00, //134/ --> �
0x00,0x00,0x00,0x00,0x00, //135/ --> �
0x00,0x00,0x00,0x00,0x00, //136/ --> �
0x00,0x00,0x00,0x00,0x00, //137/ --> �
0x00,0x00,0x00,0x00,0x00, //138/ --> �
0x00,0x00,0x00,0x00,0x00, //139/ --> �
0x00,0x00,0x00,0x00,0x00, //140/ --> �
0x00,0x00,0x00,0x00,0x00, //141/ --> �
0x00,0x00,0x00,0x00,0x00, //142/ --> �
0x00,0x00,0x00,0x00,0x00, //143/ --> �
0x00,0x00,0x00,0x00,0x00, //144/ --> �
0x00,0x00,0x00,0x00,0x00, //145/ --> �
0x00,0x00,0x00,0x00,0x00, //146/ --> �
0x00,0x00,0x00,0x00,0x00, //147/ --> �
0x00,0x00,0x00,0x00,0x00, //148/ --> �
0x00,0x00,0x00,0x00,0x00, //149/ --> �
0x00,0x00,0x00,0x00,0x00, //150/ --> �
0x00,0x00,0x00,0x00,0x00, //151/ --> �
0x00,0x00,0x00,0x00,0x00, //152/ --> �
0x00,0x00,0x00,0x00,0x00, //153/ --> �
0x00,0x00,0x00,0x00,0x00, //154/ --> �
0x00,0x00,0x00,0x00,0x00, //155/ --> �
0x00,0x00,0x00,0x00,0x00, //156/ --> �
0x00,0x00,0x00,0x00,0x00, //157/ --> �
0x00,0x00,0x00,0x00,0x00, //158/ --> �
0x00,0x00,0x00,0x00,0x00, //159/ --> �
0x00,0x00,0x00,0x00,0x00, //160/ --> �
0x04,0x7A,0x02,0x02,0x7C, //161/ --> �
0x44,0x66,0x5C,0x40,0x3E, //162/ --> �
0x46,0x64,0x5A,0x40,0x3E, //163/ --> �
0x7C,0x0A,0x0A,0x02,0x7C, //164/ --> �
0x7C,0x12,0x34,0x02,0x7C, //165/ --> �
0x66,0x7C,0x26,0x40,0x3E, //166/ --> �
0x08,0x10,0x20,0x42,0x3E, //167/ --> �
0x04,0x12,0x12,0x22,0x7C, //168/ --> �
0x0A,0x7A,0x22,0x3C,0x60, //169/ --> �
0x44,0x7E,0x44,0x44,0x3B, //170/ --> �
0x46,0x7C,0x46,0x44,0x3B, //171/ --> �
0x7C,0x02,0x62,0x7C,0x3E, //172/ --> �
0x7C,0x02,0xB2,0xBC,0xBE, //173/ --> �
0x24,0xBA,0x82,0xC2,0x7C, //174/ --> �
0x24,0xBA,0x42,0x82,0x7C, //175/ --> �
0x04,0x96,0x56,0xA6,0x7A, //176/ --> �
0x06,0x7C,0x06,0x04,0x78, //177/ --> �
0x7E,0x0C,0x6E,0x70,0x3E, //178/ --> �
0x7C,0x02,0x7E,0x70,0x2E, //179/ --> �
0x7C,0x2A,0x1A,0x02,0x7C, //180/ --> �
0x7C,0x52,0x34,0x02,0x7C, //181/ --> �
0x04,0x7A,0x42,0x02,0x7C, //182/ --> �
0x02,0x7E,0x04,0x02,0x7E, //183/ --> �
0x74,0x46,0x46,0x46,0x3A, //184/ --> �
0x02,0x7E,0x20,0x7E,0x20, //185/ --> �
0x42,0x7E,0x40,0x40,0x3E, //186/ --> �
0x44,0x7C,0x40,0x40,0x3F, //187/ --> �
0x7E,0x22,0x10,0x20,0x7E, //188/ --> �
0x7C,0x24,0x10,0x20,0x7F, //189/ --> �
0x7A,0x26,0x10,0x20,0x7E, //190/ --> �
0x74,0x2C,0x10,0x20,0x7F, //191/ --> �
0x44,0x7A,0x02,0x02,0x7C, //192/ --> �
0x22,0x7E,0x20,0x20,0x7E, //193/ --> �
0x76,0x4A,0x40,0x40,0x3E, //194/ --> �
0x04,0x06,0x46,0x46,0x3A, //195/ --> �
0x04,0x3A,0x22,0x02,0xFC, //196/ --> �
0x64,0x52,0x12,0x12,0x7C, //197/ --> �
0x24,0x3A,0x02,0x02,0xFC, //198/ --> �
0x04,0x02,0x42,0x42,0x3C, //199/ --> �
0x7C,0x12,0x32,0x06,0x7D, //200/ --> �
0x42,0x7E,0x48,0x3E,0x08, //201/ --> �
0x64,0x52,0x12,0x12,0x7D, //202/ --> �
0x02,0x7E,0x08,0x0E,0x76, //203/ --> �
0x74,0x2C,0x10,0x22,0x7D, //204/ --> �
0x7A,0x4A,0x42,0x42,0x3C, //205/ --> �
0x74,0x56,0x46,0x44,0x3B, //206/ --> �
0x06,0x0A,0x48,0x48,0x3E, //207/ --> �
0x6C,0x48,0x48,0x48,0x24, //208/ --> �
0x06,0x04,0x04,0x04,0x02, //209/ --> �
0x04,0x02,0x02,0x02,0x7C, //210/ --> �
0x03,0x07,0x02,0x02,0x7C, //211/ --> �
0x04,0x06,0x06,0x06,0x04, //212/ --> �
0x04,0x04,0x04,0x04,0x06, //213/ --> �
0x04,0x04,0x04,0x06,0x06, //214/ --> �
0x04,0x04,0x06,0x04,0x06, //215/ --> �
0x00,0x00,0x00,0x40,0xC0, //216/ --> �
0x00,0x40,0xC0,0x80,0xC0, //217/ --> �
0x00,0x00,0x00,0xC0,0xC0, //218/ --> �
0x00,0x00,0x00,0x00,0x00, //219/ --> �
0x00,0x00,0x00,0x00,0x00, //220/ --> �
0x00,0x00,0x00,0x00,0x00, //221/ --> �
0x00,0x00,0x00,0x00,0x00, //222/ --> �
0x7E,0xCB,0x4A,0x4E,0x38, //223/ --> �
0x00,0x7E,0x40,0x00,0x00, //224/ --> �
0x7E,0x40,0x00,0x7E,0x40, //225/ --> �
0x02,0x03,0x03,0x3D,0x41, //226/ --> �
0x02,0x05,0x05,0x3E,0x40, //227/ --> �
0x02,0x03,0x06,0x3F,0x40, //228/ --> �
0x04,0x02,0x02,0x02,0xFC, //229/ --> �
0x0C,0x12,0x04,0x02,0xFC, //230/ --> �
0x02,0x05,0x03,0x05,0x00, //231/ --> �
0x00,0x00,0x03,0x00,0x00, //232/ --> �
0x01,0x07,0x04,0x02,0x01, //233/ --> �    // old = 0x05,0x07,0x04,0x02,0x01,
0x02,0x01,0x01,0x02,0x03, //234/ --> �
0x02,0x07,0x02,0x00,0x00, //235/ --> �
0x03,0x03,0x01,0x01,0x01, //236/ --> �
0x00,0x02,0x05,0x02,0x00, //237/ --> �
0x00,0x02,0x05,0x05,0x00, //238/ --> �
0x38,0x44,0x54,0x44,0x38, //239/ --> �
0x38,0x44,0x44,0x44,0x38, //240/ --> �
0x38,0x44,0x64,0x04,0x78, //241/ --> �
0x3E,0x40,0x4C,0x44,0x38, //242/ --> �
0x38,0x44,0x58,0x04,0x78, //243/ --> �
0x38,0x44,0x44,0x64,0x43, //244/ --> �
0x38,0x46,0x46,0x64,0x43, //245/ --> �
0x03,0x24,0x44,0x44,0x38, //246/ --> �
0x3C,0x48,0x0C,0x78,0x3E, //247/ --> �
0x38,0x44,0x24,0x48,0x26, //248/ --> �
0x30,0x48,0x58,0x14,0x66, //249/ --> �
0x0C,0x08,0x7C,0x00,0x7C, //250/ --> �
0x38,0x44,0x30,0x10,0x10, //251/ --> �
0x00,0x00,0x00,0x00,0x00, //252/ --> �
0x3C,0x42,0x4A,0x42,0x3C, //253/ --> ����ҷԵ��
0x3C,0x7E,0xE7,0xC3,0x42, //254/ --> ��Шѹ���
0x00,0x00,0x00,0x00,0x00 //255/ --> �
};

/*****************************/
/* Graphic 84x48 Pixed Table */
/*****************************/


flash unsigned char tab_picture[ ] =
{
 0x00,0x00,0x00,0x00,0x00,
 0x00,0x00,0x00,0x00,0x00,
 0x00,0x00,0x00,0x00,0x00,
 0x00,0x00,0x00,0xC0,0xE0,
 0xE0,0x70,0x30,0xB0,0xB0,
 0xB0,0xB0,0xB0,0xB0,0xB0,
 0xB0,0xB0,0xA0,0x00,0x00,
 0x00,0xA0,0xB0,0xB0,0xB0,
 0xB0,0xB0,0xB0,0x30,0xB0,
 0xB0,0xB0,0xB0,0xB0,0xB0,
 0xB0,0xA0,0x00,0x00,0xA0,
 0xB0,0xB0,0xB0,0xB0,0xB0,
 0xB0,0x30,0xB0,0xB0,0xB0,
 0xB0,0xB0,0xB0,0xB0,0xA0,
 0x00,0x00,0x00,0x00,0x00,
 0x00,0x00,0x00,0x00,0x00,
 0x00,0x00,0x00,0x00,0x00,
 0x00,0x00,0x00,0x00,0x00,
 0x00,0x00,0x00,0x00,0x00,
 0x00,0x00,0x00,0x00,0x00,
 0x00,0xFF,0xFF,0x01,0xFC,
 0xFE,0x03,0x01,0x01,0x01,
 0x01,0x01,0x01,0x01,0x01,
 0x01,0x00,0x00,0x00,0x00,
 0x00,0x01,0x01,0x01,0x01,
 0xFF,0xFF,0x00,0xFF,0xFF,
 0x01,0x01,0x01,0x01,0x01,
 0x00,0x00,0x00,0x00,0x01,
 0x01,0x01,0x01,0xFF,0xFF,
 0x00,0xFF,0xFF,0x01,0x01,
 0x01,0x01,0x01,0x00,0x00,
 0x00,0x00,0x00,0x00,0x00,
 0x00,0x00,0x00,0x00,0x00,
 0x00,0x00,0x00,0x00,0x00,
 0x00,0x00,0x00,0x00,0x00,
 0x00,0x00,0x00,0x00,0x00,
 0x00,0x00,0x00,0x00,0x00,
 0xFF,0xFF,0x00,0xFD,0xFD,
 0x0D,0x0D,0x0D,0x0D,0x0D,
 0x0D,0x0D,0x0D,0x04,0x00,
 0x00,0x00,0x00,0x00,0x00,
 0x00,0x00,0x00,0x00,0xFF,
 0xFF,0x00,0xFF,0xFF,0x00,
 0x00,0x00,0x00,0x00,0x00,
 0x00,0x00,0x00,0x00,0x00,
 0x00,0x00,0xFF,0xFF,0x00,
 0xFF,0xFF,0x00,0x00,0x00,
 0x00,0x00,0x00,0x00,0x00,
 0x00,0x00,0x00,0x00,0x00,
 0x00,0x00,0x00,0x00,0x00,
 0x00,0x00,0x00,0x00,0x00,
 0x00,0x00,0x00,0x00,0x00,
 0x00,0x00,0x00,0x00,0x00,
 0x00,0x00,0x00,0x00,0x07,
 0x0F,0x1C,0x39,0x73,0x67,
 0x6E,0x6C,0x6C,0x6C,0x6C,
 0x6C,0x6C,0x6C,0x6C,0x28,
 0x00,0x00,0x00,0x00,0x00,
 0x00,0x00,0x00,0x3F,0x7F,
 0x00,0x7F,0x3F,0x00,0x00,
 0x00,0x00,0x00,0x00,0x00,
 0x00,0x00,0x00,0x00,0x00,
 0x00,0x3F,0x7F,0x00,0x7F,
 0x3F,0x00,0x00,0x00,0x00,
 0x00,0x00,0x00,0x00,0x00,
 0x00,0x00,0x00,0x00,0x00,
 0x00,0x00,0x00,0x00,0x00,
 0x00,0x00,0x00,0x00,0x00,
 0x80,0x00,0x00,0x00,0x80,
 0x00,0x80,0x00,0x00,0x00,
 0x80,0x00,0x80,0x00,0x00,
 0x00,0x80,0x00,0x00,0x00,
 0x00,0x80,0x80,0x80,0x80,
 0x80,0x00,0x80,0x80,0x80,
 0x80,0x80,0x00,0x80,0x80,
 0x80,0x80,0x80,0x00,0x80,
 0x80,0x80,0x80,0x80,0x00,
 0x00,0x80,0x80,0x80,0x00,
 0x00,0x80,0x00,0x00,0x00,
 0x80,0x00,0x00,0x00,0x00,
 0x00,0x80,0x80,0x80,0x00,
 0x00,0x00,0x80,0x80,0x80,
 0x00,0x00,0x80,0x00,0x00,
 0x00,0x80,0x00,0x00,0x00,
 0x00,0x00,0x00,0x00,0x3F,
 0x10,0x08,0x10,0x3F,0x00,
 0x3F,0x10,0x08,0x10,0x3F,
 0x00,0x3F,0x10,0x08,0x10,
 0x3F,0x00,0x30,0x30,0x00,
 0x3F,0x24,0x24,0x24,0x20,
 0x00,0x00,0x00,0x3F,0x00,
 0x00,0x00,0x00,0x00,0x3F,
 0x00,0x00,0x00,0x3F,0x24,
 0x24,0x24,0x20,0x00,0x3F,
 0x08,0x08,0x08,0x3F,0x00,
 0x3F,0x01,0x02,0x01,0x3F,
 0x00,0x30,0x30,0x00,0x1F,
 0x20,0x20,0x20,0x11,0x00,
 0x1F,0x20,0x20,0x20,0x1F,
 0x00,0x3F,0x01,0x02,0x01,
 0x3F,0x00,0x00,0x00
};


////////////////////// COMMAND ////////////////////////////////////////////////

/********************************/
/* Write Data or Command to LCD */
/* D/C = "0" = Write Command    */
/* D/C = "1" = Write Display    */
/********************************/
void lcd_write_data(unsigned char DataByte)
{
    // Bit Counter
    unsigned char Bit = 0;

    // Active DC = High("1"=Data)
    LCD5110_DC_HIGH();

    for (Bit = 0; Bit < 8; Bit++)		                        // 8 Bit Write
    {
        // MSB First of Data Bit(7..0)
        if ((DataByte & 0x80) == 0x80)
            // SPI Data = "1"
            LCD5110_SDIN_HIGH();
        else
            // SPI Data = "0"
            LCD5110_SDIN_LOW();

        // Strobe Bit Data
        LCD5110_SCLK_HIGH();
        // Delay Clock
        SPI_Delay();

        // Next Clock
        LCD5110_SCLK_LOW();
        // Next Bit Data
        DataByte <<= 1;
    }
}

/********************************/
/* Write Data or Command to LCD */
/* D/C = "0" = Write Command    */
/* D/C = "1" = Write Display    */
/********************************/
void lcd_write_command(unsigned char CommandByte)
{
    // Bit Counter
    unsigned char Bit = 0;

    // Active DC = Low("0"=Command)
    LCD5110_DC_LOW();

    // 8 Bit Write
    for (Bit = 0; Bit < 8; Bit++)
    {
        // MSB First of Data Bit(7..0)
        if ((CommandByte & 0x80) == 0x80)
            // SPI Data = "1"
            LCD5110_SDIN_HIGH();
        else
            // SPI Data = "0"
            LCD5110_SDIN_LOW();

        // Strobe Bit Data
        LCD5110_SCLK_HIGH();
        // Delay Clock
        SPI_Delay();

        // Next Clock
        LCD5110_SCLK_LOW();
        // Next Bit Data
        CommandByte <<= 1;
    }
}

/**************************/
/* Delay SPI Clock Signal */
/**************************/
void SPI_Delay(void)
{
    // Short Delay Counter
    int x;
    x++;
#ifndef CPU_F
    #define CPU_F   16000000UL
#endif
#if CPU_F > 8000000UL
    x++;
#endif
}

/**************************/
/* Initial LCD Nokia-5110 */
/**************************/
void lcd_initial(void)
{
    // code ����ҡ main.c , 㹷������ port B
    // ����¹�ҡ bit 6 ����� input ����� output ����
    DDRB = 0b01111111;                  // PB[7] = Input,PB[6..0] = Output
    LCD5110_ON();                       // ����Դ˹�Ҩ� LCD ���� B.6
    // Initial GPIO Signal Interface LCD Nokia-5110
    LCD5110_RES_LOW();			        // Active Reset
    LCD5110_RES_HIGH();					// Normal Operation
    LCD5110_DC_HIGH(); 					// D/C = High("1"=Data)
    LCD5110_LED_HIGH();					// LED = High(ON LED)
    LCD5110_SDIN_LOW();                 // Standby SPI Data
    LCD5110_SCLK_LOW();                 // Standby SPI Clock
    LCD5110_SCE_LOW();					// SCE = Low(Enable)

    // start initial
    LCD5110_RES_LOW();          // Active Reset
    LCD5110_RES_HIGH();         // Normal Operation

    lcd_write_command(32+1);    // Function Set = Extend Instruction(00100+PD,V,H=00100+0,0,1)
    lcd_write_command(128+38);  // Set VOP(1+VOP[6..0] = 1+0100110)
    lcd_write_command(4+3);     // Temp Control(000001+TC1,TC0=000001+1,1)
    lcd_write_command(16+3);    // Bias System(00010,BS2,BS1,BS0=00010,0,1,1)

    lcd_write_command(32+0);    // Function Set = Basic Instruction(00100+PD,V,H = 00100+0,0,0)
    lcd_write_command(12);      // Display Control = Normal Mode(00001D0E=00001100)
}

/****************************/
/* Clear Screen Display LCD */
/****************************/
void lcd_clear_screen(void)
{
    unsigned int i = 0;        // Memory Display(Byte) Counter

    lcd_write_command(128+0);   // Set X Position = 0(0..83)
    lcd_write_command(64+0);    // Set Y Position = 0(0..5)

    // 84 * 6 = 504
    for(i=0;i<504;i++)          // All Display RAM = 504 Byte
        lcd_write_data(0);      // Clear Screen Display
}

/****************************/
/* Fill Picture Display LCD */
/****************************/
void lcd_fill_picture(flash unsigned char *tab_picture)
{
    unsigned int i = 0;        // Memory Display(Byte) Counter

    lcd_write_command(128+0);   // Set X Position = 0(0..83)
    lcd_write_command(64+0);    // Set Y Position = 0(0..5)

    // 84 * 6 = 504
    for(i=0;i<504;i++)          // All Display RAM = 504 Byte
        lcd_write_data(tab_picture[i]); // Fill Picture Display
}

/***************************/
/* Set Cursor X,Y Position */
/* X[0-83]: 84 Column Data */
/* Y[0-5] : 6 Row(48 Dot)  */
/***************************/
void lcd_gotoxy(unsigned char x,unsigned char y)
{
    lcd_write_command(128+x);  				// Set X Position(1+x6,x5,x4,x3,x2,x1,x0)
    lcd_write_command(64+y);   				// Set Y Position(01000+y2,y1,y0)
}

/***************************/
/* Put Char to LCD Display */
/***************************/
void lcd_put_char(unsigned char character)
{
    // Font Size Counter
    unsigned char font_size_count = 0;
    // Font Data Pointer
    unsigned int  font_data_index;

    // Skip 0x00..0x1F Font Code
    font_data_index = character-32;
    // 5 Byte / Font
    font_data_index = font_data_index*5;

    // Get 5 Byte Font & Display on LCD
    while(font_size_count<5)
    {
         // Get Data of Font From Table & Write LCD
        lcd_write_data(tab_font[font_data_index]);
        font_size_count++;  	// Next Byte Counter
        font_data_index++;  	// Next	Byte Pointer
    }
    // 1 Pixel Dot Space
    lcd_write_data(0);
}

/*************************************/
/* Print SRAM String to LCD Display */
/*************************************/
void lcd_print_string(unsigned char *string , unsigned char CharCount)
{
    // Dummy Character Count
    unsigned char i=0;

    while(i<CharCount)
    {
        lcd_put_char(string[i]);	// Print 1-Char to LCD
        i++;                        // Next Character Print
    }
}

/*************************************/
/* Print Flash String to LCD Display */
/*************************************/
void lcd_print_flash_string(flash unsigned char *string , unsigned char CharCount)
{
    // Dummy Character Count
    unsigned char i=0;

    while(i<CharCount)
    {
        lcd_put_char(string[i]);	// Print 1-Char to LCD
        i++;                        // Next Character Print
    }
}

/*******************************************/
/* Long Delay Time Function(1..4294967295) */
/*******************************************/
void delay(unsigned long i)
{
    while(i > 0) {i--;}	                // Loop Decrease Counter
    return;
}

/**************************************************************************
/* define section */
/**************************************************************************/
// define part
#define LCD_BUG_FIXED         true         // ��䢻ѭ������ͧ����͡Ẻ�Ңͧ LCD �Դ

#define MCU         ATMEGA32
//#define CPU_F       11059200UL
#define CPU_F       4000000UL
// COUNT_INTERRUPT_UPON_XTAL ���繵�ǹѺ interrupt ��Ҥú�ͺ 1 �Թҷ������ѧ
// xtal 11.0592 ��Ǩ m_tick_count >= 43  (����ٵù��)
// ����Ҩҡ�ٵ� 1,000,000 /  (f/prescale)
//  = 1000000 / (11059200/1024)
//  = 1000000 / 10800
//  = 92 us ���¶֧ 1 clock ������ 92 us ��ҵ�ͧ�Ѻ 256 ���� �֧���Դ interrupt
//  = 92 * 256 = 23,703 us ���Դ 1 interrupt
//  ����¹˹����� ms �� 23703 / 1000 ms = 23.703 ms ���¶֧�ء� int �������� 23.703 ms
//  = 1 �Թҷ� �� 1000ms �ѧ��鹵�ͧ�Ѻ 1000ms / 23 = 43.xxx

// ���äԴ����� ��������� Hz �� / 256 �������� xx ms��͡�� interrupt 1 ���� : 1 ��
// xtal 11.0592 ��Ǩ m_tick_count >= 42  (10800 / 256 = 42.xx)
// xtal 4Mhz ��Ǩ m_tick_count >= 61  (15625 / 256 = 61.xx)
#define COUNT_INTERRUPT_UPON_XTAL   61

#define true        1
#define false       0
#define DEBUG_MODE  false

#define RTC_CHIP        0
#define ALL_MENU        6
#define _LIGHT_DELAY    45
#define _POWER_DELAY    60


/**************************************************************************
/* include section */
/**************************************************************************/
#include <mega32.h>
	#ifndef __SLEEP_DEFINED__
	#define __SLEEP_DEFINED__
	.EQU __se_bit=0x80
	.EQU __sm_mask=0x70
	.EQU __sm_powerdown=0x20
	.EQU __sm_powersave=0x30
	.EQU __sm_standby=0x60
	.EQU __sm_ext_standby=0x70
	.EQU __sm_adc_noise_red=0x10
	.SET power_ctrl_reg=mcucr
	#endif
//#include <lcd-control.h>
#include <lcd-5110-thai.h>  // include Thai ��������ͧ include lcd-control.h
#include <delay.h> // ��͹˹�ǧ����
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <sleep.h>  // powerdown
/* the I2C bus is connected to PORTC */
/* the SDA signal is bit 1 */
/* the SCL signal is bit 0 */
#asm
    .equ __i2c_port=0x15
    .equ __sda_bit=1
    .equ __scl_bit=0
#endasm
#include <i2c.h>
#include <pcf8583.h>

// ���¡�� keypad
#include <matrixkeypad4x3.h>

// �ӹǹ��ԷԹ��
#include <lunar_process.h>

// ���¡���äӹǹ �Ţ 7 ���
#include <seven_number.h>

// ���¡��ҹ����ʴ��� GUI Ẻ GRAPHICS
#include <gui.h>

// ���¡��ҹ debug utility (���¡����ѧ�ش)
#ifdef DEBUG_MODE
    #include <debug_utility.h>
#endif

/**************************************************************************
// �ٻ��ҧ�
/**************************************************************************/
// �ٻ��ǡ���
flash unsigned char pic_coin[] =
{
0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff
, 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0x8f , 0x8f
, 0x8f , 0xbf , 0xbf , 0xbf , 0xbf , 0xbf , 0xbf , 0xbf
, 0xff , 0xff , 0x7f , 0x7f , 0x7f , 0x7f , 0x7f , 0x7f
, 0x7f , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff
, 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff
, 0xff , 0xbf , 0xbf , 0x7f , 0x7f , 0xdf , 0xdf , 0xbf
, 0x7f , 0xdf , 0x3f , 0xff , 0xff , 0xff , 0xff , 0xff
, 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0x7f , 0x7f
, 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff
, 0xff , 0xff , 0xff , 0xff
, 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xf0
, 0xf4 , 0xf4 , 0xf4 , 0x04 , 0x00 , 0xf7 , 0x77 , 0x3f
, 0x9f , 0xcf , 0xe7 , 0x00 , 0x04 , 0xf4 , 0xf4 , 0xf0
, 0xff , 0xff , 0x7e , 0x7e , 0x7e , 0x7e , 0x7e , 0x00
, 0x00 , 0x7f , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff
, 0xff , 0xff , 0xff , 0xff , 0xff , 0x7f , 0x9f , 0xef
, 0xf7 , 0xfb , 0xfb , 0xfd , 0xfd , 0xfc , 0xfc , 0x7c
, 0x7c , 0x3e , 0x9c , 0xcd , 0xcd , 0xc3 , 0xe3 , 0x67
, 0x27 , 0x87 , 0xc7 , 0xeb , 0xcd , 0xce , 0xd6 , 0x9b
, 0x3b , 0x7d , 0xfd , 0xff , 0xdf , 0xbf , 0x6f , 0xdf
, 0x3f , 0xff , 0xff , 0xff
, 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff
, 0xff , 0xff , 0xff , 0xb8 , 0x38 , 0x3c , 0xfe , 0xff
, 0xff , 0x7f , 0x3f , 0xf0 , 0xf0 , 0xff , 0xff , 0xff
, 0xff , 0xff , 0xfc , 0xfd , 0xfd , 0xfd , 0xfd , 0xfc
, 0xfc , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff
, 0xff , 0xff , 0xff , 0x81 , 0x81 , 0x7e , 0xff , 0xff
, 0xff , 0xff , 0xff , 0x3f , 0x1f , 0xc1 , 0xf8 , 0xfc
, 0xfe , 0xff , 0xff , 0x8f , 0xf7 , 0xf7 , 0x8f , 0xff
, 0xff , 0xff , 0x8f , 0xf7 , 0xf7 , 0x8f , 0xff , 0xff
, 0xff , 0xfe , 0xfc , 0xf1 , 0xc7 , 0x1f , 0x3e , 0xff
, 0xff , 0xff , 0xff , 0xff
, 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xf7
, 0x07 , 0x03 , 0xf2 , 0xf2 , 0xf2 , 0xf2 , 0xf2 , 0x02
, 0x06 , 0xfe , 0xff , 0xcf , 0x4f , 0x4f , 0x4f , 0x4f
, 0xcf , 0x0f , 0x1f , 0xff , 0xf7 , 0x01 , 0x01 , 0x05
, 0xf5 , 0xf1 , 0xf1 , 0xff , 0xff , 0xff , 0x01 , 0xff
, 0xff , 0xff , 0xff , 0xff , 0xff , 0xfe , 0xf9 , 0x77
, 0xef , 0xdf , 0xdf , 0xbc , 0xb8 , 0xc3 , 0x9f , 0x7f
, 0x7f , 0xff , 0xff , 0xe3 , 0xdf , 0xbf , 0xbf , 0xbf
, 0xbf , 0xbf , 0xbf , 0xbf , 0xdf , 0xe3 , 0xff , 0xff
, 0xff , 0x7f , 0x7f , 0x9f , 0xe3 , 0xf8 , 0xfc , 0xff
, 0xff , 0xff , 0xff , 0xff
, 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff
, 0xc0 , 0xc0 , 0xff , 0xff , 0xff , 0xff , 0xff , 0xc0
, 0xc0 , 0xff , 0xff , 0xfd , 0x80 , 0xbd , 0xbd , 0xbc
, 0xbf , 0x80 , 0x80 , 0xff , 0xff , 0xff , 0xc0 , 0xc0
, 0xce , 0xce , 0xce , 0xcf , 0xcf , 0xcf , 0xe0 , 0xff
, 0xff , 0xff , 0xff , 0xff , 0xff , 0xfc , 0xfb , 0xf6
, 0xfd , 0xfb , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff
, 0xfe , 0xfe , 0xfd , 0xfb , 0xfb , 0xfb , 0xf7 , 0xf7
, 0xf7 , 0xf7 , 0xf7 , 0xf7 , 0xfb , 0xfb , 0xfb , 0xfd
, 0xfe , 0xfe , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff
, 0xff , 0xff , 0xff , 0xff
, 0xff , 0xef , 0xc7 , 0x83 , 0xff , 0xff , 0xff , 0xff
, 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff
, 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff
, 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff
, 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff
, 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff
, 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff
, 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff
, 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff
, 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff
, 0x83 , 0xc7 , 0xef , 0xff
};

// �ٻ ���� �ԹԨ�ѹ���
flash unsigned char pic_pinij_jan[] =
{
0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x04 , 0x00 , 0x00
, 0x00 , 0x80 , 0xc0 , 0xe0 , 0xe0 , 0x70 , 0xb0 , 0x70
, 0xb0 , 0xf0 , 0xb0 , 0x60 , 0xe0 , 0xc0 , 0x80 , 0x00
, 0x00 , 0x00 , 0x00 , 0x00 , 0x20 , 0x70 , 0x20 , 0x00
, 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00
, 0x00 , 0x00 , 0x00 , 0x00 , 0x04 , 0x00 , 0x00 , 0x00
, 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00
, 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00
, 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00
, 0x02 , 0x02 , 0x00 , 0x04 , 0x06 , 0x00 , 0x04 , 0x06
, 0x00 , 0x02 , 0x04 , 0x00
, 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00
, 0x7e , 0xff , 0xbb , 0xff , 0xaf , 0x57 , 0xea , 0x15
, 0x0b , 0x05 , 0x06 , 0x03 , 0x02 , 0x03 , 0x03 , 0x07
, 0x0c , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00
, 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00
, 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00
, 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00
, 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00
, 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00
, 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00
, 0x00 , 0x00 , 0x00 , 0x00
, 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x80 , 0x00 , 0x00
, 0x00 , 0x01 , 0x02 , 0x05 , 0x0a , 0x0d , 0x1b , 0x16
, 0x18 , 0x10 , 0x10 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00
, 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00
, 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00
, 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00
, 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00
, 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00
, 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00
, 0x00 , 0x00 , 0x00 , 0x00 , 0x80 , 0x80 , 0x00 , 0x00
, 0x00 , 0x00 , 0x00 , 0x00
, 0x00 , 0x00 , 0x00 , 0x00 , 0x01 , 0x03 , 0x01 , 0x00
, 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00
, 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x08 , 0xf8 , 0xf8
, 0x02 , 0xfb , 0xfb , 0x03 , 0xfb , 0xfa , 0x00 , 0x08
, 0xfa , 0xfb , 0x03 , 0x03 , 0xfb , 0xfa , 0x00 , 0x10
, 0x18 , 0x08 , 0x48 , 0x48 , 0xf8 , 0xf0 , 0x00 , 0x10
, 0x18 , 0x0b , 0x4b , 0x4a , 0xfa , 0xf3 , 0x01 , 0x08
, 0xf8 , 0xf8 , 0x00 , 0x00 , 0xf8 , 0xf8 , 0x00 , 0x08
, 0xf8 , 0xf8 , 0x10 , 0x08 , 0xf8 , 0xf0 , 0x00 , 0x00
, 0x70 , 0x78 , 0x4b , 0xdb , 0x91 , 0x00 , 0x00 , 0x00
, 0x00 , 0x00 , 0x00 , 0x00
, 0x00 , 0x00 , 0x00 , 0x40 , 0x00 , 0x00 , 0x00 , 0x00
, 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00
, 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x13 , 0x13
, 0x12 , 0x13 , 0x13 , 0x12 , 0x13 , 0x11 , 0x10 , 0x10
, 0x11 , 0x13 , 0x12 , 0x11 , 0x13 , 0x13 , 0x10 , 0x10
, 0x10 , 0x10 , 0x10 , 0x10 , 0x13 , 0x13 , 0x10 , 0x10
, 0x10 , 0x10 , 0x10 , 0x10 , 0x13 , 0x13 , 0x10 , 0x10
, 0x11 , 0x13 , 0x12 , 0x11 , 0x13 , 0x13 , 0x10 , 0x10
, 0x13 , 0x13 , 0x10 , 0x10 , 0x13 , 0x13 , 0x10 , 0x10
, 0x10 , 0x12 , 0x02 , 0x13 , 0x01 , 0x00 , 0x00 , 0x00
, 0x00 , 0x00 , 0x00 , 0x00
, 0x40 , 0x00 , 0x40 , 0x40 , 0x00 , 0xc0 , 0x00 , 0xc0
, 0xc0 , 0x00 , 0xc0 , 0x40 , 0x00 , 0xc0 , 0x80 , 0x00
, 0x40 , 0xc0 , 0x00 , 0x40 , 0x80 , 0x00 , 0x80 , 0x40
, 0x00 , 0x80 , 0xc0 , 0x00 , 0x40 , 0x00 , 0x40 , 0x40
, 0x00 , 0xc0 , 0x00 , 0xc0 , 0xc0 , 0x00 , 0xc0 , 0x40
, 0x00 , 0xc0 , 0x80 , 0x00 , 0x40 , 0xc0 , 0x00 , 0x40
, 0x80 , 0x00 , 0x80 , 0x40 , 0x00 , 0x80 , 0xc0 , 0x00
, 0x40 , 0x00 , 0x40 , 0x40 , 0x00 , 0xc0 , 0x00 , 0xc0
, 0xc0 , 0x00 , 0xc0 , 0x40 , 0x00 , 0xc0 , 0x80 , 0x00
, 0x40 , 0xc0 , 0x00 , 0x40 , 0x80 , 0x00 , 0x80 , 0x40
, 0x00 , 0x80 , 0xc0 , 0x00

};

// �ٻ��駻�ء
flash unsigned char pic_set_alarm[] =
{
 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff
, 0xff , 0xff , 0xff ,0xff , 0xff , 0x1f , 0x1f , 0x9f
, 0xff , 0xff , 0xf3 , 0xd3 , 0xc3 , 0xdf , 0xdf , 0xdf
, 0xcf , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff
, 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xef
, 0x37 , 0x9b , 0xcb , 0xed , 0xff , 0xff , 0xff , 0xff
, 0xff , 0xff , 0xff , 0x7f , 0x7f , 0x7f , 0x7f , 0x7f
, 0x7f , 0x7f , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff
, 0xff , 0xff , 0xff , 0xff , 0xff , 0x5f , 0x9f , 0x9f
, 0xff , 0xff , 0xcb , 0xd3 , 0xd3 , 0xdf , 0xff , 0xff
, 0xff , 0xff , 0xff , 0xff
, 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff
, 0xff , 0xff , 0xbf , 0xbf , 0xbf , 0x02 , 0x02 , 0x3a
, 0x3a , 0x3a , 0x22 , 0x22 , 0x22 , 0xfa , 0xfa , 0x02
, 0x02 , 0xfe , 0xff , 0xbf , 0x7f , 0xff , 0xff , 0xfb
, 0xfb , 0x03 , 0x03 , 0xff , 0xff , 0xff , 0xff , 0xff
, 0x1f , 0x0f , 0xe7 , 0x73 , 0x39 , 0x19 , 0x19 , 0x89
, 0xd1 , 0xc3 , 0xec , 0xec , 0xe3 , 0xeb , 0x23 , 0x23
, 0xe0 , 0xe1 , 0xec , 0xed , 0xc3 , 0xc1 , 0x09 , 0x19
, 0x39 , 0x79 , 0xf3 , 0x67 , 0x0f , 0xfe , 0xfe , 0xfe
, 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff
, 0xff , 0xff , 0xff , 0xff
, 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff
, 0xff , 0xff , 0xff , 0xef , 0xef , 0xe0 , 0xe0 , 0xef
, 0xee , 0x6e , 0x6e , 0xee , 0xe0 , 0xff , 0xff , 0xf0
, 0xf0 , 0xff , 0xff , 0xff , 0xff , 0xf8 , 0xfb , 0xfb
, 0xfb , 0xf8 , 0xf8 , 0xff , 0xff , 0xff , 0xff , 0xff
, 0xfe , 0xfe , 0x1c , 0xe0 , 0xfc , 0xff , 0xff , 0xff
, 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0x00 , 0x00
, 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xfe
, 0xfc , 0xfc , 0xe0 , 0x0e , 0xff , 0xff , 0xff , 0xff
, 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff
, 0xff , 0xff , 0xff , 0xff
, 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff
, 0xff , 0xff , 0xff , 0x7f , 0x7f , 0x7f , 0xff , 0xff
, 0xff , 0x00 , 0x00 , 0xff , 0xff , 0xbf , 0xbf , 0xbf
, 0xbf , 0x3f , 0x3f , 0xff , 0xff , 0x3f , 0x3f , 0xbf
, 0xbf , 0x3f , 0x3f , 0xff , 0xff , 0xff , 0xff , 0xff
, 0xff , 0xff , 0x80 , 0x3f , 0x7d , 0xfd , 0xfd , 0xff
, 0x7f , 0xbf , 0xcf , 0xcf , 0xf7 , 0xfb , 0xf8 , 0xfc
, 0xfd , 0xfd , 0xfd , 0xfd , 0xfd , 0xff , 0xff , 0xfd
, 0xfd , 0x7d , 0x3f , 0x80 , 0xff , 0xff , 0xff , 0xff
, 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff
, 0xff , 0xff , 0xff , 0xff
, 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff
, 0xff , 0xff , 0xff , 0xff , 0xff , 0xe0 , 0xef , 0xef
, 0xef , 0xe0 , 0xe0 , 0xff , 0xff , 0xc3 , 0xdb , 0xdb
, 0xfb , 0xc0 , 0xc0 , 0xff , 0xff , 0xf8 , 0xe0 , 0xff
, 0xff , 0xe0 , 0xe0 , 0xff , 0xff , 0xff , 0xff , 0xff
, 0xff , 0xff , 0xef , 0xc6 , 0xd4 , 0xd0 , 0xd1 , 0xd1
, 0xc3 , 0xc7 , 0xe7 , 0xef , 0xcf , 0xdf , 0xdf , 0xd8
, 0xdf , 0xdf , 0xcf , 0xe7 , 0xe7 , 0xc3 , 0xd1 , 0xd1
, 0xd4 , 0xd6 , 0xc7 , 0xef , 0xff , 0xdf , 0xff , 0xff
, 0xdf , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff
, 0xff , 0xff , 0xff , 0xff
, 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xef
, 0xc7 , 0x83 , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff
, 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xfe
, 0xfe , 0xf0 , 0xf0 , 0xff , 0xff , 0xff , 0xff , 0xff
, 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff
, 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff
, 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff
, 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff
, 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff
, 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff
, 0x83 , 0xc7 , 0xef , 0xff
};

// �ٻ ��õ������
flash unsigned char pic_time_setting[] =
{
0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff
, 0x0f , 0x07 , 0xe7 , 0xe7 , 0xe7 , 0xe7 , 0xe7 , 0xe7
, 0xe7 , 0xe7 , 0xe7 , 0xe7 , 0xe7 , 0xe7 , 0xe7 , 0xe7
, 0xe7 , 0xe7 , 0xe7 , 0xe7 , 0xe7 , 0xe7 , 0xe7 , 0xe7
, 0xe7 , 0xe7 , 0xe7 , 0xe7 , 0xe7 , 0xe7 , 0xe7 , 0xe7
, 0xe7 , 0xe7 , 0xe7 , 0xe7 , 0xe7 , 0x07 , 0x0f , 0xff
, 0xff , 0x7f , 0x3f , 0x9f , 0xdf , 0x4f , 0xef , 0xa7
, 0x37 , 0xf7 , 0xb3 , 0xf3 , 0xf9 , 0x59 , 0x1d , 0xfd
, 0x19 , 0xd3 , 0x37 , 0xe7 , 0xcf , 0x1f , 0x7f , 0xff
, 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff
, 0xff , 0xff , 0xff , 0xff
, 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff
, 0x00 , 0x00 , 0xff , 0xef , 0x8f , 0xbf , 0x83 , 0xff
, 0xb5 , 0x85 , 0xf5 , 0x05 , 0xff , 0xfb , 0xc2 , 0xf6
, 0xfa , 0xc2 , 0xff , 0xfd , 0xe1 , 0x77 , 0x61 , 0xf7
, 0x7f , 0xff , 0x9f , 0xbf , 0xfd , 0xeb , 0xad , 0x0b
, 0xfd , 0x9e , 0xbf , 0xff , 0xff , 0x00 , 0x00 , 0xff
, 0xff , 0xf8 , 0xe3 , 0x6f , 0xaf , 0xc8 , 0xcf , 0xd8
, 0x9a , 0xbb , 0xbf , 0x3d , 0x3f , 0x3d , 0x3c , 0x3f
, 0x9e , 0x9d , 0x9c , 0x8f , 0xc7 , 0xf0 , 0xf8 , 0xff
, 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff
, 0xff , 0xff , 0xff , 0xff
, 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff
, 0x00 , 0x00 , 0x9f , 0x9f , 0x9f , 0x9f , 0x9f , 0x1f
, 0x9d , 0x9d , 0x9e , 0x9c , 0x9f , 0x9f , 0x1f , 0x9f
, 0x9f , 0x9f , 0x9f , 0x9f , 0x9f , 0x1f , 0x98 , 0x9d
, 0x58 , 0x7d , 0x3f , 0xbc , 0x9d , 0xcf , 0xcf , 0xce
, 0xcf , 0x4c , 0x4d , 0xcf , 0xcf , 0xc8 , 0xc8 , 0x8f
, 0x9f , 0x1b , 0x3d , 0x7f , 0xff , 0xff , 0xff , 0x7f
, 0xff , 0xbf , 0x7f , 0xff , 0xff , 0xff , 0xff , 0xff
, 0xff , 0x3f , 0xbf , 0xa7 , 0xaf , 0x2a , 0xa8 , 0x2b
, 0x7b , 0xfd , 0xff , 0xff , 0xbf , 0x3f , 0xff , 0xff
, 0xff , 0xff , 0xff , 0xff
, 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff
, 0x00 , 0x00 , 0xcf , 0xcf , 0xcf , 0xcf , 0xcf , 0x00
, 0xcf , 0xcf , 0xcf , 0xcf , 0xcf , 0xcf , 0x00 , 0xcf
, 0xcf , 0xcf , 0xcf , 0xef , 0x7f , 0x1e , 0xc3 , 0xf8
, 0xfe , 0xfe , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff
, 0xff , 0x00 , 0x00 , 0xff , 0xff , 0xff , 0xff , 0xff
, 0xff , 0xfe , 0xfc , 0xf8 , 0xf0 , 0xc1 , 0x07 , 0x1f
, 0xfe , 0xfc , 0xff , 0x7c , 0x7f , 0x7f , 0x7f , 0xff
, 0xfe , 0xf0 , 0xf6 , 0xf0 , 0xf1 , 0xff , 0xff , 0xf0
, 0xf0 , 0x7f , 0x7e , 0xfd , 0x0b , 0x08 , 0xff , 0xff
, 0xff , 0xff , 0xff , 0xff
, 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff
, 0x00 , 0x00 , 0xcf , 0xcf , 0xcf , 0xcf , 0xcf , 0x00
, 0xcf , 0xcf , 0xcf , 0xcf , 0xcf , 0xcf , 0x00 , 0xcf
, 0xcf , 0xcd , 0xdf , 0xff , 0x80 , 0x00 , 0x7f , 0xff
, 0xff , 0xff , 0xff , 0xff , 0x7f , 0x9f , 0x9f , 0xef
, 0xf7 , 0xf0 , 0xf8 , 0xfb , 0xfb , 0xfb , 0xfb , 0xfb
, 0xff , 0xff , 0xff , 0xff , 0xff , 0x7f , 0x00 , 0x80
, 0xff , 0xff , 0xff , 0xff , 0xf7 , 0xf0 , 0xf0 , 0xff
, 0xef , 0xfb , 0xe0 , 0xea , 0xe2 , 0xfe , 0xe1 , 0xff
, 0xf7 , 0xff , 0xf0 , 0xf7 , 0xf0 , 0xf0 , 0xff , 0xf7
, 0xff , 0xff , 0xff , 0xff
, 0xff , 0xef , 0xc7 , 0x83 , 0xff , 0xff , 0xff , 0xff
, 0xe0 , 0xc0 , 0xcf , 0xcf , 0xcf , 0xcf , 0xcf , 0xc0
, 0xcf , 0xcf , 0xcf , 0xcf , 0xcf , 0xcf , 0xc0 , 0xcf
, 0xce , 0xcf , 0xcb , 0xcf , 0xc1 , 0xcf , 0xcc , 0xc8
, 0xd1 , 0xf3 , 0xe3 , 0xe6 , 0xcf , 0xcf , 0x9f , 0x9f
, 0x9f , 0x9f , 0x9f , 0x9f , 0x9f , 0x9f , 0xcf , 0xef
, 0xe7 , 0xe3 , 0xf3 , 0xf9 , 0xdc , 0xce , 0xe7 , 0xfb
, 0xef , 0xf3 , 0xf9 , 0xff , 0xff , 0xff , 0xff , 0xff
, 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff
, 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff
, 0x83 , 0xc7 , 0xef , 0xff
};

// �ٻ ���һ�ԷԹ 150 ��
flash unsigned char pic_find_lunar_calendar[] =
{
 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0x01 , 0xfc
, 0x06 , 0x06 , 0x7a , 0x3a , 0x8a , 0xc2 , 0xe2 , 0x72
, 0x18 , 0x98 , 0xc8 , 0xec , 0xe4 , 0xe4 , 0xf4 , 0xf4
, 0xe4 , 0xec , 0xc8 , 0x98 , 0x12 , 0x62 , 0xc2 , 0x8a
, 0x1a , 0x3a , 0x7a , 0xfa , 0xfa , 0xfa , 0xfa , 0xfa
, 0xfa , 0xfa , 0xfa , 0xfa , 0xfa , 0x02 , 0x02 , 0xfc
, 0x01 , 0xff , 0x7f , 0x7f , 0x7f , 0xff , 0xff , 0xff
, 0x07 , 0x07 , 0xff , 0xff , 0x3f , 0xbf , 0xbf , 0xbf
, 0x3f , 0x3f , 0xff , 0xff , 0x7f , 0x1f , 0x5f , 0x5f
, 0x5f , 0x1f , 0x1f , 0xff , 0xff , 0xff , 0xff , 0xff
, 0xff , 0xff , 0xff , 0xff
, 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0x00 , 0xff
, 0x00 , 0xe0 , 0xfc , 0x1f , 0xe3 , 0xf9 , 0xf9 , 0xfe
, 0xff , 0xff , 0xff , 0xff , 0xff , 0x7f , 0xff , 0xff
, 0xff , 0xff , 0xff , 0x7f , 0xbf , 0xfe , 0xfc , 0xf1
, 0xe7 , 0x0e , 0xfc , 0xf1 , 0x0f , 0xff , 0xff , 0xff
, 0xff , 0xff , 0xff , 0xff , 0xff , 0x00 , 0x00 , 0xff
, 0x00 , 0xff , 0xff , 0xbf , 0x80 , 0x9f , 0x9f , 0x9f
, 0x80 , 0x80 , 0xff , 0xbe , 0xba , 0x82 , 0xfa , 0xfa
, 0x02 , 0x02 , 0xff , 0xfb , 0x83 , 0x83 , 0xef , 0xf7
, 0xfb , 0x83 , 0x83 , 0xff , 0xfd , 0xfd , 0xc1 , 0xcf
, 0x81 , 0x81 , 0x8f , 0xff
, 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0x00 , 0xff
, 0x00 , 0x03 , 0xe7 , 0xcc , 0x99 , 0x37 , 0x67 , 0xcf
, 0xdf , 0x9f , 0x3f , 0x7f , 0x7e , 0xff , 0xfd , 0xfe
, 0x7f , 0x3f , 0xbf , 0x9f , 0xdf , 0x6f , 0x37 , 0x9b
, 0xc8 , 0x66 , 0x53 , 0x00 , 0xfa , 0x03 , 0xfb , 0xc3
, 0xdb , 0x0b , 0x0b , 0xdb , 0xfb , 0x00 , 0x00 , 0xff
, 0x00 , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff
, 0xff , 0x7f , 0x7f , 0x7b , 0x79 , 0x7b , 0xf9 , 0xfb
, 0xf8 , 0xf8 , 0xfb , 0xff , 0xff , 0xff , 0xff , 0xff
, 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0x7f , 0x3f
, 0xff , 0xff , 0xff , 0xff
, 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0x00 , 0xff
, 0x00 , 0x00 , 0xfd , 0x8d , 0xad , 0x2d , 0xfd , 0x00
, 0xfd , 0x7d , 0x39 , 0x83 , 0xc6 , 0xf6 , 0xbe , 0xf6
, 0xc2 , 0x01 , 0x31 , 0xf8 , 0xfc , 0x02 , 0xfd , 0x3d
, 0x8d , 0xad , 0x0d , 0xfd , 0xfd , 0x00 , 0xfd , 0xc5
, 0x55 , 0x55 , 0x05 , 0xfd , 0xfd , 0x00 , 0x00 , 0xff
, 0x00 , 0xff , 0xff , 0xf7 , 0x03 , 0x03 , 0xff , 0xff
, 0xf7 , 0x70 , 0x76 , 0x76 , 0x76 , 0x76 , 0x07 , 0x07
, 0xff , 0xff , 0x01 , 0x01 , 0xfd , 0xfd , 0x01 , 0x03
, 0xff , 0xfb , 0xf8 , 0xf8 , 0xea , 0xea , 0x0a , 0xf8
, 0xff , 0x00 , 0x00 , 0xff
, 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0x00 , 0xff
, 0x00 , 0x00 , 0xfa , 0xfa , 0xfa , 0xfa , 0xfb , 0x00
, 0xfb , 0x00 , 0xff , 0x02 , 0xff , 0xff , 0xff , 0xff
, 0x03 , 0xff , 0x00 , 0x00 , 0xfb , 0x00 , 0xfb , 0x5a
, 0x1a , 0xea , 0x0a , 0xfb , 0x01 , 0xfc , 0xfd , 0xfd
, 0xfd , 0xfd , 0x7d , 0xbd , 0x5d , 0x2c , 0x0c , 0x81
, 0xc0 , 0xff , 0xff , 0xfd , 0xfc , 0xfc , 0xfd , 0xfc
, 0xff , 0xfe , 0xfe , 0xfe , 0xfe , 0xfe , 0xfe , 0xff
, 0xff , 0xff , 0xfe , 0xfc , 0xfd , 0xfd , 0xfc , 0xfc
, 0xff , 0xff , 0xff , 0xff , 0xff , 0xfd , 0xfc , 0xfd
, 0xfd , 0xfc , 0xfe , 0xff
, 0xff , 0xef , 0xc7 , 0x83 , 0xff , 0xff , 0xc0 , 0x9f
, 0xb8 , 0xb0 , 0xb7 , 0xb7 , 0xb7 , 0xb7 , 0xb7 , 0xb0
, 0xb7 , 0xa0 , 0x9f , 0x80 , 0x3f , 0x7f , 0x7f , 0x3f
, 0x90 , 0x8f , 0xa0 , 0xb0 , 0xb7 , 0xb0 , 0xb5 , 0xb5
, 0xb4 , 0xb7 , 0xb6 , 0xb7 , 0xa0 , 0xaf , 0x97 , 0x8b
, 0xc5 , 0xe2 , 0xf1 , 0xf8 , 0xfc , 0xfe , 0xff , 0xff
, 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff
, 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff
, 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff
, 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff
, 0x83 , 0xc7 , 0xef , 0xff
};

// �ٻ ��͹��Ѻ
flash unsigned char pic_back[] =
{
0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff
, 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff
, 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff
, 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff
, 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff
, 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff
, 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xf3 , 0xe5
, 0xe1 , 0x05 , 0xa9 , 0xe1 , 0xe5 , 0x81 , 0x85 , 0x81
, 0x91 , 0x05 , 0x29 , 0x83 , 0x07 , 0x1f , 0x1f , 0x7f
, 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff
, 0xff , 0xff , 0xff , 0xff
, 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff
, 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff
, 0xff , 0xff , 0x57 , 0x47 , 0xdf , 0x47 , 0x47 , 0xff
, 0xff , 0xff , 0x9f , 0x9f , 0x9f , 0x9f , 0x1f , 0xff
, 0xff , 0xbf , 0xbf , 0x3f , 0xff , 0x3f , 0x3f , 0xff
, 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff
, 0xff , 0xff , 0xff , 0xff , 0xc3 , 0x9d , 0x1d , 0xfd
, 0xfd , 0xfc , 0xfa , 0xf9 , 0xff , 0xff , 0xff , 0x7f
, 0x03 , 0x89 , 0x10 , 0x4c , 0x22 , 0x80 , 0x80 , 0xc0
, 0xf0 , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff
, 0xff , 0xff , 0xff , 0xff
, 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff
, 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff
, 0xe1 , 0xe9 , 0xe8 , 0xe9 , 0xef , 0xe0 , 0xe0 , 0xff
, 0xff , 0x98 , 0x98 , 0xba , 0xbb , 0xbb , 0xf8 , 0xff
, 0xff , 0xff , 0xff , 0xf8 , 0xf9 , 0xf0 , 0xf0 , 0xf1
, 0xff , 0xff , 0xff , 0xff , 0x7f , 0x3f , 0x3f , 0xbf
, 0xbf , 0xbf , 0x3f , 0x7f , 0xff , 0x10 , 0xe6 , 0xef
, 0xff , 0xff , 0x7f , 0x7f , 0xff , 0xff , 0x67 , 0x20
, 0x81 , 0xe0 , 0xf8 , 0xfe , 0xff , 0xff , 0xff , 0xff
, 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff
, 0xff , 0xff , 0xff , 0xff
, 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff
, 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff
, 0xff , 0xff , 0xc3 , 0xf9 , 0xfd , 0xc1 , 0xc1 , 0xff
, 0xff , 0xe2 , 0xea , 0xea , 0xfa , 0xe0 , 0xe0 , 0xff
, 0xfe , 0xfe , 0xe0 , 0xe7 , 0xe0 , 0xe0 , 0xff , 0xff
, 0xff , 0xff , 0xff , 0xff , 0x7c , 0x60 , 0xe7 , 0xe7
, 0xe3 , 0xeb , 0xc3 , 0x88 , 0x0c , 0x5e , 0x7c , 0xf6
, 0xf2 , 0xfa , 0xfb , 0xde , 0x2e , 0xce , 0x9c , 0xbc
, 0x78 , 0xf9 , 0xe3 , 0x0f , 0x9f , 0x9f , 0x9f , 0x9f
, 0x3f , 0x3f , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff
, 0xff , 0xff , 0xff , 0xff
, 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff
, 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff
, 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff
, 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff
, 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff
, 0xff , 0xff , 0xff , 0xc0 , 0x00 , 0x00 , 0x00 , 0x07
, 0x0f , 0x0f , 0x1f , 0x1f , 0xce , 0xe8 , 0xe1 , 0xf7
, 0xf7 , 0x77 , 0xb7 , 0x17 , 0x47 , 0xe6 , 0xf5 , 0xf1
, 0xe7 , 0xce , 0xd8 , 0x16 , 0x13 , 0x97 , 0x13 , 0x33
, 0xf0 , 0xf8 , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff
, 0xff , 0xff , 0xff , 0xff
, 0xff , 0xef , 0xc7 , 0x83 , 0xff , 0xff , 0xff , 0xff
, 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff
, 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff
, 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff
, 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff
, 0xff , 0xff , 0xff , 0xff , 0xff , 0xf8 , 0xf0 , 0xe4
, 0xc8 , 0xc0 , 0xc0 , 0xc0 , 0xe1 , 0xf7 , 0xfb , 0xfd
, 0xfc , 0xff , 0xff , 0xff , 0xce , 0x84 , 0x81 , 0x83
, 0x83 , 0x81 , 0x80 , 0x80 , 0x80 , 0xc0 , 0xc1 , 0xe0
, 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff
, 0x83 , 0xc7 , 0xef , 0xff

};

// �ٻ 7 number
flash unsigned char pic_seven_number[] =
{
0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff
,0xff , 0xff , 0xff ,0xff , 0xff , 0xff , 0xff , 0x7f
,0x7f , 0x7f , 0x7f , 0x7f , 0xff , 0xff , 0x7f , 0xbf
,0xdf , 0x8f , 0x0f , 0x1f , 0xff , 0xff , 0x9f , 0x0f
,0x0f , 0x9f , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff
,0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff
,0x3f , 0xdf , 0xaf , 0x2f , 0xcf , 0x1f , 0xff , 0xff
,0xff , 0xff , 0xff , 0xfd , 0xc3 , 0x9f , 0xb7 , 0xab
,0xb7 , 0xbb , 0xc7 , 0xff , 0xff , 0xff , 0xff , 0xff
,0xff , 0xff , 0x3f , 0xdf , 0x3f , 0xdf , 0x3f , 0xff
,0xff , 0xff , 0xff , 0xff
,0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff
,0xff , 0x7f , 0xff , 0x07 , 0x01 , 0x00 , 0x00 , 0x00
,0x80 , 0x00 , 0x40 , 0x20 , 0x10 , 0x08 , 0x01 , 0x07
,0xf3 , 0xf9 , 0xfc , 0xff , 0xff , 0x7f , 0x3f , 0x1f
,0x1f , 0x1f , 0x3f , 0x7f , 0xff , 0xff , 0xff , 0xff
,0xff , 0xdf , 0xbf , 0x7f , 0xbf , 0xbf , 0xbf , 0x7f
,0xff , 0xfe , 0xfe , 0xff , 0xff , 0xff , 0x77 , 0x6f
,0x5f , 0x3f , 0x07 , 0x7f , 0x7f , 0x7f , 0x7f , 0x7f
,0x7f , 0x07 , 0x3f , 0x5f , 0x6f , 0x77 , 0xff , 0xff
,0xff , 0xff , 0xfe , 0xfd , 0xff , 0xfd , 0xfe , 0xff
,0x7f , 0xff , 0xff , 0xff
,0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xe3 , 0xc1
,0xc4 , 0xe7 , 0xf3 , 0xfb , 0xfc , 0xf8 , 0xf8 , 0xf1
,0xf0 , 0xf0 , 0x70 , 0x70 , 0x78 , 0x78 , 0xfc , 0xff
,0xff , 0xff , 0xff , 0xff , 0xff , 0xfc , 0xf8 , 0xf0
,0xf0 , 0xf0 , 0xf8 , 0xfc , 0xff , 0xff , 0xff , 0xff
,0xff , 0xff , 0xfe , 0xff , 0xff , 0xf9 , 0xfb , 0xfc
,0x7f , 0xff , 0xff , 0xff , 0xff , 0xff , 0xdf , 0xdf
,0x5f , 0x9f , 0x00 , 0xdf , 0xdf , 0xdf , 0xdf , 0xdf
,0xdf , 0x00 , 0x9f , 0x5f , 0xdf , 0xdf , 0xff , 0xff
,0xff , 0xff , 0xf3 , 0xed , 0xed , 0xe5 , 0xed , 0xee
,0xff , 0xff , 0xff , 0xff
,0xff , 0xff , 0xff , 0xff , 0xe3 , 0xc1 , 0xc1 , 0xc1
,0xe3 , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff
,0xe1 , 0xc0 , 0x80 , 0x80 , 0x80 , 0x80 , 0xc0 , 0xe1
,0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff
,0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff
,0xff , 0xff , 0xff , 0xff , 0xf9 , 0xf6 , 0xfa , 0xf6
,0xfb , 0xff , 0xff , 0xff , 0xff , 0xff , 0xfd , 0xfe
,0xff , 0x7f , 0x3c , 0x3f , 0x7f , 0xbf , 0xdf , 0xff
,0xff , 0xfc , 0xff , 0xff , 0xfe , 0xfd , 0x7f , 0xbf
,0xbf , 0x7f , 0xbf , 0xb7 , 0x6f , 0x1f , 0xff , 0xff
,0xff , 0xff , 0xff , 0xff
,0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff
,0xff , 0xff , 0xff , 0xff , 0x7f , 0x7f , 0xff , 0xff
,0x7f , 0x7f , 0x7f , 0x7f , 0x7f , 0x7f , 0xff , 0x7f
,0x7f , 0x7f , 0xff , 0x7f , 0x7f , 0xff , 0xff , 0xef
,0xe7 , 0xe7 , 0xe7 , 0xe7 , 0x67 , 0x27 , 0x07 , 0x87
,0xc7 , 0xe7 , 0xff , 0xff , 0x7f , 0x7f , 0x4f , 0x4f
,0x5f , 0xdf , 0x5f , 0x5f , 0x7f , 0xff , 0x7f , 0x7f
,0x7c , 0x7b , 0xfb , 0xf9 , 0xfb , 0xfb , 0xff , 0xff
,0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xfe , 0xfd
,0xfd , 0xff , 0xff , 0xfd , 0xfc , 0xfe , 0xff , 0xff
,0xff , 0xff , 0xff , 0xff
,0xff , 0xef , 0xc7 , 0x83 , 0xff , 0xff , 0xff , 0xff
,0xff , 0xff , 0xff , 0xff , 0xe0 , 0xe0 , 0xef , 0xff
,0xe1 , 0xed , 0xed , 0xfd , 0xe0 , 0xe0 , 0xff , 0xef
,0xe0 , 0xe0 , 0xef , 0xe0 , 0xf0 , 0xff , 0xff , 0xff
,0xe7 , 0xe3 , 0xe1 , 0xe0 , 0xf0 , 0xfc , 0xfe , 0xff
,0xff , 0xff , 0xef , 0xe5 , 0xe0 , 0xe8 , 0xe9 , 0xe1
,0xff , 0xfc , 0xff , 0xe0 , 0xe0 , 0xff , 0xff , 0xff
,0xef , 0xe0 , 0xe0 , 0xff , 0xff , 0xff , 0xff , 0xff
,0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff
,0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff
,0x83 , 0xc7 , 0xef , 0xff
};

// �ٻ˹����
flash unsigned char pic_nhuhin[] =
{
0xe3 , 0xc9 , 0x5d , 0x4b , 0x9d , 0xde , 0xff , 0xff
, 0xff , 0x8f , 0x27 , 0x73 , 0x9b , 0x9b , 0x13 , 0xe7
, 0xcf , 0x9f , 0xbf , 0x6f , 0x17 , 0x0f , 0x03 , 0x03
, 0x01 , 0x00 , 0x00 , 0x80 , 0x00 , 0x00 , 0x00 , 0x00
, 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00
, 0x00 , 0x80 , 0x80 , 0x80 , 0x80 , 0x80 , 0x80 , 0x01
, 0x00 , 0x01 , 0x0a , 0x06 , 0x0a , 0x30 , 0x40 , 0x80
, 0x00 , 0x00 , 0x00 , 0x00 , 0x80 , 0x80 , 0xc0 , 0xc0
, 0xc0 , 0xc0 , 0xc0 , 0x80 , 0x90 , 0xb9 , 0x31 , 0x37
, 0x67 , 0x0f , 0x3f , 0xff , 0xff , 0xff , 0xff , 0xff
, 0xff , 0xff , 0xff , 0xff
, 0xff , 0xfc , 0xfe , 0xe3 , 0xf9 , 0xcc , 0x86 , 0x26
, 0x7c , 0x79 , 0x33 , 0x86 , 0xcc , 0xf9 , 0xf0 , 0xff
, 0xff , 0x0f , 0x01 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00
, 0x00 , 0x00 , 0x03 , 0x03 , 0x07 , 0x07 , 0xc2 , 0xf0
, 0xf8 , 0xf8 , 0xfc , 0xfc , 0xfe , 0xfe , 0xfe , 0xff
, 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff
, 0xff , 0xff , 0xff , 0xff , 0xff , 0xfe , 0xfe , 0xff
, 0xfe , 0xfe , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff
, 0xff , 0xff , 0x7f , 0x7f , 0x7f , 0x7f , 0x7f , 0xff
, 0xfc , 0xf0 , 0x00 , 0x00 , 0x03 , 0x1f , 0x7f , 0xff
, 0xff , 0xff , 0xff , 0xff
, 0xff , 0xff , 0xff , 0xff , 0xff , 0xbf , 0xef , 0xfb
, 0xfe , 0xfe , 0xff , 0xff , 0xff , 0xff , 0xff , 0xff
, 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00
, 0x00 , 0x00 , 0x00 , 0x00 , 0x78 , 0xfe , 0xff , 0xff
, 0xff , 0xff , 0xff , 0xff , 0xe7 , 0xe7 , 0xf3 , 0xf3
, 0xf9 , 0xf9 , 0x7d , 0x3d , 0x3d , 0x1d , 0x1d , 0x59
, 0xd9 , 0xdb , 0xb3 , 0x37 , 0x67 , 0xff , 0xff , 0xff
, 0xff , 0xff , 0xff , 0xff , 0xff , 0xef , 0xf3 , 0xf9
, 0x39 , 0x1c , 0x1e , 0x4f , 0xef , 0xef , 0xcf , 0x9e
, 0xbc , 0xfd , 0xfe , 0x00 , 0x00 , 0x00 , 0x00 , 0x07
, 0x3f , 0xff , 0xff , 0xff
, 0xff , 0xff , 0x1f , 0x8f , 0xcf , 0xcf , 0xdf , 0x9f
, 0xb1 , 0x20 , 0x28 , 0x5c , 0xbc , 0xd9 , 0xf3 , 0xc7
, 0x1c , 0x70 , 0x30 , 0x08 , 0xc8 , 0xe4 , 0xe4 , 0xe4
, 0xcc , 0x10 , 0xe0 , 0x00 , 0x00 , 0x03 , 0x1f , 0xff
, 0xff , 0xff , 0x77 , 0x67 , 0x6f , 0xff , 0xff , 0xff
, 0xff , 0xfe , 0xfc , 0xfa , 0xf8 , 0xf8 , 0xf8 , 0xfa
, 0xfb , 0xff , 0xff , 0xff , 0xfe , 0xfe , 0xdf , 0xcf
, 0xf7 , 0xf7 , 0xef , 0xf7 , 0xd7 , 0xcf , 0xff , 0xfe
, 0xfe , 0xfc , 0xfc , 0xfc , 0xfd , 0xfd , 0x7e , 0x6e
, 0xcb , 0xd3 , 0xf7 , 0xfc , 0x80 , 0x00 , 0x00 , 0x00
, 0x00 , 0x07 , 0x7f , 0xff
, 0x0f , 0x47 , 0xe6 , 0x64 , 0x6d , 0xab , 0x6b , 0x49
, 0xea , 0xd7 , 0xbe , 0xde , 0xec , 0xf7 , 0x3b , 0x9b
, 0xef , 0xf0 , 0xfc , 0xfe , 0xf9 , 0xff , 0x07 , 0xe1
, 0x9c , 0x86 , 0x83 , 0x80 , 0x00 , 0x00 , 0x80 , 0x81
, 0x87 , 0x9f , 0xbf , 0xfe , 0xf8 , 0xe0 , 0x80 , 0x01
, 0x01 , 0x01 , 0x01 , 0x01 , 0x05 , 0x05 , 0x0d , 0x0d
, 0x0d , 0x0d , 0x05 , 0x09 , 0x0d , 0x0d , 0x0d , 0x0d
, 0x09 , 0x05 , 0x0d , 0x0d , 0x0d , 0x09 , 0x05 , 0x0d
, 0x0d , 0x05 , 0x01 , 0x01 , 0x80 , 0xf0 , 0xfc , 0xff
, 0xff , 0xff , 0xff , 0x3f , 0x8f , 0xe0 , 0xe0 , 0xe0
, 0xe0 , 0xe0 , 0xe0 , 0xe7
, 0xff , 0xfe , 0xe2 , 0xc1 , 0xd9 , 0xdd , 0xc1 , 0xed
, 0xee , 0xe3 , 0xcf , 0x8f , 0x9f , 0x3f , 0x3e , 0x7d
, 0x3b , 0xff , 0xff , 0xff , 0xff , 0xc3 , 0x80 , 0x1f
, 0x7f , 0x7f , 0xff , 0xff , 0xff , 0xfe , 0xf9 , 0xf3
, 0xe7 , 0xcf , 0x9f , 0xbf , 0x3f , 0x7f , 0x7f , 0x7f
, 0x7c , 0xf8 , 0xf8 , 0xf0 , 0xe0 , 0xe0 , 0xd0 , 0xd8
, 0xdc , 0xdc , 0xbc , 0xbc , 0xbc , 0xb8 , 0xb0 , 0xb0
, 0xb8 , 0xbc , 0xbc , 0xdc , 0xdc , 0xdc , 0xd8 , 0x48
, 0x60 , 0x70 , 0x38 , 0x3e , 0x9f , 0x9f , 0xcf , 0xe7
, 0xf3 , 0xf9 , 0xfc , 0xfe , 0xff , 0xff , 0xff , 0xff
, 0xff , 0xff , 0xff , 0xff
};


// �ٻ ������ɯ� 01
flash unsigned char pic_krit01[] =
{
0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00
,0x00 , 0x80 , 0x80 , 0xf8 , 0xe6 , 0xf9 , 0xfc , 0xf6
,0xf3 , 0x79 , 0x78 , 0xf8 , 0xf8 , 0xf8 , 0xf8 , 0xf8
,0xf8 , 0xf8 , 0xf0 , 0xf0 , 0xf0 , 0xe0 , 0xe0 , 0xc0
,0xc0 , 0x80 , 0x80 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00
,0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00
,0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00
,0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00
,0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00
,0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00
,0x00 , 0x00 , 0x00 , 0x00
,0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x80 , 0xfc , 0x7e
,0xff , 0xff , 0x1f , 0x19 , 0x0d , 0x0d , 0x18 , 0x38
,0x68 , 0x48 , 0x10 , 0x10 , 0xe0 , 0x81 , 0xe1 , 0x11
,0x11 , 0x49 , 0x69 , 0x39 , 0x19 , 0x0d , 0x0d , 0x1b
,0x1f , 0x3f , 0xff , 0x7f , 0xfe , 0xb8 , 0x00 , 0x00
,0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00
,0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00
,0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00
,0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00
,0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00
,0x00 , 0x00 , 0x00 , 0x00
,0x00 , 0x00 , 0x00 , 0x00 , 0x0f , 0x18 , 0x72 , 0x8c
,0xc0 , 0x03 , 0x0c , 0x10 , 0x11 , 0x11 , 0x11 , 0x11
,0x17 , 0x16 , 0x10 , 0x08 , 0x47 , 0x40 , 0x47 , 0x08
,0x90 , 0x96 , 0x17 , 0x11 , 0x11 , 0x11 , 0x11 , 0x10
,0x0c , 0x06 , 0xc1 , 0x8c , 0x72 , 0x18 , 0x0f , 0x00
,0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00
,0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00
,0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00
,0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00
,0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00
,0x00 , 0x00 , 0x00 , 0x00
,0x00 , 0x00 , 0x00 , 0x80 , 0xc0 , 0x78 , 0xfc , 0x86
,0x07 , 0x0a , 0x0a , 0x0a , 0x0a , 0x86 , 0x84 , 0x7c
,0xb8 , 0x50 , 0xb1 , 0x71 , 0xd1 , 0x91 , 0x21 , 0x21
,0x21 , 0x20 , 0x10 , 0x10 , 0x10 , 0x10 , 0x08 , 0x18
,0x2c , 0xe4 , 0xcb , 0x8c , 0x18 , 0x30 , 0xe0 , 0xc0
,0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00
,0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00
,0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00
,0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00
,0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00
,0x00 , 0x00 , 0x00 , 0x00
,0x1c , 0xf6 , 0xe3 , 0xf5 , 0xb8 , 0x1d , 0x2e , 0x47
,0x09 , 0x91 , 0xc3 , 0xf5 , 0xc9 , 0x85 , 0x87 , 0xc3
,0xe6 , 0x78 , 0xaa , 0x00 , 0xeb , 0xff , 0x3c , 0x00
,0x00 , 0x00 , 0x00 , 0x02 , 0x04 , 0x20 , 0x00 , 0x00
,0x00 , 0x00 , 0x07 , 0x18 , 0xeb , 0x54 , 0xfa , 0x83
,0x2f , 0x1c , 0xf8 , 0xe0 , 0x00 , 0x00 , 0x00 , 0x00
,0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00
,0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00
,0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00
,0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00
,0x00 , 0x00 , 0x00 , 0x00
,0xc0 , 0x60 , 0xb1 , 0x1b , 0xaf , 0x46 , 0xae , 0x0e
,0xaf , 0x4f , 0xaf , 0x0f , 0xa7 , 0x47 , 0xab , 0xc1
,0x6a , 0x30 , 0x1a , 0x0e , 0x1f , 0x11 , 0x20 , 0x20
,0x20 , 0x20 , 0x20 , 0x20 , 0x10 , 0x18 , 0x0d , 0x18
,0xb0 , 0x60 , 0xb0 , 0x08 , 0xaf , 0xc5 , 0x6a , 0x3f
,0x1c , 0x06 , 0x03 , 0x01 , 0x00 , 0x00 , 0x00 , 0x00
,0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00
,0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00
,0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00
,0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00
,0x00 , 0x00 , 0x00 , 0x00
};

//�ٻ������ɯ� 02
flash unsigned char pic_krit02[] =
{
0x00 , 0x00 , 0x00 , 0x00 , 0xc0 , 0xf8 , 0xf8 , 0xfc
,0xfe , 0xfe , 0xff , 0xff , 0xc7 , 0xc7 , 0xc7 , 0x77
,0x77 , 0xe3 , 0xc3 , 0xc3 , 0xc3 , 0x41 , 0x41 , 0x41
,0x81 , 0x81 , 0x83 , 0x03 , 0x07 , 0x07 , 0x07 , 0x87
,0x87 , 0x87 , 0x87 , 0x47 , 0x47 , 0xc7 , 0xc7 , 0xc7
,0xe3 , 0x73 , 0x73 , 0x73 , 0xc7 , 0xff , 0xff , 0xff
,0xff , 0xff , 0xfe , 0xfc , 0xfc , 0xfc , 0xc0 , 0x00
,0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00
,0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00
,0x00 , 0x00 , 0x00 , 0xfc , 0x66 , 0x66 , 0x66 , 0x66
,0x66 , 0x66 , 0x00 , 0x00
,0x00 , 0x00 , 0xf0 , 0xfc , 0x0f , 0x4f , 0x47 , 0x87
,0x0f , 0x1f , 0x7f , 0x80 , 0x00 , 0x20 , 0x20 , 0x20
,0x20 , 0x20 , 0x20 , 0xe3 , 0xe3 , 0xe7 , 0xc4 , 0x00
,0x00 , 0x00 , 0x00 , 0xff , 0xff , 0x08 , 0xff , 0x00
,0x00 , 0x00 , 0xc0 , 0xe4 , 0xe7 , 0xe3 , 0x23 , 0x20
,0x20 , 0x20 , 0x20 , 0x20 , 0x00 , 0x00 , 0x80 , 0xc3
,0xc3 , 0x3f , 0x9f , 0x4f , 0x47 , 0x47 , 0x0f , 0xf8
,0xf0 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00
,0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00
,0x00 , 0x00 , 0x00 , 0xb7 , 0xe6 , 0xc6 , 0xc6 , 0xc6
,0xc6 , 0xc6 , 0x00 , 0x00
,0x00 , 0x00 , 0x01 , 0x03 , 0x0f , 0x3c , 0x78 , 0xc3
,0xe0 , 0xe0 , 0x00 , 0x03 , 0x0c , 0x0c , 0x0c , 0x0c
,0x0c , 0x0c , 0x0c , 0x0c , 0x0d , 0x0d , 0x0c , 0x0c
,0x0c , 0x03 , 0x03 , 0x20 , 0x20 , 0x20 , 0x20 , 0x03
,0x0c , 0x0c , 0x8c , 0xcd , 0x0d , 0x0c , 0x0c , 0x0c
,0x0c , 0x0c , 0x0c , 0x0c , 0x0c , 0x0c , 0x03 , 0x00
,0x00 , 0xe0 , 0xc3 , 0x78 , 0x3c , 0x3c , 0x0f , 0x03
,0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00
,0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00
,0x6c , 0xcc , 0xc0 , 0xec , 0xcc , 0xcc , 0xcc , 0xcc
,0xcc , 0xcc , 0x00 , 0x00
,0x00 , 0x00 , 0x00 , 0x00 , 0xf0 , 0xf8 , 0x1c , 0x0c
,0x0e , 0x07 , 0x13 , 0x13 , 0x13 , 0x13 , 0x13 , 0x13
,0x06 , 0x0e , 0x0c , 0x1c , 0xf8 , 0xf0 , 0x60 , 0x60
,0xc1 , 0xc1 , 0xc1 , 0x41 , 0x41 , 0x41 , 0x41 , 0x41
,0x41 , 0x41 , 0x41 , 0x80 , 0x80 , 0x80 , 0x80 , 0x80
,0x80 , 0x40 , 0x40 , 0x60 , 0x70 , 0xf8 , 0xf8 , 0x9c
,0xbc , 0x7f , 0xf0 , 0x70 , 0xe0 , 0x80 , 0x00 , 0x00
,0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00
,0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00
,0x00 , 0x00 , 0x00 , 0xf9 , 0xcc , 0xcc , 0xcc , 0xcc
,0xcc , 0xcc , 0x00 , 0x00
,0x38 , 0x4c , 0xae , 0x87 , 0xeb , 0xf7 , 0xfe , 0x7c
,0x98 , 0x98 , 0x18 , 0x18 , 0x18 , 0x18 , 0x58 , 0x98
,0x58 , 0x7c , 0x7c , 0x3e , 0x3b , 0x75 , 0xab , 0x83
,0xbc , 0x03 , 0xab , 0xbf , 0xbf , 0xfc , 0xc0 , 0x00
,0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x30 , 0x30 , 0xc0
,0xc0 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x01 , 0x07
,0x1f , 0x7f , 0xc6 , 0xb8 , 0xfa , 0xb7 , 0xbf , 0xfc
,0xf8 , 0xe0 , 0xc0 , 0x80 , 0x00 , 0x00 , 0x00 , 0x00
,0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00
,0x00 , 0x00 , 0x00 , 0x78 , 0xcc , 0xcc , 0xcc , 0xcc
,0xcc , 0x78 , 0x00 , 0x00
,0xfc , 0xff , 0xef , 0xcf , 0x03 , 0x04 , 0x0c , 0x10
,0xc0 , 0xc0 , 0xe3 , 0xf0 , 0xf8 , 0xff , 0xff , 0xf0
,0xe0 , 0xe0 , 0xe0 , 0xf0 , 0xf0 , 0x3c , 0xbf , 0x1f
,0xae , 0x00 , 0xaa , 0xfc , 0xfe , 0xff , 0x0f , 0x00
,0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00
,0x00 , 0x0c , 0xcc , 0xc0 , 0x00 , 0x00 , 0x00 , 0x00
,0x00 , 0x00 , 0x03 , 0xff , 0xfc , 0xff , 0xdb , 0x3c
,0xee , 0x03 , 0xab , 0xff , 0xfe , 0xfc , 0xf0 , 0x00
,0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00
,0x00 , 0x00 , 0x00 , 0x3c , 0x66 , 0x60 , 0x60 , 0x60
,0x66 , 0x3c , 0x00 , 0x00

};


/**************************************************************************
// GLOBAL VARIABLE
/**************************************************************************/
//# ���ٷ�����͡
unsigned char menu_id = 0;
//# ���Ѻ���� ����ѹ��� �� Global ��������ͺ�ء��ǹ
unsigned char hh,mm,ss,hs;
unsigned char now_day,now_month;
unsigned int now_year;
// �� set ��� timer interrupt , clear ��� current_menu
// ���͡�÷ӧҹ�ء� 1 �Թҷ�Ẻ���Դ�ٻ
unsigned char m_wait_1sec = 0;
// + 1 ��� timer0 �ء� interrupt
unsigned int m_tick_count = 0;
// ��Ǩ俵Դ�ú _LIGHT_DELAY �Թҷ� ���Ѻ
unsigned char led_light_count_delay = 0;
// ��Ǩ����Դ������ _POWER_DELAY �Թҷ� ���Ѻ
unsigned char power_count_delay = 0;

/**************************************************************************
// ��Ǩ����ա�á���� key ������� ����� debounce
/**************************************************************************/
unsigned char get_keypad()
{
    unsigned char ch = 'x';
    if ((ch = matrix_key_chk()) != 'x')
    {
        delay_ms(20); // debounce
        if ((ch = matrix_key_chk()) != 'x')
        {
            while(matrix_key_chk() != 'x'); // wait release key
            delay_ms(10); // debounce
            // reset ���һԴ�ʧ
            led_light_count_delay = 0;
            LCD5110_LED_HIGH();
            // reset ���һԴ����ͧ
            power_count_delay = 0;
        }
    }
    return ch;
}

/**************************************************************************
// ˹�Ҩ������
/**************************************************************************/
void waitting_display(void)
{
    lcd_clear_video_ram();
    lcd_set_axay(0,THAI_LINE_1);
    lcd_put_stringf_2vram(" ��س����ѡ����..",WRITE_TRANSPARENT);
    lcd_update_video_ram();
}

/**************************************************************************
// ���ʴ��ٻ�Ҿ������͡�á�����
/**************************************************************************/
void display_logo(flash unsigned char *tab_picture)
{
    lcd_fill_picture_special(tab_picture, WRITE_REVERSE);
    lcd_update_video_ram();
    while (get_keypad() == 'x');
}


/**************************************************************************
// �ʴ��ѹ���һ��� (˹�Ҩ��á)
/**************************************************************************/
// ��ǹ��ù������ҤԴ �ѹ/��� ��ҧ� �����ʴ�˹�Ҩ�
void sub_process_current_day(void)
{
	// ź��鹷���ʴ��š�͹
	lcd_clear_video_ram();
    // �ʴ��ѹ
    lcd_set_axay(0,0);
    lcd_put_stringf_2vram(THAI_DAY_TBL[m_day_name], WRITE_TRANSPARENT);
    sprintf(tmp_display_string," %d/%d/%d", m_day_no, m_month_no, m_thai_year);
    lcd_put_string_2vram(tmp_display_string, WRITE_TRANSPARENT);

    // ��Ǩ�ѹ���
    lcd_set_axay(0,15);
    if (chk_monk_day( m_phase_up, m_lunar_phase, m_thai_month, LUNAR_YEAR_TBL[m_year_count] == 1))
        lcd_put_char_2vram('*',WRITE_TRANSPARENT);
    else
        lcd_put_char_2vram(' ',WRITE_TRANSPARENT);

    // �ʴ��ѹ�� �����͹��
    if (m_thai_month == 8 && month_twiced)
        sprintf(tmp_display_string,"%d   �-� ",m_day_name);
    else
        sprintf(tmp_display_string,"%d   %d  ",m_day_name,m_thai_month);
    lcd_change_num_2thai(tmp_display_string);
    // �ʴ��չѡ�ѵ�
    lcd_put_stringf_2vram(ANIMAL_YEAR_TBL[m_animal_year], WRITE_TRANSPARENT);
    // �ʴ���� (������Ѻ)
    // ��Ǻ��� y = 11 (14-3) , �����ҧ�� y = 19 (11+8)
    if (m_phase_up)
    {
        // �繢�ҧ���
        // ������Ţ������� �ʴ� x ��ҡѺ �
        if (m_lunar_phase < 10)
        {
            lcd_set_axay(18,11);
                sprintf(tmp_display_string,"%d",m_lunar_phase);
                lcd_change_num_2thai(tmp_display_string);
        }
        else
        {
            lcd_set_axay(15,11);
                sprintf(tmp_display_string,"%d",m_lunar_phase);
                lcd_change_num_2thai(tmp_display_string);
        }
        lcd_set_axay(18,19);
            lcd_put_char_2vram('�',WRITE_TRANSPARENT);
    }
    else
    {
        // �繢�ҧ���
        lcd_set_axay(18,11);
            lcd_put_char_2vram('�',WRITE_TRANSPARENT);
        // ������Ţ������� �ʴ� x ��ҡѺ �
        if (m_lunar_phase < 10)
        {
            lcd_set_axay(18,19);
                sprintf(tmp_display_string,"%d",m_lunar_phase);
                lcd_change_num_2thai(tmp_display_string);
        }
        else
        {
            lcd_set_axay(15,19);
                sprintf(tmp_display_string,"%d",m_lunar_phase);
                lcd_change_num_2thai(tmp_display_string);
        }
    }

    lcd_update_video_ram();
    #ifdef DEBUG_MODE
        debugf("update day..");
    #endif
}

// ��ǹ��ù������ҤԴ �����ҧ� �����ʴ�˹�Ҩ�
void sub_process_current_yam(void)
{
    unsigned char x,y;
    // ź�Ҿ �����ʴ���
    for (x = 0; x < MAX_X; x++)
        for (y = 28; y < MAX_Y; y++)
            lcd_clr_dot(x,y);

    lcd_set_axay(0,34);
    // �ʴ���ҧ�ѹ���͡�ҧ�׹
    if (hh >= 6 && hh < 18)
    {
        // ��ҧ�ѹ
        // �ʴ��ѹ��� ���
        y = get_yam_period(hh,mm,ss);
        sprintf( tmp_display_string,"%02d:%02d:%02d � %d %c",hh,mm,ss,m_day_name, get_yam_number('D',m_day_name,y) );
    }
    else
    {
        // ��ҧ�׹
        // �ʴ��ѹ��� ���
        y = get_yam_period(hh,mm,ss);
        // ����Թ���§�׹����� ����ѧ���֧ 6 ������ ��ͧ���������ѹ���
        if ( hh >= 0 && hh < 6 || hh == 24)
        {
             // �����ҧ�׹Ẻ����
             x = m_day_name -1;
             x = x < 1 ? 7 : x;
             sprintf( tmp_display_string,"%02d:%02d:%02d � %d %c",hh,mm,ss, x, get_yam_number('N', x,y) );
        }
        else
        {
             // �����ҧ�׹����
             sprintf( tmp_display_string,"%02d:%02d:%02d � %d %c",hh,mm,ss,m_day_name, get_yam_number('N',m_day_name,y) );
        }
    }
    lcd_put_string_2vram(tmp_display_string,WRITE_TRANSPARENT);

    // �ʴ��Ţ��� ��� �ѧ��Тͧ���
    lcd_set_axay(72,28);
    sprintf( tmp_display_string,"%d",get_yam_period(hh,mm,ss) );
    lcd_put_string_2vram(tmp_display_string,WRITE_TRANSPARENT);

    lcd_set_axay(72,40);
    sprintf( tmp_display_string,"%d",get_yam_rithm(hh,mm,ss) );
    lcd_put_string_2vram(tmp_display_string,WRITE_TRANSPARENT);

    lcd_update_video_ram();
}

// ��ǹ�ʴ��ѹ���һ���
void menu_current_day(void)
{
    unsigned char result = 0;
    // clear ��û����ż����� ����͢����ѹ
    unsigned char new_day_update = false; // �ѧ�������ż�
    // �Ѻ������Ҥú 2 �ҷ������ѧ (����֧���Ҩҡ RTC)
    unsigned char count2min = 0;

    // �ʴ�˹�Ҩ͡����..
    waitting_display();

    // clear ��û����ż����� ����͢����ѹ
    new_day_update = false; // �ѧ�������ż�
    // ��ͧ�ʴ������Թҷ�����
    m_wait_1sec = true;

    // ��Ŵ��� Ǵ�. ��� �����������
    rtc_get_time(RTC_CHIP, &hh,&mm,&ss,&hs);
    rtc_get_date(RTC_CHIP, &now_day, &now_month, &now_year);

    #ifdef DEBUG_MODE
        debugf("current_day()..");
        sprintf(debug_tmp_string,"date=%d/%d/%d",now_day, now_month, now_year);
        debug(debug_tmp_string);
    #endif
    // !!!!! ��ͧ�ŧ�� �.�. ��͹ !!!!  -*-
    now_year += 543;
    // ���� Ǵ� �ѹ�ä��
    if ( (result = process(now_day,now_month,now_year)) == true)
    {
        // �����ż������ʴ����˹�Ҩ�
        sub_process_current_day();
        while( get_keypad() != '1')
        {
            // ��Ҥú 1 �Թҷ� ���仴֧�������� 1 ����
            // �ء� 1 �Թҷ� �ж١ set �Ӵ� timer ��� clear �����
            if (m_wait_1sec)
            {
                // �ء� 2 �ҷ�  15 �Թҷ� ���֧�����һ���������ҡ RTC
                count2min += 1;
                if (count2min == 0)
                {
                    rtc_get_time(RTC_CHIP, &hh,&mm,&ss,&hs);
                }
                else
                {
                    // ���������ͧ �ء� 1 �Թҷ�
                    ss += 1;
                    if (ss > 59)
                    {
                        ss = 0;
                        mm += 1;
                        if (mm > 59)
                        {
                            mm = 0;
                            // ����� �ǡ hh ������������Ң�й���� 24 ���� 0 ���ԡ�
                            // ������Ը� ���¡����������
                            count2min = 255;
                        }
                    }
                }

                // clear ��â����ѹ
                if ( hh == 23 && mm == 59 )
                {
                    new_day_update = false;
                }
                if (  (hh == 0 || hh == 24) && mm == 0 && ss >= 0 && !new_day_update)
                {
                    new_day_update = true;
                    // waiting
                    waitting_display();
                    // ��Ŵ��� Ǵ�.
                    rtc_get_date(RTC_CHIP, &now_day, &now_month, &now_year);
                    now_year += 543;
                    if ( (result = process(now_day,now_month,now_year)) == true)
                        // update display ��ǧ�ѹ��� ��� �ѹ������
                        sub_process_current_day();
                }
                // update display ��ǧ����
                sub_process_current_yam();
                // clear ���� 1 �Թҷ�
                m_wait_1sec = false;
            }
        }
    }
    else
    {
        // �բ�ͼԴ��Ҵ ����ʴ���������ش������
        lcd_clear_video_ram();
        lcd_set_axay(0,THAI_LINE_0);
        sprintf(tmp_display_string, "�Դ��Ҵ(E%d)",result);
        lcd_put_string_2vram(tmp_display_string,WRITE_TRANSPARENT);
        lcd_set_axay(0,THAI_LINE_1);
        lcd_put_stringf_2vram("��سҵ�� Ǵ�.����",WRITE_TRANSPARENT);
        lcd_update_video_ram();

        // ������ҼԴ��Ҵ ����Ѻ���� �������
        // ����������� ��鹰ҹ
        now_day = 12;
        now_month = 8;
        now_year = 2551U;
        hh = 9;
        mm = 9;
        ss = 9;

        while( get_keypad() != '1' );
    }
}

/**************************************************************************
// ������º��ԷԹ 150 ��
/**************************************************************************/
void menu_calendar(void)
{
    unsigned char result;
    // �բ�ͼԴ��Ҵ ����ʴ���������ش������
    lcd_clear_video_ram();
    lcd_set_axay(0,THAI_LINE_0);
    lcd_put_stringf_2vram("��º��ԷԹ 150 ��",WRITE_TRANSPARENT);
    lcd_set_axay(0,THAI_LINE_1);
    lcd_put_stringf_2vram(" ����ѹ ��͹ ��",WRITE_TRANSPARENT);
    lcd_update_video_ram();
    result = gui_thai_date_component(6,THAI_LINE_2,&now_day, &now_month, &now_year,3);
    // ������͡
    if ( result == 99 )
    {
        // ��س����ѡ����
        waitting_display();
        // ���� Ǵ� �ѹ�ä��
        if ( (result = process(now_day,now_month,now_year)) == true)
        {
            sub_process_current_day();
            while( get_keypad() != '1' );
        }
        else
        {
            // �բ�ͼԴ��Ҵ ����ʴ���������ش������
            lcd_clear_video_ram();
            lcd_set_axay(0,THAI_LINE_0);
            sprintf(tmp_display_string, "�Դ��Ҵ(E%d)",result);
            lcd_put_string_2vram(tmp_display_string,WRITE_TRANSPARENT);
            lcd_set_axay(0,THAI_LINE_1);
            lcd_put_stringf_2vram("��س���� Ǵ�.����",WRITE_TRANSPARENT);
            lcd_update_video_ram();
            while( get_keypad() != '1' );
        }
    }
}


/**************************************************************************
// �����ʴ� �Ţ 7 ��� 9 �ҹ
/**************************************************************************/
// process ����
void sub_process_seven_number(unsigned char dd,unsigned char mm, unsigned char yy
    ,unsigned char base)
{
    unsigned char * result_ptr;
    result_ptr = get_base_from_bd(dd,mm,yy,base);
    sprintf(tmp_display_string,"�ҹ %d = %d,%d,%d,%d,%d,%d,%d"
        ,base
        ,result_ptr[1],result_ptr[2],result_ptr[3],result_ptr[4]
        ,result_ptr[5],result_ptr[6],result_ptr[7]);
    #ifdef DEBUG_MODE
        debug(tmp_display_string);
    #endif
    lcd_clear_video_ram();
    lcd_set_axay(0,THAI_LINE_0);
    lcd_put_string_2vram(tmp_display_string,WRITE_TRANSPARENT);
    draw_down_arrow();
    while( get_keypad() != '5' );
}

// �����ʴ� �Ţ 7 ��� 9 �ҹ
void menu_seven_number(void)
{
    unsigned char result;
    unsigned char base;

    #ifdef DEBUG_MODE
        unsigned char dd = 3;
        unsigned char mm = 4;
        unsigned char yy = 4;
        debugf("test menu_seven_number()");
        debug_tmp_ptr = get_base_from_bd(dd,mm,yy,1);
        debug_7number(debug_tmp_ptr);
        debug_tmp_ptr = get_base_from_bd(dd,mm,yy,2);
        debug_7number(debug_tmp_ptr);
        debug_tmp_ptr = get_base_from_bd(dd,mm,yy,3);
        debug_7number(debug_tmp_ptr);
        debug_tmp_ptr = get_base_from_bd(dd,mm,yy,4);
        debug_7number(debug_tmp_ptr);
        debug_tmp_ptr = get_base_from_bd(dd,mm,yy,5);
        debug_7number(debug_tmp_ptr);
        debug_tmp_ptr = get_base_from_bd(dd,mm,yy,6);
        debug_7number(debug_tmp_ptr);
        debug_tmp_ptr = get_base_from_bd(dd,mm,yy,7);
        debug_7number(debug_tmp_ptr);
        debug_tmp_ptr = get_base_from_bd(dd,mm,yy,8);
        debug_7number(debug_tmp_ptr);
        debug_tmp_ptr = get_base_from_bd(dd,mm,yy,9);
        debug_7number(debug_tmp_ptr);
        debugf("----end----\r\n");
    #endif


    // �բ�ͼԴ��Ҵ ����ʴ���������ش������
    lcd_clear_video_ram();
    lcd_set_axay(0,THAI_LINE_0);
    lcd_put_stringf_2vram("�Ţ 7 ��� 9 �ҹ",WRITE_TRANSPARENT);
    lcd_set_axay(0,THAI_LINE_1);
    lcd_put_stringf_2vram(" ����ѹ ��͹ ��",WRITE_TRANSPARENT);
    lcd_update_video_ram();
    result = gui_thai_date_component(6,THAI_LINE_2,&now_day, &now_month, &now_year,3);
    // ������͡
    if ( result == 99 )
    {
        // ��س����ѡ����
        waitting_display();
        // ���� Ǵ� �ѹ�ä��
        if ( (result = process(now_day,now_month,now_year)) == true)
        {
            sub_process_current_day();
            draw_down_arrow();
            while( get_keypad() != '5' );

            #ifdef DEBUG_MODE
                sprintf(tmp_display_string,"d=%d,m=%d,y=%d"
                    ,m_day_name,m_thai_month,m_animal_year);
                debug(tmp_display_string);
            #endif

            // �ҹ 1-9
            for (base = 1; base <= 9; base++)
                sub_process_seven_number(m_day_name,m_thai_month,m_animal_year,base);
        }
        else
        {
            // �բ�ͼԴ��Ҵ ����ʴ���������ش������
            lcd_clear_video_ram();
            lcd_set_axay(0,THAI_LINE_0);
            sprintf(tmp_display_string, "�Դ��Ҵ(E%d)",result);
            lcd_put_string_2vram(tmp_display_string,WRITE_TRANSPARENT);
            lcd_set_axay(0,THAI_LINE_1);
            lcd_put_stringf_2vram("��س���� Ǵ�.����",WRITE_TRANSPARENT);
            lcd_update_video_ram();
            while( get_keypad() != '1' );
        }
    }
}

/**************************************************************************
// ���ٵ���ѹ ����
/**************************************************************************/
void menu_datetime_setting(void)
{
    unsigned char result;
    unsigned char cmdexit = false;

    lcd_clear_video_ram();
    lcd_set_axay(0,THAI_LINE_0);
    lcd_put_stringf_2vram("  ������� Ǵ�. ",WRITE_TRANSPARENT);

    sprintf(tmp_display_string, "%02d:%02d:%02d",hh,mm,ss);
    lcd_set_axay(18,THAI_LINE_2);
    lcd_put_string_2vram(tmp_display_string,WRITE_TRANSPARENT);
    lcd_update_video_ram();

    while (!cmdexit)
    {
        result = gui_thai_date_component(12,THAI_LINE_1,&now_day, &now_month, &now_year,7);
        // �����ش�͡�� ���� ��ŧ
        if (result == 0 || result == 99)
            cmdexit = true;
        else
        {
            // ����ش ���������� �������
            result = gui_time_component(18,THAI_LINE_2,&hh, &mm, &ss,1);
            // ��ŧ ���� ����ش
            if (result == 99 || result == 7)
                cmdexit = true;
        }
    }

    // ����ա�����͡������� ����׹�ѹ
    if (result == 99)
    {
        lcd_clear_video_ram();
        lcd_set_axay(0,THAI_LINE_0);
        lcd_put_stringf_2vram(" �׹�ѹ������� ",WRITE_TRANSPARENT);
        lcd_set_axay(0,THAI_LINE_1);
        lcd_put_stringf_2vram(" ���������� ? ",WRITE_TRANSPARENT);
        lcd_set_axay(0,THAI_LINE_2);
        lcd_put_stringf_2vram("��ŧ     ¡��ԡ",WRITE_TRANSPARENT);
        lcd_update_video_ram();
        while( get_keypad() != '1' );
        // ��س����ѡ����
        waitting_display();
        // ��Ǩ Ǵ� �ѹ�ä��
        if ( (result = process(now_day,now_month,now_year)) == true)
        {
            // �����������
            rtc_set_date(RTC_CHIP, now_day, now_month, now_year - 543U);
            rtc_set_time(RTC_CHIP, hh, mm, ss, 0);

            // �ʴ���
            lcd_clear_video_ram();
            lcd_set_axay(0,THAI_LINE_1);
            lcd_put_stringf_2vram("����������º����",WRITE_TRANSPARENT);
            lcd_update_video_ram();
            while( get_keypad() == 'x' );
        }
        else
        {
            // �բ�ͼԴ��Ҵ ����ʴ���������ش������
            lcd_clear_video_ram();
            lcd_set_axay(0,THAI_LINE_0);
            sprintf(tmp_display_string, "�Դ��Ҵ(E%d)",result);
            lcd_put_string_2vram(tmp_display_string,WRITE_TRANSPARENT);
            lcd_set_axay(0,THAI_LINE_1);
            lcd_put_stringf_2vram("��س���� Ǵ�.����",WRITE_TRANSPARENT);
            lcd_update_video_ram();
            while( get_keypad() != '1' );
        }
    }
}

/**************************************************************************
// ���� ��� ����
/**************************************************************************/
void menu_coin_slotmachine(void)
{
    unsigned char a,b,c,d;
    lcd_clear_video_ram();
    lcd_set_axay(12,THAI_LINE_0);
    lcd_put_stringf_2vram("3..",WRITE_TRANSPARENT);
    lcd_update_video_ram();
    delay_ms(250);
    delay_ms(250);
    delay_ms(250);
    lcd_put_stringf_2vram("2..",WRITE_TRANSPARENT);
    lcd_update_video_ram();
    delay_ms(250);
    delay_ms(250);
    delay_ms(250);
    lcd_put_stringf_2vram("1..",WRITE_TRANSPARENT);
    lcd_update_video_ram();
    delay_ms(250);
    delay_ms(250);
    delay_ms(250);

    // �֧�����ҵ�駤������
    rtc_get_time(RTC_CHIP, &a,&b,&c,&d);

    lcd_set_axay(24,THAI_LINE_1);
    srand(d);
    if (rand() % 2 == 0)
         // �������� �͡���
         lcd_put_stringf_2vram("��� !!",WRITE_TRANSPARENT);
    else
         // ������͡����
         lcd_put_stringf_2vram("���� !!",WRITE_TRANSPARENT);
    lcd_update_video_ram();
    while( get_keypad() != '1' );
}

/**************************************************************************
// ���ٵ�駤�� ��û�ء
/**************************************************************************/
void menu_alarm_setting(void)
{
     unsigned char result;
    // �բ�ͼԴ��Ҵ ����ʴ���������ش������
    lcd_clear_video_ram();
    lcd_set_axay(0,THAI_LINE_0);
    lcd_put_stringf_2vram("   ������һ�ء",WRITE_TRANSPARENT);
    lcd_update_video_ram();
    result = gui_time_component(18,THAI_LINE_1,&hh, &mm, &ss, 1);
    // ������͡
    if ( result == 99 )
    {
        // �����������
        rtc_set_alarm_time(RTC_CHIP,hh, mm, ss,00);
        rtc_alarm_on(RTC_CHIP);

        // �ʴ���
        lcd_clear_video_ram();
        lcd_set_axay(0,THAI_LINE_1);
        lcd_put_stringf_2vram("����������º����",WRITE_TRANSPARENT);
        lcd_update_video_ram();
        while( get_keypad() == 'x' );
    }
}

/**************************************************************************
// �����ʴ� about , �����˵�
/**************************************************************************/
void menu_about(void)
{
    lcd_clear_video_ram();
        lcd_set_axay(0,THAI_LINE_0);
        lcd_put_stringf_2vram(" �ͺ�س������͡��",WRITE_TRANSPARENT);
        lcd_set_axay(0,THAI_LINE_1);
        lcd_put_stringf_2vram("  - �ԹԨ�ѹ��� - ",WRITE_TRANSPARENT);
        lcd_set_axay(0,THAI_LINE_2);
        lcd_put_stringf_2vram(" Pinijjan.com",WRITE_TRANSPARENT);
    lcd_update_video_ram();
    while( get_keypad() != '1' );
}

/**************************************************************************
// ��Ҥ�� menu_id ���ʴ���
/**************************************************************************/
void update_current_menu(void)
{
    #ifdef DEBUG_MODE
        debugf("update_current_menu()");
        sprintf(debug_tmp_string,"menu_id = %d",menu_id);
        debug(debug_tmp_string);
    #endif
    switch(menu_id)
    {
        case 0: lcd_fill_picture_special(pic_back, WRITE_REVERSE);break;
        case 1: lcd_fill_picture_special(pic_find_lunar_calendar, WRITE_REVERSE);break;
        case 2: lcd_fill_picture_special(pic_seven_number, WRITE_REVERSE);break;
        case 3: lcd_fill_picture_special(pic_coin, WRITE_REVERSE);break;
        case 4: lcd_fill_picture_special(pic_time_setting, WRITE_REVERSE);break;
        //case 4: lcd_fill_picture_special(pic_set_alarm, WRITE_REVERSE);break;
        case 5: lcd_fill_picture_special(pic_pinij_jan, WRITE_REVERSE);break;
    }
    lcd_update_video_ram();
}

/**************************************************************************
// �Ǻ�������ʴ��� ���� ��ҧ�
/**************************************************************************/
void show_menu(void)
{
    unsigned char key;
    #ifdef DEBUG_MODE
        debugf("show_menu()");
    #endif
    update_current_menu();

    while ( (key = get_keypad()) != '1') // �� 1 ������͡
    {
        #ifdef DEBUG_MODE
            if (key != 'x')
                puts((char*)key);
        #endif

        if (key == '4')
        {
            #ifdef DEBUG_MODE
                debugf("press '4'");
            #endif
            if (menu_id > 0)
                menu_id--;
            else
                menu_id = ALL_MENU-1;
        }
        else if (key == '6')
        {
            #ifdef DEBUG_MODE
                debugf("press '6'");
            #endif
            if (menu_id < ALL_MENU-1)
                menu_id++;
            else
                menu_id = 0;
        }

        // ��ҡ��������͢�� �������¹����ʴ���
        if ((key == '4') || (key == '6'))
        {
            update_current_menu();
        }
    }
}



/**************************************************************************
// ��˹���ҵ�ҧ� ����ͧ����к�
/**************************************************************************/
void main_init()
{
#ifdef DEBUG_MODE
    debug_init();
#endif
        /* x'tal 11.0592 ��ç���
        // ������� Timer 0
        // Timer/Counter 0 initialization
        // Clock source: System Clock
        // Clock value: 10.800 kHz
        // Mode: Normal top=FFh
        // OC0 output: Disconnected
        TCCR0=0x05;  // prescale / 1024
        TCNT0=0x00;
        OCR0=0x00;
        */

    // x'tal 4 Mhz
    // Timer/Counter 0 initialization
    // Clock source: System Clock
    // Clock value: 15.625 kHz
    // Mode: Normal top=FFh
    // OC0 output: Disconnected
    TCCR0=0x04;  // prescale / 256
    TCNT0=0x00;
    OCR0=0x00;

    // Timer(s)/Counter(s) Interrupt(s) initialization
    TIMSK=0x01;
    // Global enable interrupts
    #asm("sei")

    // �Դ��� keypad
    matrix_key_init();

    // �Դ��� real time clock
    i2c_init();
    // only time alarm = 0
    rtc_init(RTC_CHIP, 0);

    // �Դ��õԴ��� LCD
    lcd_initial();                		// Initial LCD
    lcd_clear_screen();              	// Clear Screen Display

    // ***************************************************************************
    // Thai Graphics
    // ***************************************************************************
    // �Դ���� Graphics ������ҹ������
    lcd_set_mode(LCD_GRAPHICS);

}


/**************************************************************************
// Interrupt timer 0
/**************************************************************************/
// Timer 0 overflow interrupt service routine
interrupt [TIM0_OVF] void timer0_ovf_isr(void)
{
    m_tick_count ++;
    // xtal 11.0592 ��Ǩ m_tick_count >= 42  (10800 / 255 = 42.xx)
    // xtal 4Mhz ��Ǩ m_tick_count >= 61  (15625 / 255 = 61.xx)
    if (m_tick_count >= COUNT_INTERRUPT_UPON_XTAL)
    {
        m_tick_count = 0;
        // �ú 1 �Թҷ�����
        m_wait_1sec = true;

        // �Ǻ����ʧ���ҧ
        if (led_light_count_delay < _LIGHT_DELAY)
        {
             LCD5110_LED_HIGH();
             led_light_count_delay += 1;
             if (led_light_count_delay >= _LIGHT_DELAY)
                 // off light
                 LCD5110_LED_LOW();
        }

        // �Ǻ��� power
        if (power_count_delay < _POWER_DELAY)
        {
            power_count_delay += 1;
            if (power_count_delay >= _POWER_DELAY)
            {
                // ��Դ˹�Ҩ�
                LCD5110_OFF();
                // �Դ����ͧ
                sleep_enable();
                powerdown();
            }
        }
    }
}
/**************************************************************************
// main
/**************************************************************************/
void main(void)
{
    // �纤�����������Ŵ��� refresh LCD
    unsigned char old_value = 0;

    /* ******************* ��ǹ��� initial �ػ�ó��ҧ� */
    main_init();

    /* ******************* �ʴ� ��� */
    // �ʴ���� �ԹԨ�ѹ���
    display_logo(pic_pinij_jan);

    // *************************
    // �ʴ���� �. ���
    display_logo(pic_nhuhin);
    // *************************
    /*
    // �ʴ���� �.��ɯ�
    while (get_keypad() == 'x')
    {
        if (power_count_delay != old_value)
        {
            old_value = power_count_delay;
            if (power_count_delay % 2 == 0)
                lcd_fill_picture_special(pic_krit01, WRITE_NORMAL);
            else
                lcd_fill_picture_special(pic_krit02, WRITE_NORMAL);
            lcd_update_video_ram();
        }
    };*/
    ////////////////////////////

    lcd_clear_video_ram();
    lcd_update_video_ram();
    while(1)
    {
        switch(menu_id)
        {
            case 0: menu_current_day(); break;
            case 1: menu_calendar(); break;
            case 2: menu_seven_number(); break;
            case 3: menu_coin_slotmachine(); break;
            case 4: menu_datetime_setting(); break;
            //case 4: menu_alarm_setting(); break;
            case 5: menu_about(); break;
        }
        //menu_id = show_menu();  menu_id �� global
        show_menu();
    }
}
/*****************************************************
This program was produced by the
CodeWizardAVR V1.24.8e Evaluation
Automatic Program Generator
� Copyright 1998-2006 Pavel Haiduc, HP InfoTech s.r.l.
http://www.hpinfotech.com

Project : Matrix Keypad(4x3)
Version : 1.0
Date    : 25/25/2007
Author  : SRINIVASAN.M
Company : Scorpion Controls
Comments:
This programe is made to support matrix kepad with the specification of 4 row and 3 columns.
If you have any doubts in this programe, then visit ATMEL community in www.Orkut.com.


Chip type           : ATmega8535
Program type        : Application
Clock frequency     : 1.000000 MHz
Memory model        : Small
External SRAM size  : 0
Data Stack size     : 128
*****************************************************/


// Standard Input/Output functions
#ifndef ATMEGA32
    #define MCU         ATMEGA32
    #include <mega32.h>
	#ifndef __SLEEP_DEFINED__
	#define __SLEEP_DEFINED__
	.EQU __se_bit=0x80
	.EQU __sm_mask=0x70
	.EQU __sm_powerdown=0x20
	.EQU __sm_powersave=0x30
	.EQU __sm_standby=0x60
	.EQU __sm_ext_standby=0x70
	.EQU __sm_adc_noise_red=0x10
	.SET power_ctrl_reg=mcucr
	#endif
#endif
#include <MatrixKeypad4x3.h>
#include <delay.h> // ��͹˹�ǧ���� ��ͧ��駤�� xtal � config �ͧ compiler ����

// ��˹�..
// - ��� port ��� 2 �ѧ���蹴�ҹ�� ��ҹ�� (㹹���� portA)
// - Px.0 - x.3 ��͡Ѻ Row
// - Px.4 - x.6 ��͡Ѻ Col

// �Ըա����
// 0. ��С�ȡ�� include �������͹�Ѻ MCU �����
// 1. ��С�� matrix_key_init(); (�� portA ��ҹ͡�˹�ͨҡ����ͧ��)
// 2. ��Ǩ�� matrix_key_chk(); (�Ф׹������� ������衴)

// ������ҧ debounce
// ��Ǩ����ա�á���� key ������� ����� debounce
/*unsigned char get_keypad()
{
    unsigned char ch;
    if ((ch = matrix_key_chk()) != 'x')
    {
        delay_ms(25); // debounce
        if ((ch = matrix_key_chk()) != 'x')
        {
            while(matrix_key_chk() != 'x'); // wait release key
            delay_ms(25); // debounce
        }
    }
    return ch;
}*/

// �� portA
#define KEYPAD_PORT PORTA
#define KEYPAD_DDR  DDRA
#define KEYPAD_PIN	PINA

/*

  ���ҧ᡹

  PinX.6  PinX.5  PinX.4
    |       |       |
	|       |       |
----+-------+-------+------   PinX.3
    |       |       |
----+-------+-------+------   PinX.2
    |       |       |
----+-------+-------+------   PinX.1
    |       |       |
----+-------+-------+------   PinX.0

*/

void matrix_key_init(void)
{
    KEYPAD_DDR = 0xf0;	// bit 7,6,5,4 = output
						// bit 3,2,1,0 = input
    KEYPAD_PORT = 0x0f; // 0b  0000 1111 //for internal pullup
}

unsigned char matrix_key_chk(void)
{
    unsigned char x;
    unsigned char keypad = 'x';

    // column 1
    KEYPAD_PORT = 0xbf; // 1011 1111
    for (x=0;x<255;x++);    // ��ͧ���� delay (����繤�����ص������) ���蹹���ѹ����ҹ��� port ���ѹ
    switch(KEYPAD_PIN & 0x0f)
    {
        case 0x07: keypad='1'; break; // 0000 0111
        case 0x0b: keypad='4'; break; // 0000 1011
        case 0x0d: keypad='7'; break; // 0000 1101
        case 0x0e: keypad='*'; break; // 0000 1110
    }
    if (keypad != 'x') return keypad;

    // column 2

    KEYPAD_PORT = 0xdf; // 1101 1111
    for (x=0;x<255;x++);  // ��ͧ���� delay (����繤�����ص������) ���蹹���ѹ����ҹ��� port ���ѹ
    switch(KEYPAD_PIN & 0x0f)
    {
        case 0x07: keypad='2'; break;
        case 0x0b: keypad='5'; break;
        case 0x0d: keypad='8'; break;
        case 0x0e: keypad='0'; break;
    }
    if (keypad != 'x') return keypad;

    // column 3
    KEYPAD_PORT = 0xef; // 1110 1111
    for (x=0;x<255;x++);    // ��ͧ���� delay (����繤�����ص������) ���蹹���ѹ����ҹ��� port ���ѹ
    switch(KEYPAD_PIN & 0x0f)
    {
        case 0x07: keypad='3'; break;
        case 0x0b: keypad='6'; break;
        case 0x0d: keypad='9'; break;
        case 0x0e: keypad='#'; break;
    }
    //if (keypad != 'x') return keypad;
    return keypad;
}


/**************************************************************************
// ��Ǩ����ա�á���� key ������� ����� debounce
/**************************************************************************/
/*
unsigned char get_keypad()
{
    unsigned char ch = 'x';
    if ((ch = matrix_key_chk()) != 'x')
    {
        delay_ms(20); // debounce
        if ((ch = matrix_key_chk()) != 'x')
        {
            while(matrix_key_chk() != 'x'); // wait release key
            delay_ms(10); // debounce
        }
    }
    return ch;
}*/


#include <seven_number.h>

// ���ҧ��� ��ͧ�á �����  [�ѹ][������]
flash char YAM_DAY_TBL[8][9] = { {'0','0','0','0','0','0','0','0','0'},
                            {'0','1','6','4','2','7','5','3','1'},
                            {'0','2','7','5','3','1','6','4','2'},
                            {'0','3','1','6','4','2','7','5','3'},
                            {'0','4','2','7','5','3','1','6','4'},
                            {'0','5','3','1','6','4','2','7','5'},
                            {'0','6','4','2','7','5','3','1','6'},
                            {'0','7','5','3','1','6','4','2','7'}
                            };

// ���ҧ��� ��ͧ�á �����
flash char YAM_NIGHT_TBL[8][9] = {{'0','0','0','0','0','0','0','0','0'},
                                    {'0','1','5','2','6','3','7','4','1'},
                                    {'0','2','6','3','7','4','1','5','2'},
                                    {'0','3','7','4','1','5','2','6','3'},
                                    {'0','4','1','5','2','6','3','7','4'},
                                    {'0','5','2','6','3','7','4','1','5'},
                                    {'0','6','3','7','4','1','5','2','6'},
                                    {'0','7','4','1','5','2','6','3','7'}
                                    };

// ���ҧ�����ҧ�ѹ��¹ �����ѧ
flash char YAM_DAY_BACK_TBL[8][8] = { {'0','0','0','0','0','0','0','0'},
                            {'0','3','5','7','2','4','6','1'},
                            {'0','4','6','1','3','5','7','2'},
                            {'0','5','7','2','4','6','1','3'},
                            {'0','6','1','3','5','7','2','4'},
                            {'0','7','2','4','6','1','3','5'},
                            {'0','1','3','5','7','2','4','6'},
                            {'0','2','4','6','1','3','5','7'}
                            };

// �纼Ţͧ��äӹǹ�����觡�Ѻ
unsigned char m_7base[8];

// ��Ǩ����� ��� �ͧ�ѹ���� ���Ţ����
// part day = 'D', night = 'N'
unsigned char get_yam_number(char part,char day,char yam)
{
    if (part == 'D')
        return YAM_DAY_TBL[day][yam];
    return YAM_NIGHT_TBL[day][yam];
}

// �ŧ�������Թҷ�
unsigned long int time_2sec(unsigned char hh,unsigned char mm,unsigned char ss)
{
    if (hh >= 18 && hh <= 24)
        hh -= 12;
    else if (hh >= 0 && hh <= 5)
        hh += 12;

    return (((hh*60UL)+mm)*60UL)+ss;
}
// ��Ǩ��������ҹ��� �������ǧ����������
unsigned char get_yam_period(unsigned char hh,unsigned char mm,unsigned char ss)
{
    return ((time_2sec(hh,mm,ss) - 21600) / 5400)+1;
}

// ��Ǩ��������ҹ��� ������ѧ������� 1-3
unsigned char get_yam_rithm(unsigned char hh,unsigned char mm,unsigned char ss)
{
    unsigned int second;
    //formula
    second = (unsigned int)( 5400UL-((get_yam_period(hh,mm,ss)*90UL*60UL+21600) - time_2sec(hh,mm,ss)) );

    if (second < 1800)
        return 1;
    if (second < 3600)
        return 2;
    return 3;
}

/*
**
** ����� function �ӹǹ �Ţ 7 ��� 9 �ҹ
** �������ö���¡��ҹⴴ� ���ͧ���¡��ҹ
** get_base_from_bd()
**
*/
// clear ��ҷ���纡�͹˹�� �����ͼš�äӹǹ
static void clear_base(void)
{
    unsigned char count;
    for (count = 1; count <= 7; count++)
        m_7base[count] = 0;
}

// �ӹǹ��� �ҹ 123
static unsigned char * get_base_123(unsigned char dd, unsigned char mm, unsigned char yy,
    unsigned char base)
{
    unsigned char index;
    unsigned char tmp_value;

    if (base >= 1 && base <= 3)
    {
        if (base == 1)
            tmp_value = dd;
        else if (base == 2)
            tmp_value = mm;
        else if (base == 3)
            tmp_value = yy;
        for (index = 1; index <= 7; index++)
        {
            m_7base[index] = tmp_value;
            tmp_value += 1;
            if (tmp_value > 7)
                tmp_value -= 7;
        }
    }
    return (unsigned char *)m_7base;
}

// �ӹǹ��Ұҹ 4
static unsigned char * get_base_4(unsigned char dd, unsigned char mm, unsigned char yy)
{
    unsigned char index;
    unsigned char tmp_value;

    for (index = 1; index <= 7; index++)
    {
        // �ѹ
        tmp_value = dd + index - 1;
        if (tmp_value > 7)
            tmp_value -= 7;
        m_7base[index] = tmp_value;
        // ��͹
        tmp_value = mm + index - 1;
        if (tmp_value > 7)
            tmp_value -= 7;
        m_7base[index] += tmp_value;
        // ��
        tmp_value = yy + index - 1;
        if (tmp_value > 7)
            tmp_value -= 7;
        m_7base[index] += tmp_value;
    }
    return (unsigned char*)m_7base;
}

// �ӹǹ Ǵ� ��Ф׹��Ұҹ��� 5
static unsigned char * get_base_5(unsigned char dd, unsigned char mm, unsigned char yy)
{
    unsigned char count;
    // initial base 4;
    get_base_4(dd,mm,yy);
    for (count = 1; count <= 7; count++)
    {
        m_7base[count] = m_7base[count] - ((m_7base[count] /7) * 7);
        if (m_7base[count] == 0)
            m_7base[count] = 7;
    }
    return (unsigned char*)m_7base;
}

// �ӹǹ Ǵ� ��Ф׹��Ұҹ��� 6,7
static unsigned char * get_base_67(unsigned char dd, unsigned char mm, unsigned char yy
    , unsigned char base)
{
    unsigned char twice = 1;
    unsigned char count;
    // initial_base 5;
    get_base_5(dd,mm,yy);

    if (base == 7)
        twice = 2;
    while (twice > 0)
    {
        for (count = 1; count <= 7; count++)
        {
            m_7base[count] = m_7base[count] * 2;
            m_7base[count] = m_7base[count] - ((m_7base[count] /7) * 7);
            if (m_7base[count] == 0)
                m_7base[count] = 7;
        }
        twice -= 1;
    }
    return (unsigned char*)m_7base;
}

// �ӹǹ Ǵ� ��Ф׹��Ұҹ��� 8
static unsigned char * get_base_8(unsigned char dd, unsigned char mm, unsigned char yy)
{
    unsigned char count;
    unsigned char first_num;
    // initial base 5
    get_base_5(dd,mm,yy);
    // �纤���Ţ����á
    first_num = m_7base[1];
    // �Թ�����ҧ�ѹ ��¹�Թ˹��
    for(count = 1; count <=7; count++)
    {
        m_7base[count] = YAM_DAY_TBL[first_num][count];
        // change char to number
        m_7base[count] -= 48;
    }
    return (unsigned char*)m_7base;
}

// �ӹǹ Ǵ� ��Ф׹��Ұҹ��� 9
static unsigned char * get_base_9(unsigned char dd, unsigned char mm, unsigned char yy)
{
    unsigned char count;
    unsigned char last_num;

    // initial base 5
    get_base_5(dd,mm,yy);
    // �纤���Ţ�ش���� ����á
    last_num = m_7base[7];

    // initial base 8
    get_base_8(dd,mm,yy);
    // �纤���Ţ�ش���� ��� 2
    last_num += m_7base[7];

    // ��Ѻ�Ţ�� 7
    last_num = last_num - ((last_num /7) * 7);
    if (last_num == 0)
        last_num = 7;

    // �Թ�����ҧ�ѹ ��¹�����ѧ
    for(count = 1; count <=7; count++)
    {
        m_7base[count] = YAM_DAY_BACK_TBL[last_num][count];
        // change char to number
        m_7base[count] -= 48;
    }
    return (unsigned char*)m_7base;
}

// �ӹǹ Ǵ� ��Ф׹��Ұҹ����ͧ���
unsigned char * get_base_from_bd(unsigned char dd, unsigned char mm, unsigned char yy,
    unsigned char base)
{
    clear_base();
    if (dd > 7)
        dd -= 7;
    if (mm > 7)
        mm -= 7;
    if (yy > 7)
        yy -= 7;

    if (base >= 1 && base <= 3)
    {
        return get_base_123(dd,mm,yy,base);
    }
    // �ҹ����Ţ
    else if (base == 4)
    {
        return get_base_4(dd,mm,yy);
    }
    else if (base == 5)
    {
        return get_base_5(dd,mm,yy);
    }
    else if (base >= 6 && base <= 7)
    {
        return get_base_67(dd,mm,yy,base);
    }
    else if (base == 8)
    {
        return get_base_8(dd,mm,yy);
    }
    return get_base_9(dd,mm,yy);
}


/* ��ǹ����ʴ��� component ��ҧ� */
#include <lcd-5110-thai.h>
	#ifndef __SLEEP_DEFINED__
	#define __SLEEP_DEFINED__
	.EQU __se_bit=0x80
	.EQU __sm_mask=0x70
	.EQU __sm_powerdown=0x20
	.EQU __sm_powersave=0x30
	.EQU __sm_standby=0x60
	.EQU __sm_ext_standby=0x70
	.EQU __sm_adc_noise_red=0x10
	.SET power_ctrl_reg=mcucr
	#endif
#include <gui.h>
#include <matrixkeypad4x3.h>


#define true 1
#define false 0


// ��Ǩ�ͺ column ����ҡ���ҷ���к��������
unsigned char chk_column_between(unsigned char x,unsigned char y,unsigned char check)
{
    if (check > x && check < y)
        return true;
    return false;
}

// �Ҵ��ШѴ��� component ���������
// parameter x,y ���˹觷����Ҵ
// hh,mm,ss �� ��. �ҷ� �Թҷ�
// init_column ���˹觤���������
// return �繤�� ��ѡ 0 = ����, 99 = ���͡ , �͡��� = ���
unsigned char gui_time_component(unsigned char ax,unsigned char ay,
    unsigned char *hh, unsigned char *mm, unsigned char *ss,
    unsigned char init_column)
{
    unsigned char tmp_value[7]; // [0] = free
    unsigned char x,y,xstart;
    unsigned char keypress;
    unsigned char cmdexit = false;
    unsigned char current_column = init_column; // start column 1

    // �ŧ ��.
    tmp_value[1] = *hh / 10;
    tmp_value[2] = *hh % 10;
    // �ŧ �ҷ�
    tmp_value[3] = *mm / 10;
    tmp_value[4] = *mm % 10;
    // �ŧ �Թҷ�
    tmp_value[5] = *ss / 10;
    tmp_value[6] = *ss % 10;
    while ( chk_column_between(0,7,current_column) && !cmdexit)
    {
        keypress = get_keypad();
        // ��ҡ� ���� '1' ������ ���͡
        if ( keypress == '1' )
        {
            current_column = 99;
            cmdexit = true;
        }
        // ��ҡ����� ����
        else if ( keypress == '4')
        {
            //if ( chk_column_between(0,7,current_column - 1) )
                current_column -= 1;
        }
        // ��ҡ����� ���
        else if (keypress == '6')
        {
            //if ( chk_column_between(0,7,current_column + 1) )
                current_column += 1;
        }
        // ��ҡ����
        else if (keypress == '2')
        {
            switch(current_column)
            {
                //��ѡ�á������ 0-2
                case 1:
                    tmp_value[current_column] += 1;

                    if (tmp_value[current_column] == 2)
                    {
                        if ( tmp_value[(unsigned char)(current_column + 1)] > 4 )
                            tmp_value[(unsigned char)(current_column + 1)] = 4;
                    }
                    else if (tmp_value[current_column] > 2)
                    {
                        tmp_value[current_column] = 0;
                    }
                break;

                // ��Ѻ�Թ 3 ���������ѡ�á�� 2
                case 2:
                    if (tmp_value[current_column - 1] == 2)
                    {
                        if ( tmp_value[current_column] < 3)
                            tmp_value[current_column] += 1;
                        else
                            tmp_value[current_column] = 0;
                    }
                    // �����ѡ�á�� 0,1 ��Ѻ�黡��
                    else if (tmp_value[current_column] + 1 < 10)
                    {
                        tmp_value[current_column] += 1;
                    }
                    else
                    {
                        tmp_value[current_column] = 0;
                    }
                break;

                // ��ѡ 3,5 ��ѡ�á��������Թ 5
                case 3:
                case 5:
                    tmp_value[current_column] += 1;
                    if (tmp_value[current_column] > 5)
                        tmp_value[current_column] = 0;
                break;

                // ��ѡ 4,6 0-9 ����
                case 4:
                case 6:
                    // �����ѡ�á �� 0..5 ��ͻ�Ѻ����
                    if (tmp_value[current_column - 1] < 6)
                    {
                        if (tmp_value[current_column] + 1 < 10)
                            tmp_value[current_column] += 1;
                        else
                            tmp_value[current_column] = 0;
                    }
                break;
            }
        }
        // ��ҡ�ŧ
        else if (keypress == '5')
        {
            tmp_value[current_column] -= 1;
            switch(current_column)
            {
                //��ѡ�á������ 0-2
                case 1:
                    if (tmp_value[current_column] == 255)
                        tmp_value[current_column] = 2;

                    if (tmp_value[current_column] == 2 &&
                        tmp_value[(unsigned char)(current_column + 1)] > 4 )
                        tmp_value[(unsigned char)(current_column + 1)] = 4;
                break;

                // ��Ѻ�Թ 3 ���������ѡ�á�� 2
                case 2:
                    if (tmp_value[current_column] == 255)
                    {
                        if (tmp_value[current_column - 1] == 2)
                            tmp_value[current_column] = 3;
                        else
                            tmp_value[current_column] = 9;
                    }
                break;

                // ��ѡ 3,5 ��ѡ�á��������Թ 5
                case 3:
                case 5:
                    if (tmp_value[current_column] == 255)
                        tmp_value[current_column] = 5;
                break;

                // ��ѡ 4,6 0-9 ����
                case 4:
                case 6:
                    // �����ѡ�á �� 0..5 ��ͻ�Ѻ����
                    if (tmp_value[current_column] == 255)
                        tmp_value[current_column] = 9;
                break;
            }
        }

        // display // ���������ʴ����ҡ�͹���
        if (keypress != 'x' || init_column != 99)
        {
            init_column = 99; // �������ʴ��������

            // ź˹�Ҩ� ����ǳ����ͧ�ʴ���
            // ����� ax ���֧ 8(�ѡ�� hh:mm:ss) * 6(�������ҧ��� space px) + ax(�ش�����)
            for (x = ax; x < 8*6+ax; x++)
                for (y = ay; y < 9+ay; y++)
                    lcd_clr_dot(x,y);
            // draw new
            lcd_set_axay(ax,ay);
            sprintf(tmp_display_string,"%d%d:%d%d:%d%d", tmp_value[1], tmp_value[2],
                            tmp_value[3], tmp_value[4], tmp_value[5], tmp_value[6]);
            lcd_put_string_2vram(tmp_display_string, WRITE_TRANSPARENT);

            // draw cursor
            switch(current_column)
            {
                case 1:
                case 2:
                    xstart = (6*current_column)+ax-6;
                break;
                case 3:
                case 4:
                    xstart = (6*(current_column+1))+ax-6;
                break;
                case 5:
                case 6:
                    xstart = (6*(current_column+2))+ax-6;
                break;
            }
            for (x = xstart; x < xstart+6; x++)
                lcd_set_dot(x,ay+8);

            // update screen
            lcd_update_video_ram();
        }
    }

    *hh = tmp_value[1] * 10 + tmp_value[2];
    *mm = tmp_value[3] * 10 + tmp_value[4];
    *ss = tmp_value[5] * 10 + tmp_value[6];
    // clear cursor
    for (x = xstart; x < xstart+6; x++)
        lcd_clr_dot(x,ay+8);
    return current_column;
}

// �Ҵ��ШѴ��� component ������ѹ��� ���
// parameter x,y ���˹觷����Ҵ
// dd,mm,yy �� �ѹ ��͹ ��      xx/mmm/yyyy (�.�.)
// init_column ����������
// return �繤�� ��ѡ 0 = ����, 99 = ���͡, �͡���(8) = ���
unsigned char gui_thai_date_component(unsigned char ax,unsigned char ay,
    unsigned char *dd, unsigned char *mm, unsigned int *yy,
    unsigned char init_column)
{
    unsigned int tmp_calc;
    // xx/x/xxxx (7 column �Ţ��ѡ����ѡ�ѹ����� ���Ф���� 2 ��ʹ)
    // �ٻẺ 12/3/'2'567
    unsigned char tmp_value[8]; // [0] = free
    unsigned char x,y,xstart,xstop;
    unsigned char keypress;
    unsigned char cmdexit = false;
    unsigned char current_column = init_column; // start column 1
    unsigned char next_column;

    // �ŧ �ѹ
    tmp_value[1] = *dd / 10;
    tmp_value[2] = *dd % 10;
    // ����͹�����㹵����
    tmp_value[3] = (unsigned char)*mm;
    // �ŧ �������㹵����
    tmp_calc = *yy - 2000;
    tmp_value[4] = 2;
    tmp_value[5] = (unsigned char)(tmp_calc / 100);
    tmp_calc = tmp_calc - ((int)tmp_value[5]*100);
    tmp_value[6] = tmp_calc / 10;
    tmp_value[7] = tmp_calc % 10;
    while ( chk_column_between(0,8,current_column) && !cmdexit)
    {
        keypress = get_keypad();
        // ��ҡ� ���� '1' ������ ���͡
        if ( keypress == '1' )
        {
            current_column = 99;
            cmdexit = true;
        }
        // ��ҡ����� ����
        else if ( keypress == '4')
        {
            current_column -= 1;
            next_column = current_column +1;
            if (current_column == 4)
            {
                current_column -= 1;
                next_column = current_column -1;
            }
        }
        // ��ҡ����� ���
        else if (keypress == '6')
        {
            current_column += 1;
            next_column = current_column +1;
            if (current_column == 4)
            {
                current_column += 1;
                next_column = current_column +1;
            }
        }
        // ��ҡ����
        else if (keypress == '2')
        {
            switch(current_column)
            {
                //��ѡ�á������ 0-3
                case 1:
                    tmp_value[current_column] += 1;
                    // ��Ǩ����� 3 �������
                    if (tmp_value[current_column] == 3)
                    {
                        if ( tmp_value[next_column] > 1 )
                            tmp_value[next_column] = 1;
                    }
                    else if (tmp_value[current_column] > 3)
                    {
                        tmp_value[current_column] = 0;
                    }
                break;

                // ��Ѻ�Թ 1 ���������ѡ�á�� 3
                case 2:
                    if (tmp_value[current_column - 1] == 3)
                    {
                        if ( tmp_value[current_column] < 1)
                            tmp_value[current_column] += 1;
                        else
                            tmp_value[current_column] = 0;
                    }
                    // �����ѡ�á�� 0,1,2 ��Ѻ�黡��
                    else if (tmp_value[current_column] + 1 < 10)
                    {
                        tmp_value[current_column] += 1;
                    }
                    else
                    {
                        tmp_value[current_column] = 0;
                    }
                break;

                // ��ѡ 3 ����ѡ��͹ ������ 1 - 12
                case 3:
                    tmp_value[current_column] += 1;
                    if (tmp_value[current_column] > 12)
                        tmp_value[current_column] = 1;
                break;
                // ��ѡ 4 ��ѡ�ѹ �����
                case 4:
                break;

                // ��ѡ 5 (��ѡ���¢ͧ��) ������ 4-5
                case 5:
                    tmp_value[current_column] += 1;
                    // �����ѡ������ 5 ��ѡ�Ժ�Թ 7 ����Ѻ
                    if (tmp_value[current_column] == 5 && tmp_value[next_column] >= 7)
                    {
                        tmp_value[next_column] = 7;
                        // �����ѡ˹����ҡ���� 5 ���¡��ͧ��Ѻ�� 5
                        if (tmp_value[(int)next_column + 1] > 5)
                            tmp_value[(int)next_column + 1] = 5;
                    }
                    // ����Թ 5 ������������
                    if (tmp_value[current_column] > 5)
                        tmp_value[current_column] = 4;
                break;
                // ��ѡ 6 (��ѡ�Ժ), �����ѡ������ 5 ��ѡ�Ժ�������� 0-7
                case 6:
                     tmp_value[current_column] += 1;
                     // �����ѡ���� �� 5 ��ѡ�Ժ���� 7 ��ѡ˹�������Թ 5
                     if (tmp_value[current_column-1] == 5 && tmp_value[current_column] == 7)
                     {
                         // ��ѡ˹��¨������� 0 -5 ���л����٧�ش��� 2575
                         if (tmp_value[next_column] > 5)
                             tmp_value[next_column] = 5;
                     }
                     // �����ѡ���� �� 5 ��ѡ�Ժ�������� 0-7
                     else if (tmp_value[current_column-1] == 5 && tmp_value[current_column] > 7)
                     {
                         // ��ѡ�Ժ������ 7
                         tmp_value[current_column] = 0;
                     }
                     else
                     {
                         // ��ѡ�Ժ ������ 0-9 �������
                         if (tmp_value[current_column] > 9)
                             tmp_value[current_column] = 0;
                     }
                break;

                // ��ѡ 7 (˹���)
                case 7:
                    // �����ѡ 100 = 5 ��ѡ�Ժ = 7 ��ѡ˹������� 0-5
                    if (tmp_value[(int)current_column-2] == 5 &&
                        tmp_value[(int)current_column-1] == 7)
                    {
                        // ������Թ 5
                        if (tmp_value[current_column] + 1 < 6)
                            tmp_value[current_column] += 1;
                        else
                            tmp_value[current_column] = 0;
                    }
                    //  �͡��� 0-9 ����
                    else
                    {
                        if (tmp_value[current_column] + 1 < 10)
                            tmp_value[current_column] += 1;
                        else
                            tmp_value[current_column] = 0;
                    }
                break;
            }
        }
        // ��ҡ�ŧ
        else if (keypress == '5')
        {
            // copy �ҡ��� '2' �� �����������ź -
            tmp_value[current_column] -= 1;
            switch(current_column)
            {
                //��ѡ�á������ 0-3
                case 1:
                    if (tmp_value[current_column] == 255)
                        tmp_value[current_column] = 3;

                    // ��Ǩ����� 3 �������
                    if (tmp_value[current_column] == 3)
                    {
                        if ( tmp_value[next_column] > 1 )
                            tmp_value[next_column] = 1;
                    }
                    else if (tmp_value[current_column] > 3)
                    {
                        tmp_value[current_column] = 0;
                    }
                break;

                // ��Ѻ�Թ 1 ���������ѡ�á�� 3
                case 2:
                    if (tmp_value[current_column - 1] == 3)
                    {
                        if (tmp_value[current_column] == 255)
                            tmp_value[current_column] = 1;
                    }
                    // �����ѡ�á�� 0,1,2 ��Ѻ�黡��
                    else if (tmp_value[current_column] == 255)
                    {
                        tmp_value[current_column] = 9;
                    }
                break;

                // ��ѡ 3 ����ѡ��͹ ������ 1 - 12
                case 3:
                    if (tmp_value[current_column] == 0)
                        tmp_value[current_column] = 12;
                break;
                // ��ѡ 4 ��ѡ�ѹ �����
                case 4:
                break;

                // ��ѡ 5 (��ѡ���¢ͧ��) ������ 4-5
                case 5:
                    // ��ҵ�ӡ��� 4 ������������
                    if (tmp_value[current_column] < 4)
                        tmp_value[current_column] = 5;

                    // �����ѡ������ 5 ��ѡ�Ժ�Թ 7 ����Ѻ
                    if (tmp_value[current_column] == 5 && tmp_value[next_column] >= 7)
                    {
                        tmp_value[next_column] = 7;
                        // �����ѡ˹����ҡ���� 5 ���¡��ͧ��Ѻ�� 5
                        if (tmp_value[(int)next_column + 1] > 5)
                            tmp_value[(int)next_column + 1] = 5;
                    }
                break;
                // ��ѡ 6 (��ѡ�Ժ), �����ѡ������ 5 ��ѡ�Ժ�������� 0-7
                case 6:
                    // ��Ѻ
                    if (tmp_value[current_column-1] == 5 && tmp_value[current_column] == 255)
                    {
                            tmp_value[current_column] = 7;
                    }

                    if (tmp_value[current_column] == 255)
                        tmp_value[current_column] = 9;

                     // ��Ǩ
                     // �����ѡ���� �� 5, ��ѡ�Ժ�� 7 ��ѡ˹��¨�����Թ 5
                     if (tmp_value[current_column-1] == 5 && tmp_value[current_column] == 7)
                     {
                         // ��ѡ˹��¨������� 0 -5 ���л����٧�ش��� 2575
                         if (tmp_value[next_column] > 5)
                             tmp_value[next_column] = 5;
                     }
                break;

                // ��ѡ 7 (˹���)
                case 7:
                    // �����ѡ 100 = 5 ��ѡ�Ժ = 7 ��ѡ˹������� 0-5
                    if (tmp_value[(int)current_column-2] == 5 &&
                        tmp_value[(int)current_column-1] == 7)
                    {
                        // ������Թ 5
                        if (tmp_value[current_column] == 255)
                            tmp_value[current_column] = 5;
                    }
                    //  �͡��� 0-9 ����
                    else
                    {
                        if (tmp_value[current_column] == 255)
                            tmp_value[current_column] = 9;
                    }
                break;
            }
        }

        // display // ���������ʴ����ҡ�͹���
        if (keypress != 'x' || init_column != 99)
        {
            init_column = 99; // �������ʴ��������

            // ź˹�Ҩ� ����ǳ����ͧ�ʴ���
            // ����� ax ���֧ 11(�ѡ�� mm/ddd/yyyy) * 6(�������ҧ��� space px) + ax(�ش�����)
            for (x = ax; x < 11*6+ax; x++)
                for (y = ay-6; y < 9+ay; y++)
                    lcd_clr_dot(x,y);

            // draw new
            lcd_set_axay(ax,ay);
            // �ʴ��ѹ
            sprintf(tmp_display_string,"%d%d/", tmp_value[1], tmp_value[2]);
            lcd_put_string_2vram(tmp_display_string, WRITE_TRANSPARENT);
            // �ʴ���͹
            lcd_put_stringf_2vram(THAI_MONTH_TBL[ tmp_value[3] ],WRITE_TRANSPARENT);
            // �ʴ���
            sprintf(tmp_display_string,"/2%d%d%d", tmp_value[5], tmp_value[6], tmp_value[7]);
            lcd_put_string_2vram(tmp_display_string, WRITE_TRANSPARENT);

            // draw cursor
            switch(current_column)
            {
                // �ѹ
                case 1:
                case 2:
                    xstart = (6*current_column)+ax-6;
                    xstop = xstart+6;
                break;

                // ��͹
                case 3:
                    xstart = (6*(current_column+1))+ax-6;
                    xstop = xstart+12;
                break;

                // xx/ddd/x456
                // ��ѡ ���� �Ժ ˹��� �ͧ��
                case 4:
                case 5:
                case 6:
                case 7:
                    xstart = (6*(current_column+4))+ax-6;
                    xstop = xstart+6;
                break;
            }
            for (x = xstart; x < xstop; x++)
                lcd_set_dot(x,ay+8);

            // update screen
            lcd_update_video_ram();
        }
    }
    *dd = tmp_value[1] * 10 + tmp_value[2];
    *mm = tmp_value[3];
    *yy = 2000U + ((int)tmp_value[5] * 100) + ((int)tmp_value[6] * 10) + tmp_value[7];
    // clear cursor
    for (x = xstart; x < xstop; x++)
        lcd_clr_dot(x,ay+8);
    return current_column;
}



/**************************************************************************
//  utility ��� debug
// �����ҹ
// 1. include ��� header
// 2. ���¡ debug_init() ���͡�˹� baudrate
/**************************************************************************/

#include <debug_utility.h>
#include <string.h>
#include <stdio.h>

unsigned char * debug_tmp_ptr;
unsigned char debug_tmp_string[50] = "";
unsigned char debug_tmp_string_len = 0;

// ������觤����ʴ��ŷ�� printer (��ҹ txt �ҡ flash)
void debugf(flash char * txt)
{
    putsf("\r\n");
    putsf(txt);
}

// ������觤����ʴ��ŷ�� printer (��ҹ txt �ҡ SRAM)
void debug(char * txt)
{
    putsf("\r\n");
    puts(txt);
}

// ������ʴ��� �Ţ 7 �����੾��(��ҹ txt �ҡ SRAM)
void debug_7number(char * txt)
{
    unsigned char count = 1;
    putsf("\r\n");
    while (count < 8)
    {
        itoa((int)txt[count++],debug_tmp_string);
        puts(debug_tmp_string);
        putsf(",");
    }
}

// ��˹��������� baudrate 19200
void debug_init(void)
{
    // USART initialization
    // Communication Parameters: 8 Data, 1 Stop, No Parity
    // USART Receiver: On
    // USART Transmitter: On
    // USART Mode: Asynchronous
    // USART Baud Rate: 19200
    UCSRA=0x00;
    UCSRB=0x18;
    UCSRC=0x86;
    UBRRH=0x00;
    UBRRL=0x23;
}
